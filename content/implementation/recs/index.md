---
title: Implementation of Final Recommendations
draft: false
weight: 10
---

## Proposed Improvements

The Lincoln Avenue Corridor Study Working Group recommends that roadway improvements on Lincoln Avenue between Green Street and Florida Avenue include the following elements - 

*When relevant, crash modification factors (CMF) are linked along with the proposed countermeasure. The CMF number reflects the effectiveness of the countermeasure, with any number lower than 1 representing a reduction in crashes, a 1 indicates no change in crashes, and anything over a 1 rating is likely to generate an increase in crashes. These CMF numbers are determined from the [Crash Modification Factors Clearinghouse](https://www.cmfclearinghouse.org/). The CMF Clearinghouse is funded by the Federal Highway Administration and maintained by the North Carolina Highway Safety Research Center. The CMF Clearinghouse provides roadway and traffic professionals with a repository of quantified expectations of crash changes per countermeasure, along with a quality rating to best determine how countermeasures may impact crashes.*

**1. Construct on-street bike lanes on both sides of Lincoln Avenue for the entire length of the corridor.**

*[Install Bike Lanes](https://www.cmfclearinghouse.org/detail.php?facid=7838) – 0.68 CMF applies to All Crash Types*

On-street bike lanes for the corridor’s full-length will provide cyclists with their own dedicated facilities, reducing conflicts with both vehicles and pedestrians. This improvement will also address current gaps in cycling infrastructure along the corridor, providing a direct and consistent north-south route for cyclists on the east side of campus and connecting to the east-west bicycle facilities on Green Street. In combination with the existing shared-use path on Lincoln Avenue south of Florida Avenue, this will provide 1.84 miles of continuous cycling infrastructure on Lincoln Avenue. 

Of the [three potential approaches](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#bike-and-pedestrian-scenarios) to cycling and pedestrian infrastructure, continuous bike lanes were the most popular by far [with survey respondents](https://ccrpc.gitlab.io/lincoln-ave/public/phase-two-survey/#bike-and-pedestrian-scenarios). This preference was also supported by independent analysis by consulting staff at the Lochmueller Group, who analyzed the three proposals independent of the public feedback received and deemed continuous bike lanes the preferred option, due to the minimal transitions and improved safety. 

The Study Working Group also recommends that these bike lanes be elevated between street level and sidewalk level, similar to existing bike lanes along Green Street west of Lincoln Avenue. This will provide desired physical separation between the bike lane and the roadway, discouraging vehicle intrustion onto the bike lane, while working within the limited horizontal width available along the corridor, which makes other physical separation options infeasible.

{{<image src="bike lane.png"
  alt="Photo of existing raised bike lane"
  caption="Existing raised bike lane on Green Street, between Lincoln Avenue and Wright Street"
  attr="Google Maps"
  position="center">}}

**2.	Reconfigure Lincoln Avenue roadway to three-lanes (one lane in each direction and center left turn lane).**

*[Lane Reduction / Road Diet](https://www.cmfclearinghouse.org/detail.php?facid=2841) – 0.53 CMF applies to All Crash Types*

Establishing a continual, three-lane road section for the entirety of the Lincoln Avenue corridor (one through lane in each direction, and a center left-turn lane, as seen in the Green Street image above) will provide a consistent travel pattern for the length of the corridor, reduce crossing distance for bikers and pedestrians in areas with wider current right-of-way, provide right-of-way space for the proposed bike lanes mentioned above, and moderate vehicle speed due to constrained space. 

This will entail a rehabilitation of the entire roadway, providing a new, smooth roadway surface, and repainting the road section to provide the new lane alignments. In areas with wider right-of-way, this rehabilitation will allow for introduction of green space and removal of impermeable pavement. 

To allow safe transition into the corridor, the entrances to the corridor—southbound at Green Street and northbound at Florida Avenue—will both be realigned so that one lane of through traffic enters into the corridor. At the Green Street intersection, this will be accomplished by converting the combined right-turn/through lane into solely a right turn lane, and at the Florida Avenue intersection, this will be accomplished by using bike lanes and painted buffers to remove a northbound lane, resulting in one left turn lane and one right-turn/through lane.

**3.	Close vehicle access to Lincoln Avenue from the east on Oregon Street, Iowa Street, and Indiana Avenue.**

Closing vehicle access to Lincoln Avenue from the east on Oregon Street, Iowa Street, and Indiana Avenue will be consistent with  [CUUATS Access Management Guidelines (2013)](https://ccrpc.org/transportation/access_management_guidelines.php) regarding driveway and side street locations and spacing, allowing Lincoln Avenue to function more effectively as a minor arterial carrying regional vehicle traffic. This change will also allow pedestrians, bikers and drivers to navigate the corridor more safely. These closures will provide the opportunity for the mid-block RRFB installations described below, as pedestrian crossings will not be in conflict with turning vehicles.

The project team and Lochmueller Group have performed analysis on existing traffic patterns along the corridor and projected future patterns, and have confirmed that side streets in West Urbana are currently minimally impacted by vehicle traffic, even during peak hours, and this [will still be true](https://ccrpc.gitlab.io/lincoln-ave/implementation/faq/#what-are-the-expected-impacts-on-west-urbana-traffic-flow-from-street-closures) following completion of study recommendations. Road closures will also be designed to ensure continued access and use of closed blocks by existing residents, and enable bikers and pedestrians to continue to access Lincoln Avenue from these roadways.

{{<image src="oregon closure.png"
  alt="Diagram of proposed intersection closure"
  caption="Diagram of proposed intersection closure"
  attr="Lochmueller Group"
  position="center">}}

**4.	Convert Nevada Street and Vermont Avenue access from the east to solely right-turn in/right-turn out.**

Similarly to the complete road closures described above, converting Nevada Street and Vermont Avenue’s eastern access to/from Lincoln Avenue to solely right-in/right-out will allow for greater compliance with access management guidelines, more effective performance of Lincoln Avenue as a minor arterial, and improved safety for bikers and pedestrians. 

While vehicular access at these two intersections is not being completely closed, the removal of left turn crossings at these locations will improve safety and traffic flow by minimizing conflict points and the potential for head-on and angle crashes, which usually have the potential for higher severity injuries. 

At Nevada Street, the eastern leg of Nevada is located fewer than 100 feet north of the signalized western leg of Nevada Street. This proximity to a signalized intersection means that left turns here create the opportunity for conflict points, as drivers turning left onto Lincoln Avenue do not have sufficient time and space to prepare for the signalized intersection, and drivers turning left off of Lincoln Avenue can create queuing directly before the intersection. Similarly, Vermont Avenue is located within 250 feet of the signalized Pennsylvania Avenue intersection, and has the potential to create conflicts for traffic entering or exiting this intersection. The fact that both of these streets, in addition to the three closed streets mentioned above, do not continue straight through to the west side of Lincoln Avenue ensures the ability to safely alter access at these locations and effectively direct vehicles onto other routes.

{{<image src="right in right out.png"
  alt="Diagram of proposed right-in-right-out intersection"
  caption="Diagram of proposed right-in-right-out intersection"
  attr="Lochmueller Group"
  position="center">}}

**5.	Convert and consolidate all crosswalks outside of signalized intersections into mid-block crossings with rapid rectangular flashing beacons (RRFB).**

*[Mid-Block Crossing with RRFBs](https://www.cmfclearinghouse.org/detail.php?facid=11181) – 0.82 CMF applies to Vehicle/Pedestrian Crash Type*

[Rapid rectangular flashing beacons (RRFBs)](https://ccrpc.gitlab.io/lincoln-ave/implementation/faq/#what-is-a-rapid-rectangular-flashing-beacon-rrfb) provide push-buttons for pedestrians that activate flashing lights, allowing for clear communication between pedestrians and drivers about pedestrians’ intention to cross the road and drivers’ requirement to yield right of way. Following completion of this proposed improvement, all pedestrian crossings on the Lincoln Avenue corridor will either be at signalized intersections with pedestrian signals, or at mid-block crossings with RRFBs. RRFBs increase the visibility of pedestrians and bikers, reducing pedestrian and bike crashes at crosswalks.

This will entail – 
- Constructing a new, RRFB crosswalk after closing access on the east leg of Oregon Street
- Following the closure of Iowa Street, constructing RRFBs at the existing crosswalk 
- Removal of existing crosswalks at Ohio Street and Indiana Street, and construction of a mid-block, RRFB crossing between them, connected to the campus pedestrian network to the west
- Shifting the existing crosswalk at Michigan Avenue north of the intersection, and supplementing it with RRFBs.

{{<image src="springfield rrfb.png"
  alt="Photo of existing RRFB"
  caption="Existing RRFB on Springfield Avenue, in front of Grainger Library"
  attr="Google Maps"
  position="center">}}

**6.	Adjust signal timing along corridor to accommodate roadway changes.**

In response to  the above changes, signal timing at the five signalized intersections on the corridor (Green Street, Illinois Street, Nevada Street (west leg), Pennsylvania Avenue, and Florida Avenue) will be adjusted, to ensure that these signals are best allowing for smooth traffic flow after the above proposed improvements are implemented. Leading pedestrian intervals will also be considered as part of the future signal timing optimization.

**7. Relocate Lincoln Avenue bus stops between Iowa Street and Ohio Street, and northbound bus stop at Oregon Street.**

In order to minimize conflicts between different roadway users following the redesign of Lincoln Avenue, both the northbound and southbound bus stops between Iowa and Ohio Streets will be slightly relocated. 

Pending official University of Illinois approval, the southbound bus stop and shelter, located in front of Lincoln Avenue Residence Halls, will be shifted slightly westward into the University's right-of-way, with both the bike lane and sidewalk shifting behind the bus stop. This will allow southbound buses to pull out of of the path of traffic while stopped, and will also prevent buses, or passengers boarding or alighting from the bus, from interfering with pedestrians or cyclists on the west side of Lincoln Avenue. 

The northbound bus stop, currently located mid-block directly across from the southbound stop, will be shifted north to be located at the newly-closed intersection of Iowa Street. This will allow the bus stop shelter to be shifted eastward, out of the path of the sidewalk, and reducing conflicts between riders and other cyclists and pedestrians. While stopped buses will remain in the roadway at this location, road striping, signage, and bus driver training will work to ensure safe interactions between road users while buses are stopped. This will include ensuring that stopped buses pull forward far enough to avoid interfering with the RRFB crosswalk at Iowa Street.

{{<image src="bus stops.png"
  alt="Diagram of new bus stop configuration"
  caption="Diagram of new bus stop configuration"
  attr="Lochmueller Group"
  position="center">}}

Due to the closure of the east leg of Oregon Street and the installation of an RRFB crossing at this location, this bus stop will also be shifted slightly north, from the south side of Oregon Street to the north side of the street. The existing bus stops at Florida Avenue, Illinois Street, and southbound at Oregon Street will remain in their current locations, and striping, signage, and bus driver training will ensure safe interactions between roadway users at these locations.

***See full schematic engineering drawing for the final recommendations [here](https://ccrpc.gitlab.io/lincoln-ave/implementation/recs/LincolnAveFinalConcept.pdf).***

{{<image src="Final Proposal Visualization.png"
  alt="Diagram map of proposed changes on the Lincoln Avenue Corridor"
  caption="Right-click and open the image in a new tab to zoom and see detail."
  position="center">}}

## Timeline

The Urbana [Capital Improvement Plan](https://urbanaillinois.us/departments/public-works/about-public-works/engineering/capital-improvement-plan) (CIP) is the City's Plan for addressing infrastructure and capital asset needs. In the current, 2025-2029 version of the plan, the section discussing the [Lincoln Avenue Corridor](https://www.urbanaillinois.us/sites/default/files/attachments/CIP%20FY25-FY29%20Full%20Document.pdf#page=40) calls for the design phases to occur in Fiscal Years 2027 and 2029. This project is currently in a study or conceptual phase, and the design will follow, which will finalize the construction plans. 

The timeline for implementing individual components of the corridor improvement will be dependent on the availability of funding.  

Access management changes can be pursued and tested using low-cost, quick-build, temporary measures, but the study recommendation is that any side street closures be performed in concert with the pedestrian improvements, so that roadway users will perceive the safety benefits of the proposed improvements at the same time that they are adjusting to the changes in access along the east side of Lincoln Avenue.

If construction is performed in phases due to funding availability, the recommendations are organized in general order of timing below:
  
### Short Term

-	Close vehicle access to Lincoln Avenue from the east on Oregon Street, Iowa Street, and Indiana Avenue.
-	Convert Nevada Street and Vermont Avenue access from the east to solely right-turn in/right-turn out.
-	Convert all crosswalks outside of signalized intersections into mid-block crossings with rapid rectangular flashing beacons (RRFB).

### Medium/Long Term

-	Reconfigure the Lincoln Avenue roadway to 3-lanes (one lane in each direction and center left turn lane).
-	Construct on-street bike lanes on both sides of Lincoln Avenue for the entire length of the corridor.
-	Adjust signal timing along the corridor to accommodate roadway changes.


## Potential Funding Options

As discussed in the Capital Improvement Plan (CIP), CIP work draws from [five dedicated funds](https://www.urbanaillinois.us/sites/default/files/attachments/CIP%20FY25-FY29%20Full%20Document.pdf#page=2) - 

 - Capital Replacement and Improvement Fund (CR&I)
 - Local Motor Fuel Tax Fund
 - State Motor Fuel Tax Fund
 - Sanitary Sewer Benefit Tax Fund
 - Stormwater Utility Fee Fund

Pending the avaialbility of other funding sources, the CIP [currently calls](https://www.urbanaillinois.us/sites/default/files/attachments/CIP%20FY25-FY29%20Full%20Document.pdf#page=40) for future work on the Lincoln Avenue Corridor Study to be paid from the State Motor Fuel Tax Fund. However, a variety of grant programs exist which may provide funding for project implementation in the future. Possible grant funding sources are listed below.

### EPA Community Change Grant

As of Fall of 2024, the City of Urbana is pursuing an [EPA Community Change Grant](https://www.epa.gov/inflation-reduction-act/inflation-reduction-act-community-change-grants-program), a component of which is transforming the Lincoln Avenue Corridor into a Green and Complete Street through incorporating the proposed improvements of the Lincoln Avenue corridor study. The Community Change grant program funds “environmental and climate justice activities to benefit disadvantaged communities through projects that reduce pollution, increase community climate resilience, and build community capacity to address environmental and climate justice challenges.” Urbana’s proposal seeks to fund a variety of projects, including sidewalk and street tree improvements, greening of schoolyards, and drainage improvements, in addition to the implementation of the recommended changes to the Lincoln Avenue Corridor. 

### SS4A - Safe Streets and Roads for All 

The [Safe Streets and Roads for All (SS4A)](https://www.transportation.gov/grants/SS4A) grant program was established in the Bipartisan Infrastructure Law, with the goal of providing funds to local and regional governments to prevent roadway deaths and injuries. This program provides funding in two different categories – Planning and Demonstration Grants, through which recipients can perform planning work to reduce roadway fatalities and injuries, as well as demonstration activities like pilot programs—and Implementation Grants, to implement projects and strategies from previous planning processes.

In 2023, RPC received a Planning and Demonstration Grant for $945,000 to conduct supplemental planning to better consider equity in transportation safety plans, update safety data used in transportation plans, and conduct a demonstration Complete Streets project on north Lincoln Avenue. This demonstration project will be a temporary road diet on Lincoln Avenue north of the corridor, between Killarney Street and Wascher Drive. RPC staff intends to apply for SS4A Implementation funds in future application cycles, with the implementation of the Lincoln Avenue Corridor Study recommendations as one component of this grant.

### STBGP- Surface Transportation Block Grant Program

The Surface Transportation Block Grant Program (STBGP), previously known as Surface Transportation Program (STPU), is one of the core Federal-aid Highway Program categories. The Champaign-Urbana urban area receives a population-based suballocation of STBGP funds from IDOT each year.

Local agencies seeking to use STPU/STBGP funds for the Champaign-Urbana Urbanized Area are required to submit a project application to be reviewed by CUUATS staff and the Project Priority Review working group who use a set of criteria based on federal and regional transportation goals to score each project and allocate funding to the top scored projects based on funding availability.

### RAISE Discretionary Grant Program- Rebuilding American Infrastructure with Sustainability and Equity

Formerly known as BUILD (Better Utilizing Investments to Leverage Development) and TIGER (Transportation Investment Generating Economic Recovery) Discretionary Grant Programs, RAISE provides grant funding to projects that advance national goals and will have a significant impact locally or regionally.

For more information, visit the U.S. Department of Transportation [webpage on RAISE](https://www.transportation.gov/RAISEgrants).

### BIL- Bipartisan Infrastructure Law

The Bipartisan Infrastructure Law (BIL) [also known as the Infrastructure Investment and Jobs Act (IIJA)] was signed into law in November 2021 by President Biden. The law “provides approximately $350 billion for Federal highway programs over a five-year period (fiscal years 2022 through 2026). Most of this funding is apportioned (distributed) to States based on formulas specified in Federal law,” as well as through grant programs ([Bipartisan Infrastructure Law | Federal Highway Administration](https://www.fhwa.dot.gov/bipartisan-infrastructure-law/funding.cfm)).

The Multimodal Projects Discretionary Grant Opportunity (MPDG) allows agencies to apply for three grant programs (INFRA, RURAL, and MEGA) receiving funding from the BIL using one application.

-	[INFRA- Infrastructure For Rebuilding America](https://www.transportation.gov/grants/infra-grants-program)
-	[RURAL- Rural Surface Transportation Grant Program](https://www.transportation.gov/grants/rural-surface-transportation-grant)
-	[MEGA- National Infrastructure Project Assistance](https://www.transportation.gov/grants/mega-grant-program)

### Illinois HSIP- Highway Safety Improvement Program

The HSIP is a federal program managed by the Federal Highway Administration (FWHA) and aims to reduce fatalities and serious injuries from traffic-related crashes occurring on public roads. Funding is allocated to the State of Illinois and then set aside in the Highway-Railroad Crossing Fund and the High-Risk Rural Roads program. The remaining funds are allocated to the HSIP- Roads fund comprised of HSIP- State Roads, which receives 60% of the remaining funds, and HSIP- Local Roads, which receives 20% of the funds. The HSIP is a possible funding source specifically for improvements at Lincoln Avenue’s intersection with Ohio Street due to its IDOT designation as a top 5 percent safety priority. For more information on HSIP and the funding process, visit the [IDOT webpage on HSIP](https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-opportunities/highway-safety-improvement-program).

### ITEP- Transportation Enhancement Program

According to the Illinois Department of Transportation, the ITEP is a “reimbursable grant program” that requires joint funding, with the preliminary engineering costs paid up-front and reimbursed over the implementation process. Federal funding reimburses up to 50% for any acquisition costs and up to 80% for preliminary engineering costs. Project categories include pedestrian/bicycle facilities, making this a feasible option to fund the on-street bike lanes and pedestrian crossing infrastructure. More information can be found at the [IDOT webpage on ITEP](https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-opportunities/ITEP).

### Other Grants

Information on additional grants can be found here: [Grants | US Department of Transportation](https://www.transportation.gov/grants).

