---
title: Frequently Asked Questions
draft: false
weight: 20
---

## What is a rapid rectangular flashing beacon (RRFB)? 

A rapid rectangular flashing beacon (RRFB) is a button-activated light, located at the same location as a crosswalk, which signals to all users on a road that a pedestrian or bicyclist intends to cross the road. Another example of an RRFB on campus can be found on Springfield Avenue in front of the Grainger Engineering Library (see Google Street View image below).

In crosswalks that are located away from intersections, where vehicles are not otherwise required to stop, RRFBs provide the benefit of clear communication between pedestrians and other roadway users. Through pushing the button, pedestrians can signal their clear intention to cross, without needing to step into the roadway and put themselves in front of moving traffic first. Similarly, the button-activated flashing lights provide a clear signal to drivers and other roadway users that they need to stop and provide right-of-way to the pedestrian. 

On unsignalized crossings with heavy vehicle traffic, this communication can be difficult, with pedestrians reluctant to step in front of busy vehicle traffic, and this reluctance resulting in drivers being unclear on if the pedestrian wants to cross or not. This issue was expressed often in the first round of public feedback for the Lincoln Avenue Corridor Study, with the unsignalized pedestrian crossings being a source of frustration for pedestrians, who must wait long periods for an opening in traffic or for vehicles in all traffic lanes to actually yield right of way. Similarly, drivers on the corridor expressed concern with unpredictable pedestrian crossings, both on and outside of crosswalks. The bright, flashing beacons of the RRFB will allow pedestrians to communicate their intentions and drivers or other road users to understand their legal obligation to yield right of way.

<br>
 
{{<image src="springfield rrfb.png"
  alt="Street view photo of RRFB crossing at Springfield Avenue"
  attr= "Google Maps"
  position="full">}}

## Why were particular side roads selected for closure? 

As discussed in the [Access Control](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#access-control) section of the Potential Interventions page, Lincoln Avenue is a minor arterial roadway, moving traffic between local roadways and major arterials. To accommodate the demands of this road type, design guidelines recommend that side streets should be 660 feet or more apart; many West Urbana roadways that connect to Lincoln Avenue are spaced fewer than 400 feet apart. 

Access management guidelines also discourage the location of side streets within close proximity to signalized intersections, as turning traffic can conflict with vehicles either stopping at or exiting the traffic signal. This guided the decision to create right-in-right-out intersections at Nevada Street and Vermont Avenue, as these streets are located close to existing traffic signals, and vehicles performing left turns at these locations cause safety and traffic flow problems as vehicles approach or exit the traffic signal.

Finally, the corridor study seeks to provide signalized pedestrian crossings—either rectangular rapid flashing beacons (RRFBs) or traffic signals—at regular intervals across the corridor, located about 500 feet apart to encourage usage by pedestrians. RRFB crossings are not typically located at intersections where they will deal with conflicts from turning cars, but are instead located at mid-block locations with solely forward-traveling traffic. In addition to the access spacing guidelines discussed above, the road closures at Oregon and Iowa will allow for the installation of rapid rectangular flashing beacons (RRFBs) at what are currently non-signalized vehicle intersections, making crossings for bikers and pedestrians safer and easier these locations. 

Both of these spacing guidelines—vehicle access distance and signalized pedestrian crossing distance—guided the decision to close some streets and institute right-in-right-out on others, above and beyond other features likely roadway surface materials. 

## Will bikes and pedestrians still have access to closed streets?

Yes, all side streets with vehicle access closures to Lincoln Avenue will still provide paved routes to Lincoln for pedestrians and cyclists. While designs for these closures are still in progress, possibilities include temporary closures using decorative road barricades with space between them for cyclists and pedestrians, or culs-de-sac with ADA-accessible ramp and sidewalk connections in the bulb of the cul-de-sac. The closure of these roads to automobiles will also enable the installation of RRFB crossings at or near these intersections, improving the ability for cyclists and pedestrians to cross Lincoln Avenue from these roads.

## What are the expected impacts on West Urbana traffic flow from street closures?

As discussed in the [Access Control](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#access-control) section of the Potential Interventions page, traffic counts were taken at the intersections of the West Urbana side streets with Lincoln Avenue during morning and afternoon peak traffic hours. These counts showed very low traffic counts for the proposed closed or altered streets, with fewer than one vehicle per minute navigating on any of these side streets even during the busiest hours. This indicates that, even if traffic moving between Lincoln Avenue and West Urbana is concentrated into fewer designated routes, this overall neighborhood traffic level is still low enough to prevent traffic issues along the new routes.

This was verified by the diverted traffic volume projections created by the project engineer, seen below and discussed in the [Potential Interventions page](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#update-lincoln-avenue-corridor-anticipated-vehicle-impacts-from-access-management). Even with traffic from West Urbana consolidated into a smaller quantify of side streets, peak hour traffic at any of these side streets is not projected to average at higher than one vehicle per minute, and on most of these streets, is substantially lower than this amount. As discussed in the in-depth explanation, the most recent daily vehicle counts on Busey Avenue  (250 vehicles north of Nevada Street and 275 vehicles south of Nevada Street) indicate that there is sufficient capacity for vehicles to use Busey Avenue to access the consolidated Lincoln Avenue accesses. However, the configuration of intersection controls (location of two-way and four-way stop signs) on Busey should be re-evaluated as part of the implementation process.

  {{<image src="Diversion Volume graphic_Page_2.png"
  alt="Diagram of projected vehicle turn counts on Lincoln Avenue"
  attr= "Lochmueller Group"
  position="center">}}

{{<image src="Diversion Volume graphic_Page_1.png"
  alt="Diagram of projected vehicle turn counts on Lincoln Avenue, with access closures."
  attr= "Lochmueller Group"
  position="center">}}


  

## Why are some crosswalks being consolidated?

The one location where crosswalk consolidation is suggested is at the block between Ohio and Indiana, where the two, unsignalized crosswalks located at both cross streets are being converted into a single, RRFB-signalized crosswalk at mid-block. This change is being proposed for several reasons – 

-	The corridor plan proposal aims to provide signalized pedestrian crossings at approximately 500-foot intervals. The nearest crossing to the north is located at the newly-closed intersection of Lincoln with Iowa, and the nearest crossing to the south will be located at the intersection of Lincoln with Michigan. Therefore, this mid-block crossing allows for consistent, even spacing over the length of the corridor, providing nearby crossings for pedestrians and predictability of pedestrian crossings for drivers, buses, and cyclists traveling on Lincoln Avenue.
-	In general, RRFBs are recommended at uncontrolled, marked crosswalks or other places where cars are not making turn movements (for example, the existing RRFB on Springfield Avenue in front of Grainger Library).
-	As seen in the map below, the existing Ohio and Indiana crossings do not connect directly to the pedestrian grid on the west side of Lincoln Avenue. Instead, the Indiana crossing lines up with the unused historical front door of the McKinley Health Center, while the Ohio crossing lines up with the side of Shelden Hall, one of the Lincoln Avenue Residence Halls. In fact, the lack of a logical connection to the pedestrian grid can be seen in the desire path cutting across the lawn to access this crosswalk (seen in map and street view below). The campus-side connections to these existing crosswalks mean that very few pedestrian trips will be lengthened by consolidating these crosswalks into a mid-block crossing; any north or south travel to access the crosswalk would have likely occurred anyway later in the trip. The new crosswalk would connect directly to east-west pedestrian infrastructure on campus, removing the need for the desire path in order for people to access the crosswalk. In the few cases where this change does lengthen a pedestrian’s trip—walking directly between the eastern-facing exit to Shelden Hall to a destination directly on Ohio Street—the change would be less than a half-block reroute that would allow access to a flashing beacon and a much safer crossing, making the brief detour more than worthwhile. 

<br>


**Iowa to Michigan - Existing Conditions**
{{<image src="Ohio Indiana - Existing Conditions 2.png"
  alt="Map of existing pedestrian infrastructure."
  position="center">}}

**Iowa to Michigan - Proposed Conditions**
  {{<image src="Ohio Indiana - Proposed Conditions 2.png"
  alt="Map of proposed pedestrian infrastructure"
  position="center">}}

{{<image src="ohio desire path.png"
  alt="Map of proposed pedestrian infrastructure"
  attr="Google Maps"
  caption="Existing Desire Path (right of tree, connecting to crosswalk)"
  position="center">}}
 

## Will these changes make it more difficult to drive on Lincoln Avenue?

While the proposed interventions seek to enable safe and efficient travel for all modes, and will reallocate space from motor vehicles to other modes, it will nonetheless result in a safer and more straightforward travel experience for drivers, in addition to pedestrians and bicyclists.

-	Creating better options for non-automobile transportation on the corridor will encourage local residents to take advantage of these non-auto modes when they can, reducing vehicle traffic from residents who previously drove due to the difficulty of walking or biking on the corridor.
-	[Traffic modeling](https://ccrpc.gitlab.io/lincoln-ave/future/alternative-evaluation/) indicates that the proposed improvements, when all implemented together, will improve the automobile level of service on the corridor. Additionally, this modeling shows that the improved corridor will perform better for vehicles in 25 years, even when population and trip growth has increased, than it does right now. 
-	Instituting a consistent, three-lane road design for the whole corridor will remove the confusion that currently results from needing to merge when the road section changes in the middle of a busy area of campus. Additionally, the consistent center turn lane will allow for left-turning vehicles to move out of the lane of forward traffic in a manner that they are not currently able to do in several locations along the corridor.
-	Installing evenly spaced, signalized pedestrian crossings will allow for much more predictable pedestrian crossing along the corridor—the even spacing of the crosswalks will encourage pedestrians to make use of them, rather than jaywalking due to distance from a crosswalk, and they won’t be clustered together as seen currently in some crosswalk locations. In addition, pedestrian crossings all being located at signalized intersections or at rectangular rapid flashing beacon (RRFB)-equipped crosswalks will make the crossing of pedestrians safer and ensure the pedestrian use of these crosswalks is predictable and clear to drivers. Because of this, there will be less confusion about pedestrian’s intent to walk or need to suddenly brake due to unexpected crossings. 
-	Similarly, as discussed in the [Access Control](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#access-control) section of the web plan, the closures of some West Urbana side streets, as well as the conversion of some into right-in-right-out intersections, will allow for more predictable entry of cars into Lincoln Avenue. Lincoln Avenue is a minor arterial road in our county, meaning that it serves a regional purpose, moving traffic between local roadways and the major arterials (state and federal highways) of our county, in addition to serving as the major access point to the University of Illinois campus on its eastern side. As such, side road access should not be as frequent as would be expected on a neighborhood, low traffic street. The road closures will direct West Urbana drivers to particular side streets, from which they can enter Lincoln Avenue in a predictable and safe manner.
-	As drivers feel slightly more enclosed from the redesign of the roads, they will travel at consistent and moderated speeds, more appropriate to the busy, multimodal condition of Lincoln Avenue. This moderation, similar to the reduced sudden braking from pedestrian crossings, will make driving on Lincoln Avenue safer and less likely to cause vehicle-on-vehicle crashes.

## Why are we not adding a pedestrian signal phase or no-right-on-red? 

Several comments, particularly map comments in the second phase of public outreach, suggested changing the signal cycle on traffic signals on the corridor. Suggestions included adding pedestrian scrambles or exclusive pedestrian intervals (where all vehicle traffic is stopped in every direction, and pedestrians can cross in any direction), as well as removing right-turn-on-red from the signalized intersections on the corridor. While both of these measures have their place, particularly in areas with heavy pedestrian traffic (see the pedestrian signal phasing at Green and Wright), they were not determined to be appropriate interventions for the Lincoln Avenue Corridor. The [Alternative Evaluation](https://ccrpc.gitlab.io/lincoln-ave/future/alternative-evaluation/) page discusses the projected vehicle traffic impacts of different future scenarios on the corridor. Through improvements to traffic signal timing and phasing, the changes proposed in the Lincoln Avenue Corridor Study will result in improved automobile level of service—the corridor will perform better for automobiles than it does with the current infrastructure, and in fact, will perform better with future population and traffic growth than the corridor performs currently. The corridor study steering committee determined that the peak hour delays seen in the projections were a worthwhile trade-off for the safety and multimodal access benefits that are offered from the proposed changes—offering significant benefits to cyclists and pedestrians is worth the less-than-ideal conditions for drivers. 

However, providing further accommodations—such as reprogramming traffic signals to include pedestrian scrambles or no-right-on-red—was likely to result in a “F” level of service (the lowest grade) in some sections of the corridor, which would be harder to justify against the provided benefits. Additionally, pedestrian levels along Lincoln Avenue intersections were not perceived to be significant enough to necessitate pedestrian-only cycles, unlike in some areas with heavier pedestrian traffic on campus. Ultimately, the proposal provided by the corridor study sought to balance safe and efficient bike and pedestrian travel with reasonable level of service for vehicles on a minor arterial road.

## What will the final design of the new crosswalks entail? Will these include bump outs, pedestrian islands, raised crosswalks, or lighting improvements?

Because the current process is a corridor study, which will be followed by preliminary engineering and detailed design phases before construction and implementation, the actual detailed design work will take place as the proposed changes move toward implementation in the future. Detailed changes (for example, the location of street lighting on the redesigned corridor) will be clarified in these later phases. 

The proposed changes to the road section (three consistent vehicle lanes, with bike lanes on each side) and crosswalks (all pedestrian crossings will be signalized, either through traffic signals at intersections or RRFBs away from signalized intersections) offer significant safety benefits above the existing roadway design. Because of potential difficulties to their implementation, additional crossing infrastructure improvements (like bump-outs or pedestrian medians) are not being proposed as part of the study recommendations. However, these may be explored on a case-by-case basis during the future engineering and design phases. Potential difficulties to the incorporation of these changes across the corridor include - 

- **Bump-Outs** - Due to the implementation of bike lanes and limited availability of right of way on the corridor, bump-out (or curb extensions) do not appear to be feasible, as they would intrude into the bike lane. 
- **Raised Crosswalks** - Constructing raised crosswalks would provide increased paving costs, difficulty in road clearing, and traffic flow impediments along the minor arterial roadway of Lincoln Avenue. 
- **Pedestrian Medians** - In several locations along the corridor, the usage of the center lane for a shared left turn lane may impede the construction of pedestrian medians. 
   - However, The signalization of all pedestrian crossings on the corridor also means that traffic in both directions will be clearly instructed to stop for crossings. While, for example, the pedestrian islands seen on Lincoln Avenue between Green Street and University Avenue play an important role in providing refuge—pedestrians are able to check that the two lanes on the far side of the roadway are safe before they cross—the redesigned road section and crossing signalization on the Lincoln Avenue Corridor will notify traffic to stop in both directions before pedestrians begin their crossing.

## Why did bike lanes take this form?

As discussed in the [Potential Interventions](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#bike-and-pedestrian-scenarios) section, the corridor study team developed three possible scenarios for providing bike and pedestrian infrastructure on the corridor, using different combinations of on-street bike lanes and off-street shared-use paths. As discussed in the [Round 2 Public Involvement](https://ccrpc.gitlab.io/lincoln-ave/public/phase-two-survey/#bike-and-pedestrian-scenarios), Scenario 1, with bike lanes extending along the full length of the corridor, was by far the most popular of these scenarios. Following this public feedback, the project team also solicited feedback on the three scenarios from engineers and planners (experts on designing bicycle infrastructure) who were not involved in the project and unaware of this public feedback. This expert analysis also found the full-bike-lane scenario to be the preferred scenario, due to the consistent nature through the full corridor, reduced bike and pedestrian conflicts, and reduced need for cyclists to reroute across the road.

Given the determined roadway section—three vehicle lanes, with a bike lane on each side—limitations in roadway space guided the design of the bike lanes themselves. Curbs, large painted buffers, or other separation between vehicle and bike lanes were frequently not feasible in the existing roadway space. The final design tries to ensure the greatest separation possible between bike and vehicle traffic, given these constraints, by providing an elevated bike lane (similar to the design seen on Green Street between Wright Street and Lincoln Avenue). The small ledge between bike and vehicle lanes provides both a visual and physical barrier to drivers, discouraging vehicles impeding into the bike lane. 

## How will loading zones be handled in the redesigned Lincoln Avenue?

-	Commercial deliveries on the corridor can be scheduled for early morning, low-traffic hours, when parking along Lincoln Avenue will have the lowest impacts.
-	In general, delivery or other commercial vehicles will be directed to the nearest side street along Lincoln Avenue, where there will be space for temporary parking outside of the flow of traffic.
-	Lincoln Avenue-adjacent properties with sufficient driveway and parking lot space will be encouraged to use this for pre-planned loading events, like move-in or regular food service or inventory deliveries.
-	Steering Committee members will explore opportunities to re-utilize their right of way for simplified loading purposes.
-	Design and phasing of side street closures will be implemented with loading zones in mind, so that commercial truck-accessible spaces are located within a reasonable distance of all properties.
-	Enforcement and clear communication will ensure that daytime parking on Lincoln Avenue is not allowed.

Stakeholders with frequent commercial deliveries on the corridor (eg. businesses or Greek houses with food service deliveries) will be instructed to schedule all on-street deliveries for before 7AM, ensuring that delivery trucks are not obstructing the bike lanes during times of significant bike and vehicle traffic. All daytime or non-recurring deliveries will be directed to use parking or loading zones on side streets. The City of Urbana will examine the feasibility of reallocating existing metered parking on side streets as Loading Zones, providing off-Lincoln loading areas for daytime deliveries.

## How will bike lanes accommodate bus and delivery stops?

Through communication with adjacent stakeholder properties and collaboration with law and code enforcement, the loading regulations discussed above will be instituted and enforced. This will prevent conflicts between delivery vehicles from impeding on the bike lane during times of significant cycling and vehicle traffic and avoid conflicts between these users.
Availability of right of way and space will allow a bus pull-off and bike lane diversion at the southbound stop in front of Lincoln Avenue Residence Halls, removing conflict points for these modes. For the remaining bus stops along the corridor, geometric constraints will necessitate bus drop-off and pickup taking place at the curb, in the bike lane. While this will present potential conflicts between cyclists and buses, this is not an unusual alignment in the campus area (seen, for example, on First and Fourth Street in Campustown). Bus drivers will be advised to ensure that they are properly signaling and watching for cyclists when stopping and starting along the corridor, and cyclists will have the option to enter the flow of traffic or wait for bus loading to complete, depending on traffic and safety of entering the vehicle lane.
