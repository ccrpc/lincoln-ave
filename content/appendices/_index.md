---
title: Appendices
draft: false
menu: main
weight: 50
---

Check back here for data that is published about the corridor!

<rpc-table url="intersection_counts.csv"
  table-title="Intersection Traffic Counts"
  table-subtitle="7:00 AM to 7:00 PM"
  source="CCRPC"
  source-url="https://ccrpc.org/"
  text-alignment="l,r"></rpc-table>
