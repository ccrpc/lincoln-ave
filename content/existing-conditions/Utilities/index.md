---
title: Utilities
draft: false
weight: 17
---

This section provides an overview of existing utilities in and around the study area. Information was collected about existing sanitary and storm sewer lines, water mains, fire hydrants, street lighting posts, and conduits. Utility locations on the maps are shown to the accuracy of information provided by the City of Urbana Public Works Engineering Division, the Champaign County GIS Consortium, and surface evidence of their locations. Maps are available below where data is permissible for publication.

## Sanitary and Storm Sewer 
The Urbana Department of Public Works and University of Illinois each maintain sanitary and storm sewer systems within their respective jurisdictions. The following map shows the distribution of sanitary and storm mains for the City of Urbana within the area of influence. The sanitary sewer system carries wastewater from homes and businesses to the Urbana-Champaign Sanitary District’s Northeast Wastewater Treatment Plant. The storm sewer system carries rainwater runoff from roadways, homes, and businesses to local creeks and streams. 

<iframe src="https://maps.ccrpc.org/lincoln-avenue-sanitary/" width="100%" height="600" allowfullscreen="true"></iframe>

## Water
The Illinois American Water Company provides water for the area. In the study area, there are water mains along Florida Avenue and lateral service connections to adjacent properties. There are twelve fire hydrants distributed along Lincoln Avenue between Green Street and Florida Avenue. Water mains and fire hydrants are also distributed in the residential subdivisions within the area of influence. The University of Illinois maintains the water lines on University property.

## Street Lighting 
Between the University of Illinois Urbana-Champaign and the City of Urbana, Lincoln Avenue has nearly consistent street light coverage between Green Street and Florida Avenue. Lincoln Avenue between Michigan Avenue and Pennsylvania Avenue has lighting only on the west side of the street. Lincoln Avenue between California Avenue and Green Street includes lighting almost exclusively on the east side of the street. Lighting is more consistent along stretches where University properties face the corridor.
The lighting existent along the corridor are streetlights catered towards automobiles. There is not pedestrian-scale lighting available along the corridor despite having numerous crosswalks catered to pedestrians. Research has shown that poor lighting can cause a significant decrease in the number of people walking and biking. Poor lighting can also reduce the number of transit riders, since transit riders must walk to and from bus stops. The following map shows the distribution of street lighting along the corridor provided by the City of Urbana.  

<iframe src="https://maps.ccrpc.org/lincoln-avenue-lighting/" width="100%" height="600" allowfullscreen="true"></iframe>