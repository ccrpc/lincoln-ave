---
title: Plans, Reports, and Studies
draft: false
weight: 60
---



## Urbana Comprehensive Plan (2005)

The City of Urbana is currently [in the process of updating its comprehensive plan](https://www.urbanaillinois.us/comprehensive-plan) after the most recent update [was published in 2005]. The 2005 plan does not provide specific guidance on the Lincoln Avenue Corridor, aside from a goal of marketing Lincoln Avenue as “The Gateway to the University of Illinois.” This ties into another objective in the document, to conduct corridor planning on North Lincoln Avenue as one of the entryways into the City of Urbana. 

{{<image src="comp plan Community_Outreach_Summary-cover.png"
  link="/destination"
  alt="Cover page fro Imagine Urbana Community Outreach Summary, showing outline drawings of Urbana residents"
  caption="Imagine Urbana Community Outreach Summary Cover Page"
  attr="City of Urbana" attrlink="https://www.urbanaillinois.us/comprehensive-plan"
  position="right">}}

As a component of the current plan update, in 2021 the City of Urbana conducted public outreach to engage residents in the process of updating the comprehensive plan. The feedback received was published in the [Community Outreach Summary](https://www.urbanaillinois.us/sites/default/files/attachments/CommunityEngagementSummary_web.pdf). Transportation and Infrastructure was identified as one of five key themes from the public engagement, with responses in this theme falling into four categories: 
-	Road and Sidewalk Maintenance
-	Development Impact
-	Design for People Walking and Biking
-	Connecting Infrastructure to the Environment


Overall, residents voiced a desire for a more walkable community with safe, accessible infrastructure and an increased focus on sustainability.

Looking at individual-level feedback in the appendix of the Community Outreach Summary, the Lincoln Avenue Corridor comes up several times in comments from residents lauding the role of the Lincoln-Busey corridor as a transition space between campus and the West Urbana Neighborhood. Transportation-related comments along the corridor include making the traffic signals at Nevada Street and Florida Avenue more pedestrian friendly (possibly through the implementation of pedestrian-only cycles), adding a bike traffic sensor to the signal at the Green Street intersection, improving signal timing for smooth traffic flow along the corridor, and improving the roadway surface along the corridor.

In 2023, the City published Examine Urbana, which is an analysis of existing conditions in Urbana that will be used to guide the future recommendations of the comprehensive plan. The Transportation section of this document does not address the Lincoln Avenue Corridor specifically, but it provides a series of Future Considerations questions that are useful to consider for this corridor study as well. These questions are – 
-	How can the City promote equity in transportation and access to a variety of stores and housing (land use decisions)?
-	How can the City improve transportation planning and monitoring safety?
-	How will we build a transportation network that truly prioritizes safety for everyone?
-	How can the City collaborate better with other transportation authorities about infrastructure and development?
-	Recognizing that we have extremely limited funds, how can the City continue improving the prioritization of infrastructure spending?
-	What can the City do to ensure new infrastructure is more financially sustainable and encourage the use of existing infrastructure?
-	How can the City rethink streets as public spaces for transportation and recreation for all ages?

## Urbana Climate Action Plan (2012)

The [Urbana Climate Action Plan](https://www.urbanaillinois.us/sites/default/files/attachments/Adopted%20Version.pdf) was released in two phases, one focused on short-term implementation from 2013 to 2015, and the second with more long-term goals, to be pursued primarily from 2015 to 2020. This two-phased document seeks to guide the City of Urbana’s policies on reducing the threat of climate change. While the Lincoln Avenue Corridor is not explicitly mentioned in this plan, several transportation-related goals are relevant to the work of the Lincoln Avenue Corridor Study.

[Phase I of the plan](https://www.urbanaillinois.us/sites/default/files/attachments/Adopted%20Version.pdf) listed five central goals, of which Goal #2 was “Reduce Emissions from Gasoline Consumption”. This goal consisted of the following action items – 
-	Action 1: Support sustainable transportation through infrastructure improvements. 
-	Action 2: Promote a culture of sustainable transportation. 
-	Action 3: Encourage participation in sustainable transportation programs.
-	Action 4: Consider transportation sector emissions projections in land-use planning decisions.

This goal also incorporated the following implementation strategies – 

Energy Efficiency:
1. Pursue bicycle and pedestrian infrastructure improvements.
2. Pursue the development of energy-efficient transportation infrastructure such as modern roundabouts and other yield-controlled intersections, and speed control and traffic-calming measures.

Existing Plans

3. Implement recommendations from the 2010 Urbana Bicycle Master Plan, the 2009 Long Range Transportation Plan, and the 2004 Champaign County Greenways and Trails Plan.

Information and Outreach:

4. Provide and promote education on safe bicycling practices, and for motorists, bicyclists, and pedestrians to share the road.
5. Promote energy-efficient driving techniques.
6. Encourage residents to use walking or bicycling for at least a part of their weekly transportation needs.
7. Pursue local government actions recommended in the State of Illinois Electric Vehicle Advisory Council Report published in December 2011. 
8. Collaborate with local businesses and institutions to create programs encouraging shared commuting, car-share, bike-share, and bike purchase programs.
9. Identify specific barriers to adopting more active and energy-efficient transportation behaviors through focus groups and surveys, and provide information and resources to help overcome these barriers.

Following the work of Phase I, including the progress and knowledge gained from seeking to implement this phase, [Phase II](https://www.urbanaillinois.us/sites/default/files/attachments/ucap-p2.pdf) revised the transportation goal slightly, to simply read “Reduce Transportation Emissions from Fossil Fuels.” In looking toward long-term strategies to achieve this goal, Phase II listed the following actions and implementation strategies.

- Action 1: Evaluate existing zoning and development codes for possible integration of LEED-ND and other green development standards 
  - LEED-ND combines energy-efficient buildings with an energy-efficient street pattern and urban form to create more sustainable places.
- Action 2: Reduce single occupancy vehicle mode share from 51.6% to 40%  
  - Evaluate strategies to increase pedestrian, bike, carpool, and transit mode share
  - Implement recommendations from the Urbana Bicycle Master Plan to achieve the next level of certification as a Bicycle Friendly Community.
  - Support the creation of a pedestrian master plan 
  - Encourage transit ridership

While the Lincoln Avenue Corridor Study will plan for safe and efficient transport for all modes, the Urbana Climate Action Plan guides city infrastructure and policies to specifically discourage private automobile travel and encourage active transportation modes.

## University District Traffic Circulation Study (2013)

The Champaign County Regional Planning Commission (CCRPC) received a grant from the Illinois Department of Transportation to conduct a [traffic circulation study for the University of Illinois at Urbana-Champaign’s University District](https://ccrpc.org/transportation/university_district_traffic_circulation_study.php). It is the only study that has specifically addressed traffic circulation in the University District.

### Existing Conditions
Within the plan, the intersection of Lincoln Avenue and Florida Avnue was identified on the lists for the worst ten intersections based on the Average Control Delay for the AM and Noon Peak periods. Additionally, Lincoln Avenue and Pennslyvania and Lincoln Avenue and Nevada were identified on the list for worst intersections based on the PM Peak periods. Of the Top 30 Intersections with the Highest Crash Frequency, six of these were located along Lincoln Avenue within the area of influence:

  - Lincoln Avenue and Springfield Avenue
  - Lincoln Avenue and Green Street
  - Lincoln Avenue and Illinois Street
  - Lincoln Avenue and Nevada Street
  - Lincoln Avenue and Pennsylvania Avenue 
  - Lincoln Avenue and Florida Avenue

The highest number of crashes in the district took place at the intersection of Lincoln Avenue and University Avenue. The intersections of Lincoln Avenue at Green Street, Pennsylvania, and Florida were also on the list of top ten intersections with the highest number of crashes. The Lincoln Avenue corridor had the highest number of crashes between 2006 and 2010 for pedestrian and bicycle crashes. The plan also identified the intersection of Lincoln Avenue and Illinois Street as the fourth busiest intersection in terms of bike volume.

### Recommendations 

The plan lists quite a few reccommendations for areas within the University District, including Lincoln Avenue. Those involving the Lincoln Avenue Corridor include the following:

  Implementation of Recommended Alternatives 
  -  Improve capacity and/or signal phasing at Lincoln Avenue and Windsor Road intersection. (High Priority)
  -  Implement a road diet and two (5 ft.) bike lanes on Lincoln Avenue from Windsor Road to St. Mary’s Road. (Medium Priority)
  -  Removal of curb bumpout on the northwest corner of Lincoln Avenue/Pennsylvania Avenue intersection. (Low Priority)


More information can be found in Chapter 6 of the [University District Traffic Circulation Study](https://ccrpc.org/transportation/university_district_traffic_circulation_study.php).


## Champaign County Greenways and Trails Plan: Active Choices (2014)

The [Champaign County Greenways & Trails Plan](https://ccrpc.org/transportation/champaign_park_district_trails_master_plan/champaign_county_greenways_and_trails_plan_2014.php) is an effort coordinated by the Regional Planning Commission with local agencies to develop the county greenways and trails system. The plan is designed to provide guidance and a framework to implement Champaign County’s desire to create a bikeable, walkable, environmentally aware, and active community. 
In addition to planning for future infrastructure, the plan also documents existing trails and greenspaces along the Lincoln Avenue Corridor. More than 100 acres of green space are located along or adjacent to the corridor, including the University of Illinois Arboretum, Hallene Gateway Plaza, Illini Grove, and the university’s recreational Complex Fields. The only trail or bikeway directly on the corridor is the section of Lincoln Avenue sidepath between Iowa Street and Michigan Avenue, but the corridor connects to a variety of other trail and bike path infrastructure, including sidepaths to the west and south of Lincoln Avenue’s intersection with Florida Avenue and bike paths west of Lincoln Avenue near Florida Avenue, Ohio Street, and Iowa Street.

### Future Conditions

After analyzing current conditions, the plan lists a series of recommendations for trails or greenways that fall within the corridor study area. These projects are prioritized as high, medium, or low priority, based on a 16-point checklist found in the plan. Two projects are located directly along the Lincoln Avenue Corridor—a shared-use path from Pennsylvania Avenue to Florida Avenue, which is ranked as high priority, and a nature trail from Michigan Avenue to Pennsylvania Avenue, which is ranked as medium priority. In addition to these two projects directly on Lincoln Avenue Corridor, the following high-priority and medium-priority projects intersect with or are adjacent to the corridor. Since the plan's publication, some of the recommendations have been completed or are currently in progress. The status is indicated next to each project name below.

**High Priority -** 
- Green Street – Bike Lanes from Lincoln Avenue to Wright Street **(Completed)**
-	Green Street – Bike Lanes from Race Street to Lincoln Avenue **(Completed)**
- Oregon Street – Bike Lanes from Lincoln Avenue to Goodwin Avenue **(Not Completed)**
-	Pennsylvania Avenue– Bike Route from Race Street to Lincoln Avenue **(Not Completed)**


**Medium Priority -** 
-	Iowa Street – Bike Route from Busey Avenue to Lincoln Avenue **(Not Completed)**
-	Nevada Street – Bike Route from Lincoln Avenue to Goodwin Avenue **(Not Completed)**
-	Lincoln Avenue - Bike lanes from Florida Avenue to St Mary’s Road **(Not Completed)**
-	Florida Avenue - Side Path from Orchard Street to Lincoln Avenue **(In Progress)**
-	Pennsylvania Avenue – Bike Lanes from Lincoln Avenue to Urbana City Limits **(Not Completed)**
-	Florida Avenue Residence Hall – Shared-use path from Lincoln Avenue to Virginia Drive **(Completed)**
-	Florida Avenue – Undetermined from Lincoln Avenue to Orchard Street **(In Progress)**
-	Armory Avenue – UIUC bike path from Lincoln Avenue to Gregory Street **(Completed)**


## Urbana Bicycle Master Plan (UBMP) (2016)

The [2016 Urbana Bicycle Master Plan (UBMP)](https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/urbana_bicycle_master_plan_2016/index.php) is an update of the 2008 plan of the same name. Both plans examine the status of bicycle facilities in Urbana, in addition to the conditions that must be met to allow for higher bicycle usage in the community. The UBMP emphasizes the need for accessible bicycle facilities that increase cyclist safety. The development of the UBMP included public workshops in neighborhoods, schools, and municipal buildings where Urbana residents provided feedback on existing bicycle facilities and requested new ones.

Before making specific recommendations, the UBMP notes opportunities and constraints for cycling in Urbana. Opportunities related to the Lincoln Avenue Corridor include – 
-	Filling in the gap in Florida Avenue bike infrastructure between Lincoln Avenue and Race Street.
-	Reinforcing neighborhood connections at Illinois Street (the highest bicycle entryway to the university campus, through the Lincoln Avenue intersection) and West Washington Street (filling in a gap in cycling infrastructure on Busey Avenue, so that the Washington Street bike infrastructure can connect to existing infrastructure on Iowa Street and campus).

Constraints related to the Lincoln Avenue Corridor include - 
-	The difficulty of biking along or across arterial roads, of which Lincoln Avenue is named specifically.
-	Brick roads making routes difficult to cycle on, with Nevada Street east of Lincoln Avenue named as one example.
-	Offset intersections making it hard for cyclists to cross roads safely, with the offset of Oregon Street at Lincoln Avenue named as an example. 

  {{<image src="bike rec core map.PNG"
  link="/destination"
  alt="A map showing the bike plan's recommendations located in the core of Urbana"
  caption="Map of bicycle infrastructure recommendations in Urbana's core"
  attr="CUUATS" attrlink="https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/urbana_bicycle_master_plan_2016/index.php"
  position="right">}}

Recommendations within the Lincoln Avenue Corridor Study area include –
-	***Constructing a nature trail with wayfinding signage through the Illini Grove, between Michigan Avenue and Pennsylvania Avenue. This nature trail would connect to the existing sidepath on the west side of Lincoln Avenue, between Iowa Avenue and Michigan Avenue.*** Currently, this sidepath narrows to a standard sidewalk, bounded by a retaining wall on the west, at Michigan Avenue. Construction of the nature trail would allow cyclists to continue to safely travel on the west side of Lincoln Avenue, without being required to either cross the street, navigate into traffic, or ride on a narrow sidewalk with pedestrians.
-	***Widening existing sidewalk on the west side of the road between Pennsylvania Avenue and Florida Avenue, to an 8-foot-wide sidepath.*** Along with the previous recommendation, this would create continuous off-street bike infrastructure from Iowa Street to Florida Avenue, encompassing the entire southern half of the study area and connecting to existing off-street bike infrastructure south and west of the intersection of Lincoln Avenue and Florida Avenue, as well as the future shared use path east of the intersection.
-	***Adding bicycle warning signs at Lincoln Avenue and Iowa Street,*** to alert drivers to cyclist crossings.
-	***Adding sensors to the stoplights at the Lincoln Avenue intersections with Illinois Street and Pennsylvania Avenue intersection.***
-	***Constructing additional bike parking at the intersection of Lincoln Avenue and Nevada Street.***


In addition to these recommendations on the Lincoln Avenue Corridor, several recommendations are adjacent or connected to the corridor, including – 
-	***A sidepath on Florida Avenue east of Lincoln Avenue.*** This recommendation was rated as a high-priority infrastructure recommendation and one of the top ten recommendations of the plan. 
-	***Bike wayfinding for Florida Avenue paths west of Lincoln Avenue.***
-	***Bike wayfinding on Illinois Street between Lincoln Avenue and Coler Avenue.*** This area was also named as an opportunity area for neighborhood connections, as Illinois Street is the highest bicycle entryway to the university campus, with over 800 daily cyclist crossings at the Lincoln Avenue intersection.
-	***Adding a bike lane to Pennsylvania Avenue west of Lincoln Avenue.***
-	***Making Iowa Street east of Lincoln Avenue a bike route with wayfinding signage.***




## LRTP 2045 (2018)]

The [Long Range Transportation Plan (LRTP)](https://ccrpc.gitlab.io/lrtp2045/) is a federally mandated document that is updated every five years. This document looks at the projected evolution of pedestrian, bicycle, transit, automobile, rail, and air travel in the region over the next 25 years. Champaign County Regional Planning Commission (CCRPC) completed the most recent update of the region’s LRTP for the horizon year 2045 in 2019 (CCRPC is currently in the process of completing LRTP 2050). The LRTP 2045 planning process provided multiple opportunities for public involvement. Input relevant to the Lincoln Avenue Corridor Study is presented below.

### LRTP 2045 Goals

The LRTP 2045 has the following five overarching long-term goals: safety, multimodal connectivity, equity, economy, and environment. These goals and their objectives are based on a combination of the Federal transportation goals, State of Illinois transportation goals, local knowledge, current local planning efforts, and input received from the public. The goals are explained in detail on the [LRTP 2045 webpage](https://ccrpc.gitlab.io/lrtp2045/goals/). 

{{<image src="LRTP Vision Board_22x34_reduced.jpg"
  link="/destination"
  alt="Diagram describing the LRTP 2045 Vision, the goals for the LRTP, as well as a map of example projects"
  caption="LRTP 2045 Vision Diagram"
  attr="CUUATS" attrlink="https://ccrpc.gitlab.io/lrtp2045/vision/futureprojects/"
  position="full">}}


### LRTP 2045 Public Input Related to the Lincoln Avenue Corridor Study

Public comments about locations along the Lincoln Avenue Corridor were nearly evenly split between all modes of travel (Walking or Wheelchair, Bicycle, Bus, Automobile). The [LRTP 2045 online map](https://ccrpc.gitlab.io/transportation-voices-2045/) documents the following public comments on Lincoln Avenue in the study area:
- Green Street
  - Bicycle: “The markings for the bike line on west-bound Green St at Lincoln Ave are unusual - they're a solid line rather than a dashed line indicating that turning cars should merge. As currently marked, cars turning right are seemingly directed to turn across the straight-through traffic in the bike lane.”
  - Bicycle: “When traveling west bound on Green St at the Lincoln Ave signal, in the absence of cars *also going west*, cyclists *never* get a green light. Cars going east on Green or turning north onto Lincoln get a one-way green light, and then the light changes back to green for Lincoln's N/S traffic.”
- Oregon Street
  - Bicycle: Repair or replace bike lanes or paths
- Nevada Street
  - Walking or Wheelchair: “Pedestrian signals only activated if button is pressed before the lights change. Once the lights change, you're forced to wait for another round of lights.”
- Ohio Street
  - Bus: Amenities at this stop function well
- Indiana Avenue
  - Automobile: Fix uneven pavement
- Michigan Avenue
  - Walking or Wheelchair: “I think the crosswalks on Lincoln should include flashing lights like the one on Windsor Road.”
  - Walking or Wheelchair: “Hard to cross Lincoln Ave between Florida Ave and Nevada St. Need a better system for cars to slow down when pedestrians present.”
- Pennsylvania Avenue 
  - Bicycle: “Pennsylvania Ave is in desperate need of resurfacing and adding some striped bike lanes to improve traffic flow. The street is not very fun to bike on as is”
  - Bus: “Why don't busses stop here? (Silver)”
- Delaware Avenue 
  - Automobile: Fix uneven pavement

### Future Projects: Fiscally Constrained

As part of the LRTP, the Federal Highway Administration requires a listing of the fiscally constrained projects that are part of the overall vision for the urbanized area. Fiscally constrained projects are those that have either guaranteed or reasonably guaranteed funding secured for the completion of the project. A separate federally required document for the region, the Transportation Improvement Program (TIP) lists fiscally constrained transportation projects anticipated to be constructed in the metropolitan planning area during the next four years. The program reflects the goals, objectives, and recommendations from the 25-year Long Range Transportation Plan (LRTP) in the short term. The following fiscally constrained projects are located within the Lincoln Avenue Corridor study area:
- Lincoln Avenue and Springfield Avenue (South of University Avenue and North of Green Street) - ADA accessible ramps, sidewalk repair and replacement, refuge islands, roadway surfaces, curb and gutter repair and replacement
- Florida Avenue (from west of Lincoln Avenue to east of Vine Street) - Complete street reconstruction/rehabilitation

### Future Projects: Local and Unfunded
Each agency in the region has its own set of transportation priorities and goals to improve mobility in their respective jurisdiction, alone and in conjunction with surrounding jurisdictions. The following list includes local project priorities for the future:
- Pennsylvania Avenue (from City Limit to Lincoln Avenue) - Reconstruction with bike sharrows, sidewalk, and ramp improvements
- Lincoln Avenue (from Florida Avenue south; approximately 400 ft) - Reconstruction
- Lincoln Avenue (from Nevada Street to Pennsylvania Avenue) - Safety improvements

For more information on these 2045 Vision Future Projects, visit the LRTP 2045 webpage. 



## University of Illinois at Urbana-Champaign Campus Master Plan (2018)

The stated purpose of the [University of Illinois at Urbana-Champaign Campus Master Plan](https://www.uocpres.uillinois.edu/resources/uiucplan) is “first, to protect and celebrate the legacy of the University of Illinois at Urbana-Champaign, in its history, its stature, and its sense of place; second, to look forward and provide a framework to guide campus growth, set collective priorities, and manage future investment.” In its planning for the campus built environment, this document guides the development of transportation, green space, residential, and academic facilities within, adjacent to, or otherwise impacting the Lincoln Avenue Corridor. 

The Facilities and Campus Analysis section of the plan notes that the Lincoln Avenue Corridor is a primary campus thoroughfare, with more than 20,000 vehicles traveling along the corridor daily. In identifying areas of pedestrian concern on campus, the Intersection of Lincoln Avenue and Green Street is noted as an area of primary concern, while the intersections at Nevada Street and Florida Avenue are also noted for their crash frequency or conflict between vehicles and pedestrians. While acknowledging that the university is a bronze-level Bicycle Friendly University, the plan also points to Lincoln Avenue as a major opportunity to fill gaps in bike infrastructure. Lincoln Avenue north of Iowa Street and the path between Lincoln Avenue and Dorner Drive south of Lincoln Avenue Residence Hall are both identified as specific unaddressed gaps in cycling infrastructure. 

  {{<image src="uiuc master campus gateways.PNG"
  link="/destination"
  alt="Map of showing major gateways to enter the University of Illinois campus"
  caption="Map of identified campus gateways"
  attr="University of Illinois" attrlink="https://www.uocpres.uillinois.edu/resources/uiucplan"
  position="right">}}

This section of the plan also identifies two specific cycling infrastructure investments that have been proposed in other plans—an off-street path through Illini Grove between Indiana Avenue and Pennsylvania Avenue, and the conversion of the west sidewalk between Pennsylvania Avenue and Florida Avenue into a shared-use path. In line with this analysis, the plan proposes that the entire Lincoln Avenue Corridor should be converted into a bicycle route, seeking to avoid the frequency of vehicle-to-bicycle crashes along this corridor. Automobile-related investments that are called for along the corridor include the identification of Green Street and Lincoln Avenue as one of three areas on campus that are most in need of future structured parking. 

Other planned investments that are adjacent to or may otherwise affect the corridor include – 
-	***The improvement of major gateways to campus.*** Along Lincoln Avenue, the entrance at Illinois Street was deemed to be good, but in need of improvement, while the gateways at Green Street and Florida Avenue were deemed to need reconsideration and redevelopment.
-	***The conversion of Illinois Street into “The Illinois Experience,”*** with a future signature building on Lincoln Avenue, a new pedestrian walkway and linear park along Illinois Street, an associated narrowing of vehicle road space, renovations to Illinois Street Residence Hall (ISR), expansion of ISR dining facilities, construction of new residential facilities adjacent to ISR, closure of Gregory Street between Illinois Street and Oregon Street, and construction of additional collections and storage space for the Spurlock Museum of World Cultures.
-	***Other residential construction in addition to the ISR facilities,*** including a new undergraduate residence hall north of Lincoln Avenue Residence Hall, redevelopment of the university dance studies for a potential mixed-use development, and redevelopment of Orchard Downs, including replacement of units, the addition of 156 units to replace Ashton Woods upper level and graduate housing, and the possible long-term development of the land directly south of Orchard Downs for a mixed-use or residential facilities.
-	***The inclusion of Lincoln Avenue south of the study corridor in the “ACES Legacy Corridor”,*** concentrating university agriculture facilities along a single spine. The development of this corridor would reinforce Lincoln Avenue as an important corridor for university shuttles, especially those accessing the agricultural facilities to the south.


## Champaign-Urbana Pedestrian and Bicycle Survey (PABS) (2019)

The [Champaign-Urbana Metropolitan Planning Area (MPA) Pedestrian and Bicycle Survey (PABS)](https://ccrpc.gitlab.io/pabs/) was administered in 2019 by CUUATS. This survey was carried out with financial support from the Illinois Department of Transportation (IDOT) as part of a larger grant to expand local efforts to collect and monitor active transportation activity and infrastructure in the region. Questionnaires were distributed to households in the Champaign-Urbana MPA via mail and asked respondents about their walking and cycling habits. 

{{<image src="bike ped survey.PNG"
  link="/destination"
  alt="Image of the webmap from the survey website, showing favorite and least favorite walking and cycling routes"
  caption="Map of favorite and least favorite walking and cycling routes"
  attr="CUUATS" attrlink="https://ccrpc.gitlab.io/pabs/responses/"
  position="right">}}

Topics covered in the 2019 PABS survey included:
-	General purposes of walking and bicycling trips
-	Frequency of walking and bicycling
-	Reasons for not walking or bicycling
-	Favorite and least favorite bicycling and walking routes in the community

Survey results showed that respondents listed the Lincoln Avenue Corridor as a least favorite route for both cycling and walking, and no respondents listed the corridor as a favorite route. 


## C-U Urban Area Safety Plan (2019)

The [Champaign-Urbana Urban Area Safety Plan (CUUASP)](https://ccrpc.org/transportation/champaign-urbana_urban_area_safety_plan/index.php) is a guideline for safety stakeholders to identify and implement safety improvements and programs to reduce fatalities and serious injuries on area roadways. Champaign-Urbana follows the state goal outlined in the Illinois Strategic Highway Safety Plan 2017 (ILSHSP) to reduce roadway crash fatalities to zero.

Among the high-priority locations identified in the CUUASP, the intersections of Lincoln Avenue with Ohio Street and Iowa Street were both identified as high-priority unsignalized intersections. In the safety plan’s period of analysis (2012-2016), the Ohio Street intersection was the location of five A-Injury (incapacitating) crashes and 30 total crashes, while the Iowa Street intersection was the location of three A-Injury crashes out of 24 total crashes. 


### Safety Plan Implementation

Following the CUUASP and Champaign County Rural Safety Plan, CUUATS created the Safety Plan Implementation document to work toward implementing recommendations from the Urban and Rural Safety Plans by conducting detailed safety analyses for specific locations. Candidate intersections and segments were selected from the high-priority locations in the two safety plans. As part of the safety analysis, detailed recommendations for improvements were proposed, allowing the responsible agencies to make informed decisions to improve safety at the identified locations.

The intersection of [Lincoln Avenue and Ohio Street](https://ccrpc.gitlab.io/ccspi-high-priority-locations/urban-unsignalized-intersections/lincoln-and-ohio/) was identified as one of the 18 analysis locations for this document. While the focus of the analysis was at this specific intersection, this document notes that –


*Crashes at the other three stop-controlled intersections on Lincoln Avenue between Iowa Street and Michigan Avenue were also reviewed in this study, as the entire section of Lincoln Avenue near campus has been problematic for an extended period. Rear-end crashes were prevalent at all the intersections along the Lincoln Avenue corridor between Ohio Street and Michigan Avenue. However, the intersection of Lincoln Avenue and Ohio Street had a higher number of pedestrian and pedalcyclist crashes than the other intersections from 2015-2019.*

{{<image src="safety plan crashes.png"
  link="/destination"
  alt="Diagram showing the crashes at Lincoln Avenue and Ohio Street by type and specific location"
  caption="Crash Diagram for Lincoln Avenue and Ohio Street"
  attr="CUUATS" attrlink="https://ccrpc.gitlab.io/ccspi-high-priority-locations/urban-unsignalized-intersections/lincoln-and-ohio/"
  position="right">}}

In the updated study period of 2015 to 2019, the intersection analysis documents 28 total crashes, of which two are A-Injury (incapacitating), three are B-Injury (clearly evident, but not incapacitating injury), and four are C-Injury (not evident to observers, but reported by the crash victim). Twenty-one of these crashes were vehicle-to-vehicle rear-end crashes, three each were vehicle collisions with pedestrians or cyclists, while the remaining crash was an angle (side) vehicle crash. All of the cyclist and pedestrian crashes resulted in injury, including both victims of A-Injury crashes, who were pedestrians.

In response to crash data and road conditions, the implementation document recommends three proposed countermeasures – 
1.	Installing pedestrian refuge medians and curb bump-outs on Lincoln Avenue. This will improve the visibility of pedestrians and bicyclists, reduce the time that they are in the street, and encourage slower vehicle speeds.
2.	Installing barriers or fences along the sidewalk to encourage the use of crosswalks.
3.	Conducting a detailed Lincoln Avenue corridor study. The City of Urbana and CUUATS submitted a grant application to IDOT in March 2022, requesting funding to conduct a detailed corridor study of Lincoln Avenue between Green Street and Florida Avenue. The goal of the corridor study is to increase safety and mobility in this segment of Lincoln Avenue.

## Illinois Climate Action Plan (2020)

Building upon previous Illinois Climate Action Plans from 2010 and 2015, the stated goal of the [University’s 2020 plan](https://sustainability.illinois.edu/campus-sustainability/icap/) is to “further our pursuit of carbon neutrality while striving for holistic, campuswide sustainability and strengthening community resilience. Poised at the outset of a new decade, iCAP 2020 represents our continuous commitment to environmental stewardship, to honor the work of the recent past and advance toward a safe and sustainable future.”

In a similar manner to the Urbana Climate Action Plan, the University’s plan does not specifically address the Lincoln Avenue Corridor. However, the plan does include several transportation-related objectives which are relevant to the Lincoln Avenue Corridor Study. These relevant objectives include – 

  {{<image src="campus sustainability graph.PNG"
  link="/destination"
  alt="A graph showing single occupancy vehicle trips as a percentage of trips made by university staff, with observed decreases over the last decade and continued decreases aimed for in the plan "
  caption="University staff single-occupancy vehicle mode share"
  attr="University of Illinois" attrlink="https://sustainability.illinois.edu/campus-sustainability/icap/"
  position="right">}}

- Objective 3.2 - Increase the Pavement Condition Index (PCI) for university-owned roads so the average PCI score is at least 65 by FY25 and at least 70 by FY30.
- Objective 3.3 - Establish an Electric Vehicle Task Force to identify key goals for supporting the use of electric vehicles on and off campus by FY22.
- Objective 3.4 - Reduce driving on campus and report the percentage of staff trips made using single-occupancy vehicles from 60% to 50% by FY25 and 45% by FY30. 
  - Objective 3.4.1 - Develop a Commuter Program (Bus, Bike, and Hike) for faculty and staff. Register 100 people by FY24 and 500 people by FY30.
  - Objective 3.4.2 - Continue to implement the 2014 Campus Bike Plan.
  - Objective 3.4.3 - Establish telecommuting policies for the campus by FY24. 

These objectives seek to advance campus transportation sustainability and resilience by improving road infrastructure, electrifying vehicles, encouraging active transportation modes, and reducing commuting needs.


## Urbana Pedestrian Master Plan (2020)

The [Urbana Pedestrian Master Plan](https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/2020_urbana_pedestrian_master_plan_(final_report).php) serves as a guide to the City of Urbana, aiming to improve pedestrian safety and plan for infrastructure and programs to create a safe, walkable community. The four central goals of the plan are as follows:

-	**Goal #1:** Improve Urbana’s pedestrian infrastructure to enable and encourage all residents and visitors to choose to walk to destinations.
-	**Goal #2:** Invest in Urbana’s pedestrian resources (infrastructure, education, encouragement, and enforcement) to improve all substandard areas, especially areas of concentrated racial or ethnic minorities, lower-income areas, and transit-dependent populations.
-	**Goal #3:** Eliminate fatal and serious pedestrian/vehicle crashes.
-	**Goal #4:** Create healthy, sustainable, aesthetically pleasing, and economically stimulating community streetscapes and natural landscapes that both inspire and facilitate walking.

{{<image src="ped numbers map.PNG"
  link="/destination"
  alt="Map showing the number of pedestrian crossings at major intersections in Urbana"
  caption="Map of pedestrian crossings at major intersections in Urbana"
  attr="CUUATS" attrlink="https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/2020_urbana_pedestrian_master_plan_(final_report).php"
  position="right">}}

Within the Existing Conditions chapter of the plan, Lincoln Avenue between Oregon Street and Florida Avenue is listed as the area in Urbana with the highest population density. Located on the boundary of the University District, the study area also includes the greatest density of transit stops. The intersection of University Avenue at Lincoln Avenue is one of the highest peak hour traffic intersections in Urbana, creating barriers to the pedestrian network along the corridor and beyond. 



The Champaign County Regional Planning Commission (CCRPC) and the City of Urbana hosted ten public events to solicit public input on walking in Urbana. Findings from the public input sessions showed that Lincoln Avenue was listed as one of the top three streets mentioned by participants, along with University Avenue and Vine Street.



Following public input and analysis of existing conditions, the pedestrian plan made the following recommendations on or along the Lincoln Avenue Corridor – 
-	Bevel the sidewalks (i.e. smooth out vertical changes in the sidewalk surfaces) on both sides of Lincoln Avenue, between Nevada Street and Iowa Street.
-	Improve the pedestrian signal on the east side of the intersection of Lincoln Avenue and Florida Avenue.
-	Install a sidepath on the south side of Florida Avenue, between Lincoln Avenue and Race Street
-	Conduct crossing enhancement analysis at the intersections of Lincoln Avenue with Indiana Street and Pennsylvania Avenue, due to reported pedestrian difficulty crossing these intersections. 

In the Implementation section of the plan, the sidewalk beveling and pedestrian signal improvements above were both listed as Top Priority Recommendations.



## Campus Landscape Master Plan (2022)

The [Campus Landscape Master Plan](https://fs.web.illinois.edu/Insider/2023/01/06/campus-landscape-master-plan-our-treasured-landscape-will-thrive/) presents a vision for the overall campus landscape, utilizing specific design guidelines to achieve the framework and principles outlined in the Campus Master Plan. In the plan, there are multiple recommendations located within the University District that will impact the Lincoln Avenue Corridor. 

  {{<image src="uiuc landscape map.PNG"
  link="/destination"
  alt="Map of Illinois Climate Action Plan projects relevant to the landscape plan, including bee habitat along Lincoln Avenue"
  caption="Map of Illinois Climate Action Plan projects relevant to the landscape plan"
  attr="University of Illinois" attrlink="https://fs.web.illinois.edu/Insider/2023/01/06/campus-landscape-master-plan-our-treasured-landscape-will-thrive/"
  position="right">}}

-	Lincoln Avenue is noted as a concern for pedestrians and cyclists; thus the plan suggests converting all roadways within the district (i.e. Illinois, Nevada, etc.) into green streets, which will benefit the corridor by improving safety and mobility along the streets and across Lincoln Avenue. Aspects of these green streets include bioretention/rain gardens and gathering spaces. 
-	Lincoln Avenue serves as the eastern gateway into the University District. The plan encourages creating pedestrian scale gateways that improve overall aesthetics and safety, specifically at Green/Lincoln, Illinois/Lincoln, and Florida/Lincoln. This also includes repairing failing infrastructure and upgrading plant design at the Hallene Gateway.
-	The intersections of Lincoln Avenue with Oregon Street and Lincoln Avenue with Nevada Street are two locations identified as requiring aesthetic and safety improvements.
-	The connection of the University District to the Arboretum and Athletics District (along the Lincoln Avenue Corridor) lacks quality pedestrian infrastructure. There is also a lack of placemaking within this district, as well as a gateway into the campus from the south. 
-	The Campus Master Plan proposed the ACES Legacy Trail to celebrate the university’s agricultural legacy, better connect the campus with the South Farms/research areas and improve entry into campus. This would make Lincoln Avenue the north-south “eco-corridor” of campus. The trail begins at the intersection of Lincoln Avenue and Florida Avenue and continues south on the Lincoln Avenue Corridor towards Curtis Road, before looping back up towards campus. To ensure the connectivity and accessibility of this trail system, South Lincoln should include an off-street, multi-use trail and on-street bicycle routes. 
-	The plan recommends integrating a complete streets approach to Kirby/Florida Avenue, from Neil Street to Lincoln Avenue. This would include a new gateway into the university, as well as the implementation of a green street along this corridor. 




## Transportation Improvement Program (TIP), Fiscal Years 2023-2028

The [Transportation Improvement Program (TIP)](https://ccrpc.org/transportation/transportation_improvement_plan_(tip)/transportation_improvement_program_(tip)_fy_2023-2028.php) is a federally required document that lists local transportation projects prioritized for construction in the next four to six years. The [TIP Project Database](https://maps.ccrpc.org/tip-2023-2028/) reflects transportation projects throughout the Metropolitan Planning Area that are scheduled for implementation and have relatively secure funding. The [TIP Illustrative Projects Table](https://data.ccrpc.org/dataset/tip-fy-2023-2028/resource/3b83a742-f695-4ae0-a40c-26124b6c585e) reflects transportation projects that have been identified as priorities but do not yet have funding.

### TIP FY 2023–2028 Projects in the Lincoln Avenue Corridor Study Area

While there are no funded projects in the TIP 2023-2028 along the Lincoln Avenue Corridor, the corridor does intersect with two funded projects along its edges. Resurfacing and roadway rehabilitation along Lincoln Avenue and Springfield Avenue (TIP Project ID: UR-20-06) has been completed. This project is located between University Avenue and Green Street, along the corridor’s northern edge. Urbana is the sole agency responsible for this project, which is funded by state and local motor fuel taxes. Along the corridor’s southern edge, the recently completed [Florida Avenue Corridor Study](https://ccrpc.gitlab.io/florida-ave/) (TIP Project ID: RPC-21-03) is also listed. This project identified multimodal safety and mobility improvements on Florida Avenue between Lincoln Avenue and Vine Street and was funded by State Planning and Research Grants and matching funds from the Illinois Department of Transportation (IDOT). [Relevant recommendations from this study](https://ccrpc.gitlab.io/florida-ave/implementation/implementation_plan/) include the construction of a shared-use path south of Florida Avenue from Lincoln Avenue to Race Street, improvement of pavement, sidewalks, and lighting on Florida Avenue from Lincoln Avenue to Vine Street, and upgrading existing traffic signals at the Florida Avenue intersections with Lincoln Avenue and Orchard Street. The implementation of Florida Avenue improvements (TIP Project ID: UR-23-06) is scheduled in the TIP for the fiscal year 2024. Because of its consistency with federal and regional transportation goals, this project has been approved to utilize federal Surface Transportation Block Grant (STBG) funding, in addition to Urbana Motor Fuel Tax funds and COVID relief funds.

Roadway and safety improvements along the Lincoln Avenue corridor have been prioritized by the City of Urbana and the University of Illinois under the [TIP Illustrative Projects Table](https://data.ccrpc.org/dataset/tip-fy-2023-2028/resource/3b83a742-f695-4ae0-a40c-26124b6c585e), but these improvements have not yet been designated funding. Other adjacent projects in the illustrative projects table include reconstruction of Florida Avenue from Wright Street to Vine Street, pavement restoration or replacement on Pennsylvania Avenue from the western city limit to Lincoln Avenue, reconstruction of Lincoln Avenue from Florida Avenue south (approximately 400 ft), and continuation of the Lincoln Avenue roadway and safety improvements west along Pennsylvania Avenue and north along Dorner Drive.