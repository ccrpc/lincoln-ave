---
title: Transportation
draft: false
weight: 16
---

Lincoln Avenue is one of the most important north-south corridors for the City of Urbana and the University of Illinois Campus District. The corridor connects I-74 with the University District and its associated academic facilities, retailers, residential areas, and more. The annual average daily traffic (AADT) along the corridor within the area of influence is as high as 13,100 vehicles. This corridor is utilized by all major modes of transportation, including pedestrians, bicyclists, transit, passenger vehicles, and freight vehicles. To plan for future transportation improvements, it is important to look at the existing transportation conditions and understand current traffic operations, infrastructure conditions, facility geometries, and safety issues. 

This section covers the following topics: existing geometric features of roadways and intersections, traffic flow data, traffic growth characteristics, intersection and roadway segment operational conditions, crash analysis, access management, transit service, and pedestrian and bicycle facilities.


## Geometric Features of Roadways and Intersections 

Existing geometric features of the Lincoln Avenue corridor include roadway functional classification, lane configurations, roadway widths, presence of shoulders, roadway surface conditions, intersection geometry, and intersection control types. This data is collected and maintained by the City of Urbana, University of Illinois, and CUUATS staff.


### Functional Classification 

Functional classification is a process of categorizing roads primarily based on mobility and accessibility. Mobility relates to operating speed, level of service, and riding comfort. It is a measure of the expedited traffic conditions between origin and destination. Accessibility identifies travel conditions with increased exit and entry locations. The Illinois Department of Transportation (IDOT) uses the following classifications for roadways throughout the state:

-	**Principal arterials** have the highest level of mobility and the lowest level of accessibility. Interstates and other major arterials serve trips through urban areas and long-distance trips between urban areas.

-	**Minor arterials** serve shorter trips between traffic generators within urban areas; they have more access but a lower level of mobility than interstates and other principal arterials.

-	**Major and minor collectors** provide both mobility and access by gathering trips from localized areas and feeding them onto the arterial network.

-	**Local streets** are lower-volume roadways that provide direct land access but are not designed to serve through-traffic.


Lincoln Avenue is a minor arterial throughout the study area and intersects with the other minor arterials of Green Street and Florida Avenue.

<iframe src=" https://maps.ccrpc.org/lacs-road-classification/" width="100%" height="600" allowfullscreen="true"></iframe>


### Lane Configuration Along Lincoln Avenue 

This section of Lincoln Avenue includes several different lane configurations. From Green Street to California Avenue, the roadway includes two travel lanes in each direction and a center turn lane. From California Avenue to the west leg of Nevada Street, the roadway includes two travel lanes in each direction with a center median. From Nevada Street to Pennsylvania Avenue, the roadway includes one travel lane in each direction, with a center turn lane and a painted buffer. From Pennsylvania Avenue to Florida Avenue, the lane confiugrations change rabidly within a three-block length, starting with one travel lane in each direction and a center turn lane, widening to include two lanes in each direction and a center median, and ending at Florida Avenue with three southbound lanes and two northbound lanes. 


### Roadway Surface Type 

Lincoln Avenue pavement is covered with asphalt over portland cement concrete (APC). Only the street section between California Avenue and Nevada Street is covered with asphalt over asphalt concrete (AAC).

-   Green to California: APC (Asphalt over Portland Cement Concrete)

-   California to Nevada: AAC (Asphalt over Asphalt Concrete)

-   Nevada to Florida: APC (Asphalt over Portland Cement Concrete)

### Roadway Surface Conditions

The City of Urbana’s pavement management system includes regular inspections and a Pavement Condition Index (PCI) to monitor roadway surface conditions in the city. The PCI is a numerical index between 0 to 100 that indicates the general condition of a pavement section. Higher index value indicates better pavement condition. According to the 2022 Pavement Conditions Index, the following scores exist for segments along the project area:

 -  Green to California: 30

-   California to Nevada: 39

-   Nevada to Pennsylvania: 25

-   Pennsylvania to Delaware: 31

-   Delaware to Florida: 19

Overall, the segments in this corridor averaged a PCI of 29 out of 100, which is considered very poor. The PCI score for the full length of Lincoln Avenue is 54, indicating the the roadway surface in this corridor is significantly poorer than in other locations along Lincoln Avenue.


### Intersection Control Types

With high traffic volumes, traffic signal timing becomes an important element in limiting congestion and keeping traffic flowing. Traffic along the Lincoln Avenue corridor flows well during the AM and PM peak hours, due to the traffic signal coordination efforts by the City of Urbana. There are 16 intersections on Lincoln Avenue within the study area, this analysis focuses on the 5 signalized intersections that include intersection control for all directions: Lincoln Avenue at Green St, Illinois St, Nevada St, Pennsylvania St, and Florida Ave. The maps below show the locations of these signalized intersections, as well as the crossings along Lincoln Avenue that exist between unsignalized intersections. 

#### Functional Classification and Intersection Control

<p float="left">
  <img src="Road Classification.png" width="100%"
  alt="Map showing functional classification and intersection control types for roads along the Lincoln Avenue Corridor" />



## Access Management

Access management is a systematic means of balancing the access and mobility requirements of streets and roads. [It involves the coordination between land use and transportation development practices to manage the location, design, and operation of driveways, median openings, and street connections to roadways.](http://www.teachamerica.com/accessmanagement.info/pdf/348NCHRP.pdf) Implementing access management guidelines helps provide a safe and efficient transportation system. Access management along the Lincoln Avenue corridor was evaluated by four criteria outlined in the [CUUATS Access Management Guidelines (2013)](https://ccrpc.org/transportation/access_management_guidelines.php): intersection functional area, signalized access spacing, unsignalized access spacing, and driveway spacing. Several locations along the corridor fall short of the recommendations associated with one or more of the four criteria.


### Intersection Functional Area

Intersections are comprised of physical and functional areas. The physical extent of an intersection is the fixed space within the corners of the intersection. The functional area extends upstream and downstream of the intersection and includes the roadway length required for vehicle storage and maneuvering. The upstream functional area of an intersection depends on the vehicle queuing (storage length), driver perception-reaction time, and the distance required for decelerating or stopping. The downstream functional area of an intersection is the distance required by the driver to clear the intersection and be able to perceive and react to a conflict downstream of the intersection.


Access connections, such as driveways or median openings, are not recommended in the intersection functional area because they could create conflict points, which the driver approaching or exiting an intersection may not be able to negotiate safely. The table below lists the desirable and limiting (necessary) values for the upstream and downstream functional distances.


<rpc-table
url="intersection function area.csv"
table-title="Desirable and Limiting Functional Distances on Lincoln Avenue, 2022"
source="Access Management Guidelines for the Urbanized Area, 2013">
</rpc-table>


### Signalized Access Spacing

Signal spacing is defined as the distance between two successive at-grade signalized intersections, measured between the closer curb/edge of the intersection. Optimum spacing of the intersections has a significant impact on safety, traffic operations, and level of service in the corridor. Each additional traffic signal reduces travel speed while increasing travel time and delay. The optimal spacing of signals depends on the cycle length and the posted speed limits.


According to CUUATS Access Management Guidelines, the recommended minimum distance between two signalized intersections on a minor arterial such as Lincoln Avenue is 1,320 feet. The spacing between the signal at Green Street and the signal at Illinois Street is about 560 feet, or 42% of the recommended distance, which should be considered when evaluating future changes along the corridor.


### Unsignalized Access Spacing

Unsignalized intersections are more common than signalized intersections and include stop-controlled, yield-controlled, and uncontrolled intersections. According to CUUATS Access Management Guidelines, the spacing between two consecutive unsignalized accesses along a minor arterial should be at least 660 feet. The space between unsignalized intersections is less than the minimum recommendations for most of the unsignalized intersections in the study area. For example, there are four unsignalized intersection on one 650 ft stretch of Lincoln Avenue between W California Avenue and Nevada St., which isn’t a desirable condition for managing access spacing.


### Driveway Spacing

Driveway spacing is the distance between two adjacent driveways and is measured from the nearest edge of one driveway to the nearest edge of the next driveway. Recommended driveway spacing distance is longer on collectors and arterials compared to local roadways. Lincoln Avenue is classified as a Minor Arterial; the CUUATS Access Management Guidelines recommends the following for minor arterials:
  - Commercial roadways with ADT volumes between 2,500 and 12,000 (collectors and minor arterials)- The unsignalized access spacing criterion should be followed, unless:

    - Traffic Impact Analysis determines otherwise.
    - Restricted by lot width.
    - Engineering judgment states otherwise.
  
  - Right in/out driveways can be provided on a minor arterial (6,000 – 12,000 ADT) at 330 feet spacing.

## Traffic Flow Data

### Turning Movement Counts
Peak hour turning movement counts help quantify existing traffic operation conditions at different intersections along the corridor. Vehicle turning movement counts were collected by RPC staff at six major intersections along the Lincoln Avenue corridor. The intersections include Lincoln Avenue at Green Street, Illinois Street, Nevada Street, Ohio Street, Pennsylvania Avenue, and Florida Avenue. These volumes were collected in the Spring of 2023 from the hours of 7:00 AM 7:00 PM. From the data collected, the peak travel periods were determined to be 7:45 AM -8:45 AM in the morning and 4:30 PM to 5:30 PM in the evening. The graphic below depicts the peak hour volumes for the six main intersections within the study area.

<p float="center">
  <img src="Graphic 1 - Fully Straightened Turning Movement Counts.png" width="150%"
  position="full"
  alt="Map showing peak hour traffic volumes for major intersections along the Lincoln Avenue corridor" />
</p>

### 24-Hour Traffic Volume
The following table documents the average daily traffic counts (ADT) along this corridor in 2016 and 2021, along with the percentage change over this time period. Between 2016 and 2021, ADT decreased throughout the study corridor by approximately 19-25 percent. The largest decrease in ADT volumes along the study corridor was north of Pennsylvania Avenue, and south of Iowa Street.

<rpc-table
url="24-hour traffic volume.csv"
table-title="24-Hour Traffic Volume, 2016-2021"
source="https://www.gettingaroundillinois.com/">
</rpc-table>

The following figures show hourly (24-hour) traffic flow variations along the Lincoln Avenue corridor in three segments: South of Green Street, North of Indiana Avenue, and North of Florida Avenue. As shown in these figures, northbound and southbound traffic throughout the study corridor is similar in volume throughout the day, with a slight preference for northbound travel during the AM peak period towards the center of the University of Illinois campus, and for southbound travel during the PM peak period away from the University of Illinois campus.


### Speed Study
Speed data was collected by IDOT along the Lincoln Avenue Corridor in 2021. The figures below depict the average speeds of vehicles and the 85th percentile traveling speed through the study area corridor. The main segments analyzed were from south of Green Street to north of Indiana Avenue and from north of Indiana Avenue to north of Florida Avenue. 

It should be noted that most of Lincoln Avenue within the study area corridor has a posted speed limit of 30 miles per hour (mph), but there is a section between Iowa Street and Michigan Avenue where the speed limit drops to 25 mph. 

The average travel speed south of Green Street was higher than the posted speed limit for northbound traffic, and one mile below the speed limit for southbound traffic. The average travel speed north of Indiana Avenue was higher than the posted speed limit for both northbound and southbound traffic. Northbound traffic average speed did not increase between Indiana Avenue and Green Street, however southbound traffic average speed increased by approximately 3mph between Green Street and Indiana Avenue. The average travel speed north of Florida Avenue is below the posted speed limit of 30 mph for both northbound and southbound traffic. Northbound traffic average speed increased between Florida Avenue and Indiana Avenue, while southbound traffic average speed decreased from Indiana Avenue to Florida Avenue.

<rpc-chart url="speed_data_average.csv"
  chart-title="Average Speeds Through the Corridor"
  x-label="Segment"
  y-label="Speed"
  y-max=40
  y-min=20
  type="line"></rpc-chart>



The 85th percentile speed is the speed at or below which 85% of drivers travel through a section of roadway. The 85th percentile speed was determined using the data provided for a full 24-hour period. Speeds were reported within a range of 5 mph (Ex. 30-34mph, 35-39mph). In order to create the graph, the midpoint of each range was used for plotting the 85th percentile speed in the northbound and southbound directions. 

In general, the 85th percentile speeds follow a similar trajectory as the average speed. The 85th percentile travel speed south of Green Street, was higher than the posted speed limit for northbound and southbound traffic by 2 mph in the southbound direction and 7 mph in the northbound direction. The 85th percentile travel speed north of Indiana Avenue was higher than the posted speed limit for both northbound and southbound traffic. Northbound traffic 85th percentile speed did not increase between Indiana Avenue and Green Street, however southbound traffic speed increased by approximately 5mph (or 1 speed range) between Green Street and Indiana Avenue. The 85th percentile travel speed north of Florida Avenue is higher than the posted speed limit of 30 mph for both northbound and southbound traffic. Northbound traffic 85th percentile speed increased between Florida Avenue and Indiana Avenue, while southbound traffic average speed decreased from Indiana Avenue to Florida Avenue.

<rpc-chart url="speed_data_85th.csv"
  chart-title="85th Percentile Speeds Through the Corridor"
  x-label="Segment"
  y-label="Speed"
  y-max=40
  y-min=20
  type="line"></rpc-chart>

### Traffic Operations
Traffic operations were evaluated at the six main intersections along the study corridor, and the four roadway segments between signalized intersections. The measures of effectiveness that are being reported include the level of service (LOS), delay reported in seconds per vehicle, and average and 95th percentile (maximum) queue distance. 

Intersection performance or traffic operations are quantified by six Levels of Service (LOS), which range from LOS A ("Free Flow”) to LOS F ("Fully Saturated"). LOS C is normally used for design purposes and represents a roadway with volumes ranging from 70% to 80% of its capacity. LOS D is generally considered acceptable for peak period conditions in urban and suburban areas and would be an appropriate benchmark of acceptable traffic for the study area road system. The LOS criteria descriptions listed below include conditions for both intersections and roadways.

  -	LOS A: Describes primarily free-flow operations at average travel speeds. Drivers have complete freedom to maneuver within the traffic stream and through an intersection with minimal delay.
  -	LOS B: At this level, the ability to maneuver within the traffic stream is slightly restricted and represents stable flow with slight delays. Control delays at signalized intersections are not significant.
  -	LOS C: Represents stable operations but the ability to maneuver and change lanes in mid-block locations is more complicated than at LOS B. Intersections have stable flow with acceptable delays.
  -	LOS D: This level borders on a range in which small increases in flow may cause substantial increases in delay and decreases in travel speed. This situation may occur due to adverse signal progression, inappropriate signal timing, high volumes, or a combination of these factors. Intersections are approaching unstable flow with tolerable delay (e.g. travelers must occasionally wait through more than one signal cycle before proceeding).
  -	LOS E: Traffic flow faces significant delays at this level. Adverse progression, high signal density and high volumes are main contributing factors to this situation. Intersections have unstable flow with an approaching intolerable delay.
  -	LOS F: This level is characterized by extremely low speeds and a forced or jammed flow. Intersection congestion is expected at critical signalized locations, with high delays, high volumes, and extensive queuing.

Levels of service for intersections are determined based on the average delay experienced by motorists. Signalized intersections reflect higher delay tolerances as compared to unsignalized and roundabout locations because motorists are accustomed to and accepting of longer delays at signals. For signalized and all-way stop intersections, the average control delay per vehicle is estimated for each movement and then aggregated for each approach and the intersection as a whole. For intersections with partial (side-street) stop control, the delay is calculated for the minor movements only (side-street approaches and major road left-turns) since thru traffic on the major road is not required to stop. The following tables show the LOS criteria for signalized and unsignalized intersections.

<rpc-table
url="LOS Criteria for Signalized and Unsignalized Intersections.csv"
table-title="LOS Criteria for Signalized and Unsignalized Intersections"
source="HCM 6th Edition">
</rpc-table>

### Operational Conditions of Selected Intersections
Existing traffic operational conditions were evaluated for the six largest intersections along the Lincoln Avenue corridor located at Florida Avenue, Pennsylvania Avenue, Ohio Street, Nevada Street, Illinois Street, and Green Street. Selected intersection criteria such as Level of Service (LOS), approach delay, and intersection delay were analyzed to determine the existing operational conditions during the morning and afternoon peak hours on a typical weekday.  The operational analysis was completed using the Synchro 11 software suite, which is based on the methodologies outlined in the Highway Capacity Manual (HCM 6th Edition) published by the Transportation Research Board (TRB).

### Operational Conditions of the Selected Intersections
The existing traffic operations were evaluated for the morning and afternoon peak travel periods at the six main intersections within the study area. Five of the six intersections studied are signalized intersections and include Lincoln Avenue at Green Street, Illinois Street, Nevada Street (3-leg), Pennsylvania Avenue, and Florida Avenue. One of the six intersections studied is stop controlled at Lincoln Avenue at Ohio Street. The results from the operational analysis are shown in the graphics below. The first graphic depicts the level of service for each approach as well as the overall intersection. The second graphic depicts the average and 95th percentile (maximum) queue length experienced at each intersection approach. The queue lengths shown represent the highest average or 95th percentile queue experienced  during either the AM or PM peak period. 

<p float="left">
  <img src="Graphic 2 - Fully Straightened Intersection LOS.png" width="100%"
  alt="Map showing the morning and afternoon peak hour level of service for each of the major intersections on the Lincoln Avenue Corridor" />
</p>

<p float="left">
  <img src="Graphic 3 - Fully Straightened Queue Lengths.png" width="100%"
  alt="Map showing the maximum peak hour queue lenghth for each of the major intersections on the Lincoln Avenue Corridor"  />
</p>

During the morning peak hour all of the signalized study intersections operate at LOS C or better, while individual approaches at the intersections also operate at LOS C or better. The unsignalized intersection at Lincoln Avenue and Ohio Street only causes delay for vehicles waiting to turn from the major and minor roads. At this intersection Lincoln Avenue continues to flow freely while Ohio Street is stop controlled. Therefore, the delay for this intersection is only quantified for the eastbound Ohio Street movements and the southbound left turn from Lincoln Avenue. The results of the operational analysis at this intersection during the morning peak period are LOS A for Lincoln Avenue and a LOS C for Ohio Street. 

During the afternoon peak hour most of the signalized intersections and approaches within the study area operate at LOS C or better. However, the intersection of Lincoln Avenue and Pennsylvania Avenue operates at LOS F, along with the southbound approach at Lincoln Avenue and Pennsylvania Avenue. The southbound and westbound approaches at the intersection of Lincoln Avenue and Florida Avenue operate with LOS D. The unsignalized intersection of Lincoln Avenue at Ohio Street operates similar to the morning peak hour.

The intersection that experiences the most severe queueing during the PM peak hour is Lincoln Avenue and Pennsylvania Avenue. The maximum southbound approach queue exceeds 1,000 feet, which can quickly create a ripple effect throughout the corridor when not promptly cleared. During a site field visit conducted during September of 2023, the delay at the southbound approach of Lincoln Avenue and Pennsylvania Avenue and the resulting queue was observed to contribute to corridor wide queues that backed up through the intersection of Lincoln Avenue and Nevada Street.

### Operational Conditions of Roadway Segments
Four roadway segments along Lincoln Avenue were analyzed using HCS 2023 software that measures effectiveness of the segment by quantifying the travel speed of through vehicles and the volume to capacity ratio of through vehicles at the boundary intersections. While the description of the levels of service remains the same as those for intersections, the criterion for these levels is different for Urban Street Segments than it is for intersections. The LOS criteria table for urban street segments is shown below and comes from the HCM 6th Edition chapter 18: Urban Street Segments. This table is specific for motorized vehicles.

<figure>
  <img src="Urban Street LOS for Motorized Vehicles.png" alt="table depicting criteria for LOS urban street segments"/>
  <figcaption> Source: HCM 6th Edition </figcaption>
</figure>

The results of the segment analysis during the morning and afternoon peak hours are shown below. The morning peak hour segments operate anywhere from LOS B to LOS E. The segment that experiences the most congestion is northbound traffic between Florida Avenue and Pennsylvania Avenue, which operates at LOS E. Southbound traffic between Florida Avenue and Pennsylvania Avenue operates at LOS C. Both northbound and southbound traffic between Pennsylvania Avenue and Illinois Street operate at LOS B, while both northbound and southbound traffic between Illinois Street and Green Street operate with LOS D.

The afternoon peak hour again operates anywhere from LOS B to LOS E. The two segments that experience the most congestion are southbound traffic between Florida Avenue and Pennsylvania Avenue, and between Green Street and Illinois Street, which both operate at LOS E. Northbound traffic between Florida Avenue and Pennsylvania Avenue, southbound traffic between Nevada Street and Pennsylvania Avenue, and northbound traffic between Illinois Street and Green Street operate at LOS D. Both northbound and southbound traffic between Nevada Street and Illinois Street operate at LOS C, while northbound traffic between Pennsylvania Avenue and  Nevada Street operates with LOS B.

These results indicate that the roadway is varied in its operations throughout the study corridor. However, the southern end of the corridor near Pennsylvania Avenue experiences the most congested conditions for both intersections and segments across both peak periods. 


<p float="left">
  <img src="Graphic 4 - Fully Straightened Segment LOS.png" width="100%" padding= "5px"
  alt="Map showing the morning and afternoon peak hour level of service for each of the segments on the Lincoln Avenue Corridor"  />
</p>

## Crash Analysis
To identify existing safety issues, staff collected and analyzed the most recent crash data available from 2013 to 2022 from IDOT, including total number of crashes, crash types, crash severity, roadway surface, and lighting conditions when crashes occurred. Additionally, the findings from the 2020 [Vision Zero for University of Illinois Campus](https://uofi.app.box.com/s/zyuqv8p11tleiuqzsvwge3utsuuutipp) report are also used to identify road safety locations.


### Crash Trends
According to the Vision Zero report's focus group survey, Lincoln Avenue is identified as the most problematic campus location based on public perception. There were 337 reported crashes along the study corridor between 2013 and 2022. More than half of the crashes (56 percent or 190 crashes) occurred at intersections. The figure below shows the total number of intersection crashes and segment crashes reported per year along Lincoln Avenue. The highest number of crashes occurred in 2017 (52 crashes); the lowest number of crashes occurred in 2022 (15 crashes). The decrease in crash numbers in 2018 was mainly attributed to the impact of the Multimodal Corridor Enhancement Project (MCORE), which reduced traffic flow in and out of the University District through Green Street and Lincoln Avenue. Subsequently, the reduction in the number of crashes after 2019 is likely due to COVID-19, which reduced the amount of driving, and related changes in driver behavior.

<rpc-chart
url="Traffic Crash Trends, 2013-2022.csv"
type="line"
switch="true"
y-label="Number of Crashes"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2013-2022 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Traffic Crash Trend, 2013-2022"></rpc-chart>


The table below focuses on the 135 crashes that occurred at signalized intersections and 55 crashes that occurred at unsignalized intersections along the corridor. The intersection of Florida Avenue & Lincoln Avenue had the highest number of crashes (54 crashes) between 2013 and 2022.  Additionally, the intersection of Iowa Street & Lincoln Avenue, Nevada Street (N) & Lincoln Avenue, Ohio Street & Lincoln Avenue, Oregon Street & Lincoln Avenue, and Pennsylvania Avenue & Lincoln Avenue are also included in the table. These five intersections have been perceived as problem locations in Vision Zero Report 2020. 

<rpc-table
url="Int Crash Locations 2013-2022.csv"
table-title="Intersection Crashes, 2013-2022">
</rpc-table>

(*) Indicates unsignalized intersections

For the purposes of this detailed corridor analysis, staff utilized two criteria to classify intersection crashes—the crash occured within a 75 ft radius of the intersection, and the crash was identified as intersection-related in IDOT's crash dataset. This is a more specific criteria than, for example, the standard that IDOT uses to identify top five percent crash locations. This is why the Ohio Street crash counts seen above are different than the counts mentioned in this intersection's listing as a top five percent location, as seen on the [Planning Process](https://ccrpc.gitlab.io/lincoln-ave/process/) page. Within the Lincoln Avenue corridor and its tight grouping of side streets, this stricter analysis criteria allowed for more specific designations of intersection and mid-block crashes, and prevented overlap from adjacent intersections.

The following table documents the location and year of the 147 mid-block crashes along the corridor between 2013 and 2022. The segment between Ohio Street - Indiana Avenue had the highest number of mid-block crashes (32 crashes), followed by the segment between W Iowa Street - Ohio Street (29 crashes), while the segment between W Illinois Street - W California Avenue had the lowest number (4 crashes).

<rpc-table
url="Mid Block Crashes, 2013-2022.csv"
table-title="Mid Block Crashes, 2013-2022">
</rpc-table>

The following map shows the number of intersection crashes and mid-block crashes between 2013 and 2022.

<iframe src="https://maps.ccrpc.org/intersection-and-mid-block-crashes-2013-2022/" width="100%" height="600" allowfullscreen="true"></iframe>



### Crash Types
The table below shows the predominant crash types at each intersection along Lincoln Avenue from 2013 to 2022. As indicated below, front to rear, turning crashes and angle are the most prevalent crash types at both signalized and unsignalized intersections, accounting for 47 percent, 22 percent, and 14 percent of total intersection crashes, respectively. 

<rpc-table
url="Int Crash Type 2013-2022.csv"
table-title="Intersection Crash Type, 2013-2022">
</rpc-table>

Front to rear crashes are the dominant crash type for mid-block crashes, accounting for 69 percent of the total mid-block crashes.

<rpc-table
url="Mid Block Crash Type, 2013-2022.csv"
table-title="Mid Block Crash Type, 2013-2022">
</rpc-table>

### Crash Severity
The severity of a crash is determined by the most severe injury of a person in that crash. The crashes are classified on the KABCO scale: 
  - ‘K’ represents a fatal crash; 
  - ‘A’ represents a crash that caused an incapacitating injury, also referred as a serious injury; 
  - ‘B’ represents a crash that caused a non-incapacitating injury; 
  - ‘C’ represents a crash that caused a reported/not evident injury;
  - ‘O‘ represents a crash with no indication of injury and that just resulted in property damage (PDO). 

The table below illustrates the severity levels of intersection crashes along the study corridor. In 2020, a fatal intersection crash occurred at Florida Avenue & Lincoln Avenue involving three vehicles and was caused by failing to yield right of way. The intersection of Florida Avenue & Lincoln Avenue also saw the most injury crashes, including the only fatal crash in the corridor, three B-injury crashes, and ten C-injury crashes during the period of analysis.

There were two A-injury crashes at the intersection of Iowa Street & Lincoln Avenue, with one of these two crashes involving a pedestrian. Additionally, there was a pedestrian crash resulting in an A-injury at the intersection of Ohio Street & Lincoln Avenue due to a failure to yield the right of way. This intersection (Ohio Street & Lincoln Avenue) also had the highest number of both injury and non-injury crashes among all unsignalized intersections. 

IDOT has established safety tiers to evaluate roadway safety performance and opportunity for improvement. The latest safety tiers were updated in 2022, categorizing roadway segments into High, Medium, or Low designations, using a prioritization based on safety performance and opportunity for improvement. In the latest safety tiers, the intersections of Ohio Street & Lincoln Avenue and Iowa Street & Lincoln Avenue were both identified as High. The previous safety tiers, 2017 IDOT Safety Tiers, includes a Critical/5 percent, High, Medium, Low or Minimal designation. In this previous categorization, the intersection of Ohio Street & Lincoln Avenue was identified as Critical, while the intersection of Iowa Street & Lincoln Avenue was identified as High. 

<rpc-table
url="Int Crash Severity 2013-2022.csv"
table-title="Intersection Crash Severity, 2013-2022">
</rpc-table>

The table below shows traffic crash severity levels for all mid-block crashes along the corridor between 2013 and 2022. One fatal crash occurred on the segment between Nevada Street and Iowa Street in 2013. The crash resulted in the death of a pedestrian and was attributed to the physical condition of the driver. Three A-injury crashes occurred on the following mid-block segments: Ohio Street - Indiana Avenue in 2016, Delaware Avenue - Florida Avenue in 2019, and Iowa Street - Ohio Street in 2016. The crash on the mid-block segment between Iowa Street and Ohio Street was a result of failing to reduce speed to avoid the crash, while the other two crashes were caused by failing to yield right of way.  

<rpc-table
url="Mid Block Crash Severity, 2013-2022.csv"
table-title="Mid Block Crash Severity, 2013-2022">
</rpc-table>

The map below shows crashes along the Lincoln Avenue Corridor by severity.

<iframe src="https://maps.ccrpc.org/lacs-crashes-2013-2022/" width="100%" height="600" allowfullscreen="true"></iframe>


### Road Surface and Lighting Conditions
The table below shows that many of the intersection crashes (67 percent) and mid-block crashes (65 percent) along this corridor occurred during dry road surface conditions. However, at the intersection of Florida Avenue & Lincoln Avenue, 24 out of the 54 crashes occurred when the road surface was wet, or when ice, snow, or slush was present. Similarly, at the mid-block segment between Ohio Street and Indiana Avenue, 16 out of the 32 crashes happened under such road surface conditions.

<rpc-table
url="Road Surface Conditions on Int, 2013-2022.csv"
table-title="Road Surface Conditions on Intersection Crashes, 2013-2022">
</rpc-table>

<rpc-table
url="Road Conditions on Mid Block Crashes, 2013-2022.csv"
table-title="Road Surface Conditions on Mid Block Crashes, 2013-2022">
</rpc-table>


The following tables show that most of the intersection crashes (77 percent) and mid-block crashes (74 percent) occurred during daylight hours.

<rpc-table
url="Lighting Conditions on Int, 2013-2022.csv"
table-title="Roadway Lighting Conditions on Intersection Crashes, 2013-2022">
</rpc-table>

<rpc-table
url="Lighting on Mid Block Crashes, 2013-2022.csv"
table-title="Roadway Lighting Conditions on Mid Block Crashes, 2013-2022">
</rpc-table>

## Transit Service

The Champaign Urbana Mass Transit District (MTD) is responsible for providing fixed-route transit service in the Champaign-Urbana urbanized area. MTD provides additional transportation services for the University of Illinois as well as Champaign and Urbana public middle and high school students. 

The 2 Red and 22 Illini routes travel along substantial sections of the corridor, but several other routes enter the study area. The 8 Bronze, 12 Teal, 13 Silver, and 21 Raven travel along Lincoln Avenue for one to two blocks on the southern end of the corridor to enter the Pennsylvania Avenue Residence Hall and Florida Avenue Residence Hall, the major transit hub for this area of the community.  On the north end of the corridor, the 5 Green and 24 Link both serve the intersection of Green Street and Lincoln Avenue. The 10 Gold does not enter the study area, but does navigate the area of influence.

Of the nine routes listed above, five (the 12 Teal, 13 Silver, 21 Raven, 22 Illini, and 24 Link) are considered to be “campus routes.” They reinforce accessibility around the campus area to academic buildings, the campus research park, and some of the student housing outside of the immediate campus area, mostly further north on Lincoln Avenue. The remaining four routes (2 Red, 5 Green, 8 Bronze, and 10 Gold) offer more meaningful access off campus to Downtown Champaign, Downtown Urbana, the Market Place mall, the commercial center on North Prospect Avenue, and others.
The area is also served by MTD’s SafeRides service, a demand-response service providing curb-to-curb trips at night, primarily for students. The University also manages its own paratransit service through the Disability Resources and Educational Services department (DRES).

#### Transit Routes and Stops
<p float="left">
  <img src="Transit Route and Stops.png" width="100%"
  alt="Map showing transit routes and stops in the corridor study area of influence" />
</p>

### Span of Service

The routes above reflect the peak service during the day for any given weekday that the University is in session. However, the depth and span of service changes based on the time of day, the day of the week, and whether the university is in session.

During the highest level of service, the study area will see transit service between 6:00 AM in the morning to almost 6:00 AM the following morning on campus routes like the 12 Teal, 13 Silver, and 22 Illini. However, the 220 Illini Evening service no longer enters the area of influence for this study. Routes with evening service like the 5 Green will run limited until 2:00 AM or 3:00 AM. Most routes, though, will end between 6:00 PM and 7:00 PM.

When the university is out of session, service will end earlier, with most late-night service ending before midnight.

During the university sessions, SafeRides runs from 5:00 PM (or 7:00 PM, depending on daylight savings time) to 6:00 AM the following morning, as long as the university is in session. During university breaks, the service runs until midnight Monday through Saturday. It does not run over the summer. 

DRES paratransit schedules a narrower span of service over the summer but remains flexible for student needs.


### Transit Facilities
MTD has tiered bus amenities, as outlined in the MTD Stop Placement Guidelines document published in January 2016. Any stop with over 50 daily boardings and alightings is considered a candidate for a bus shelter.  This includes a bus stop sign, a sidewalk platform, and lighting. A level boarding platform is recommended, with a waste receptable and designated stop announcement considered optional.

A stop with over 150 boardings is considered a candidate for all of the above, with an LED Departure Board, a STOPwatch Kiosk, and security cameras being recommended. This full deck of amenities is found commonly around the campus area and at major stops and transfer points in the surrounding community.

Along the study corridor, the only bus shelters along Lincoln Avenue are in front of the Lincoln Avenue Residence Hall. They are both full shelters with garbage cans and schedule information, but without the real-time kiosk. As a note, the west side stop had an average of 64 boardings in 2019 and 282 alightings, so the lack of amenities there is largely justified; if most of the people at the stop are alighting, they are not waiting at the stop or utilizing the amenities. The stop on the east side, however, has 331 daily boardings and 86 alightings, making it a prime candidate for the enhanced amenities.

Champaign-Urbana only has a few designated bus lanes or bus-only roads in the community, and none of them are in the area of influence for Lincoln Avenue. However, between Pennsylvania Avenue and Nevada Street, Lincoln Avenue has one southbound lane, a center/turn lane, and one northbound lane. The northbound lane is 11 feet, with an 8-foot shoulder. While this does not qualify as a “bus lane”, it does allow northbound buses to pull into the shoulder and make passenger stops along that stretch. Due to the width, buses can pull over without completely leaving traffic, though conflict points still exist when the bus resumes moving while other vehicles are planning to pass it in the center lane.

### Ridership Data
In 2019, there were 65 stops connected to the area of influence, with a total stop activity of 19,033 total boardings and alightings per day. MTD stops are comprised of a larger “parent stop” and smaller “child stops” or “stop points”. In this context, if one stop point is in the area of influence, but its “partner” is located just outside of the area, the partner is still included in this count. In 2022, there were 69 stops connected to the area, with a total stop activity of 12,237, for a decrease of 35.7% relative to the 2019 count. The decrease in ridership is largely presumed to be due to COVID factors. The specifics of that are not obvious, but could be related to students studying remotely, lingering attitudes about COVID being spread on public transit, or other pandemic-related causes.

This decrease in ridership is distributed more-or-less proportionally throughout the area of influence. There were slight increases in ridership along Green Street and at the Illinois Street Residence Hall, but there were otherwise no major geographic trends to the decrease in ridership.


<iframe src="https://maps.ccrpc.org/lacs-transit-activity/" width="100%" height="600" allowfullscreen="true"></iframe> 



## Bike and Pedestrian Facilities

In 2014, Urbana was named the first Gold-Level Bicycle Friendly Community in Illinois by the League of American Bicyclists and renewed its Gold-Level status in 2018. Urbana's rates of biking and walking to work are the highest in the region, and the corridor’s area of influence currently includes a variety of on-street and off-street pedestrian and bicycle facilities including:

-   **Bike Lane** – Road lane dedicated for bicycle travel, alongside vehicle travel.
-   **Bike Route** – Road with bikeway signage that provides for easy/safe bicycle travel with no designated lane for bicycles.
-   **Shared-Use Path** – Off-street trail, sometimes alongside a road, closed to vehicle traffic but open for non-motorized pedestrian and bicycle travel.
-   **Sharrow** – On-street symbol of a bicycle and two chevrons on top. Indicates that bikes and vehicles may use a full lane and is employed when a street is too narrow for both a designated bike lane and vehicle lane.
-   **Sidewalk** – Off-street path along the roadway, primarily for pedestrians.


On-street bike facilities in the study area include bike lanes on Green Street and Illinois Street east and west of Lincoln Avenue. These bike lanes connect the UIUC campus to neighborhoods west of Lincoln Avenue. 


Off-street facilities within the area of influence consist of sidewalks and shared-use paths. Along Lincoln Avenue, there are sidewalks on either side of the street. Sections of the sidewalk widen into a Shared-Use Path and connect to the UIUC campus pathways. These shared-use paths are present between Iowa Street and Michigan Avenue, and south of Florida Avenue, continuing on Lincoln Avenue past the area of influence to Windsor Road. These shared-use paths are typically eight to ten feet wide and connect the University athletics, Arboretum, campus buildings, and surrounding neighborhoods. Both pedestrian and bicycle travel is allowed on these paths, as there is room for two-way traffic of both bicyclists and pedestrians. 


There is an offset intersection at Oregon and Lincoln Avenue, which can cause challenges for cyclists and pedestrians at unprotected intersections. Additionally, the following intersections were identified in the [Urbana Bicycle Master Plan (2016)](https://ccrpc.org/documents/urbana-bicycle-master-plan-2016/) as difficult in terms of accessing the University District: Oregon Street and Lincoln Avenue; Iowa Street and Lincoln Avenue.


The Urbana Bike Master Plan (2016) also provided recommendations for segments of Lincoln Avenue and the area of influence. These recommendations are included on the [Plans, Reports, and Studies](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/plans-reports-and-studies/#urbana-bicycle-master-plan-ubmp-2016) page.


Additionally, the [Urbana Pedestrian Master Plan (2020)](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/) details the area around Lincoln Avenue (between Oregon and Florida) as having the highest population density in Urbana (pg. 16-17). The Urbana Pedestrian Master Plan has also provided recommendations for segments of Lincoln Avenue and the area of influence. These recommendations are included on the [Plans, Reports, and Studies](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/plans-reports-and-studies/#urbana-pedestrian-master-plan-2020) page.


## Bicycle and Pedestrian Network Analysis
The main travelling corridors within the Area of Influence are largely adequate. On a scale from A (being the best) to F (being the worst), the area is dominated almost entirely with segments in the B to C range, as shown in the map below. There are four minor arterials in the area of influence: Springfield Avenue, Green Street, and Florida Avenue running east-west, and Lincoln Avenue itself running north-south. All of the “D” grades are found on those minor arterials.

An important note on those minor arterials: due to the recent MCORE (Multimodel Corridor Enhancement) Project, Green Street experienced a reduction in traffic lanes, implementation of bike infrastructure, and re-paving. Therefore, it now has an “A” grade for its entire run through the area of influence.


<iframe src="https://maps.ccrpc.org/lincoln-avenue-blos/" width="100%" height="600" allowfullscreen="true"></iframe> 


<rpc-table
url="BLOS_Grade_Freq.csv"
table-title="Bicycle Level of Service Grade Frequency for Area of Influence">
</rpc-table>

The network provided by shared-use paths as defined above, though, warrants special comment. The street system of this part of the community is clearly disrupted by the presence of campus. From a strict urban design standpoint, the campus area has a more irregular block layout and more one-way streets, allowing for fewer clean throughways for motor vehicles. However, as shown in the map below, bicyclists have access to a more traditional grid system due to the layout of off-street facilities. Where westbound traffic along Illinois Street normally stops at Goodwin Avenue, and motor vehicles would have to detour, bike lanes are found along the block to the west. Similarly, the McKinley Health Center and Lincoln Avenue Residence Hall occupy a larger block, interrupting traffic, but shared-use paths are found along the block. These factors allow bicycles to have comparatively uninterrupted east-west access through campus compared to motor vehicle traffic.


<iframe src="https://maps.ccrpc.org/lacs-bicycle-infra/" width="100%" height="600" allowfullscreen="true"></iframe> 


The same is largely true of the pedestrian network, with sidewalks connecting gaps in the pedestrian network that the irregular block structure of the campus creates for motor vehicles. The map below shows the sidewalk layout of the area of influence. The lines shown in blue represent sidewalks compliant with the Access Board’s Public Right-of-Way Accessibility Guidelines (PROWAG), which have been adopted as the standard by the City of Urbana.  Paths shown in blue are over 48 inches wide, with a cross slope over under 2.1%, and with the pavement in good condition. While there are other requirements under ADA and PROWAG related to curb ramps, turning radii, etc., these are the criteria that can be displayed at the sidewalk segment level.

Paths shown in orange meet the width and slope requirements, but are considered “cracked” or exhibit another surface condition in a way that may or may not compromise compliance. Paths shown in gray are not PROWAG compliant, normally due to the steepness of the cross slope across the segment.
Quality aside, the area of influence has a well-developed sidewalk networks. All streets have sidewalks and they are generally wide enough for accessible use, which would not be the case for many neighborhoods in the surrounding community.


<iframe src="https://maps.ccrpc.org/lacs-sidewalk/" width="100%" height="600" allowfullscreen="true"></iframe> 
