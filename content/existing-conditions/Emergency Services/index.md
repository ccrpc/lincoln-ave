---
title: Emergency Services
draft: false
weight: 12
---

It is critical to document how emergency service providers currently utilize the roadways and service the area when assessing the existing conditions and considering future improvements.
 
## Fire Protection

The City of Urbana provides fire protection within their corporate limits based on a proximity dispatch method, where the closest units are calculated based on real-time using GPS. The fire stations closest to the study area are located at 1105 West Gregory Drive and 400 South Vine Street. Just north of the area of influence is another station located at 1407 North Lincoln Avenue.  

The Cities of Urbana and Champaign jointly provide fire protection services for University of Illinois properties, based on an Intergovernmental Agreement for Fire Protection Services between the University of Illinois and the Cities of Champaign and Urbana. In the case of a fire on University property, the campus fire station is used first, while other unit(s) are supplemented based on proximity and incident type. 



## Police Service

Police protection is provided to the area of influence by the Urbana Police Department and University of Illinois Police Department. The Urbana Police Department provides police services within the Urbana corporate limits. The University of Illinois Police jurisdiction boundary is visible in the map below. 


<iframe src="https://www.google.com/maps/d/embed?mid=1a3cRKeBlKxQhFksvMEz8tf82NWcNuBM&ehbc=2E312F" width="640" height="480" source: https://police.illinois.edu/what-uipds-campustown-plans-mean-for-student-safety/></iframe>

