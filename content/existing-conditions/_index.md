---
title: Existing Conditions
draft: false
menu: main
weight: 20
---

Existing conditions data establish a baseline from which planning visions can be grounded. This section includes demographic, land use, environmental, transportation, utility, and emergency services data.