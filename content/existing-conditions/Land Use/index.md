---
title: Land Use and Zoning
draft: false
weight: 15
---

The Lincoln Avenue Corridor functions as a minor arterial road, serving as a transition between the University District and the rest of the City of Urbana. The land uses in and around the corridor reflect the identity of the University as a flagship campus and the dense, walkable character of west Urbana. Over half of the land surrounding the corridor is institutional space maintained by the University. Over one-third of the land is residential, including single-family, multi-family, and university student housing. A smaller portion of the land is commercial. This section provides an overview of existing land use patterns and their implications for future transportation improvements.

<rpc-table
url="Land_use_updated_for_web.csv"
table-title="Land Use Categories in Lincoln Avenue Area of Influence, 2022"
source="Champaign County, Tax Assessor, 2022 Parcel-level Data">
</rpc-table>

<iframe src="https://maps.ccrpc.org/lincoln-avenue-land-use/" width="100%" height="600" allowfullscreen="true"></iframe>


To the west of Lincoln Avenue, the predominant land use is institutional space, including a variety of university properties used for commercial, educational, residential, and recreational purposes. The northern half of this corridor also includes a variety of commercial uses.

East of Lincoln Avenue, the land use shifts to predominately residential, including low, medium, and high-density residential. Most of the residences on the east side of Lincoln Avenue to Orchard Street were built in the 1920s and 30s. There are also newer multi-family residential structures along Lincoln Avenue, such as 901 Western Avenue built in 2016 and 809 S. Lincoln Avenue built in 2018. Like some of the structures in the surrounding neighborhood, these multi-family structures are rentals mostly occupied by the University of Illinois students. Other structures east of Lincoln Avenue include a number of University of Illinois fraternity and sorority houses and religious centers. 

### Zoning
The [Urbana Zoning Ordinance](https://www.urbanaillinois.us/zoning) (most recently amended in 2021) is one of the main tools the City uses to regulate development and the kinds of uses allowed on individual property. The purpose of the Zoning Ordinance is to implement the policies of the City of Urbana as expressed in the 2005 Comprehensive Plan, adopted by the City Council on April 11, 2005. [The Urbana Comprehensive Plan](http://urbanaillinois.us/node/933) emphasizes infill development and encourages new growth in a compact and contiguous manner. The City is currently in the process of [updating the 2005 Comprehensive Plan](https://imagineurbana.com/).

