---
title: Demographics
draft: false
weight: 10
---

Examining the population in the area surrounding the Lincoln Avenue Corridor—both their demographic attributes and their transportation habits—informs how this corridor can serve both the residents living near it and the region as a whole. 

## Area of Influence

For the purposes of this section, the Lincoln Avenue Corridor's Area of Influence is defined as seen in the map below—the Census Block Groups located within the rectangle bounded by Springfield Avenue on the North, Orchard Street on the east, St Mary's Road on the south, and Goodwin Avenue on the west. The block group on the southeast corner of this rectangle was excluded, because including it would only add one household inside the rectangle (the University of Illinois President's House) while adding the data of thousands of residents located outside of the rectangle. 

<iframe src="https://maps.ccrpc.org/lincoln-avenue-area-of-influence/" width="100%" height="600" allowfullscreen="true"></iframe>

Unless stated otherwise, the data presented in this page is taken from the 5-Year American Community Survey, covering the years from 2017 to 2021. All data is taken from the individual block groups seen above, then combined (either by addition or weighted average, depending on the variable) to determine the data for the Area of Influence as a whole.

## Population

The population of the block groups making up the Lincoln Avenue Area of Influence is 10,696 individuals. Of these individuals, 5,992 live in group quarters (which in the case of this area, means dormitories). The remaining 4,704 residents live in the 2,205 households in the area, resulting in an average household size of 2.1 people. Of the households in this area, 22.4% are families living together, 25.7% are non-related households, and 52.0% are individuals living alone.

Looking at change over time, Census Bureau data shows a population increase of more than 1,000 people from 2000 to the early 2010s. However, the most recent census data shows a decrease in population below the number even in the 2000s. However, much of this decrease is largely accounted for by census undercounting issues during the pandemic, during which many students returned to their parents homes and were not counted at their Champaign-Urbana residences.

<rpc-chart
url="population_over_time_bg.csv"
type="horizontalBar"
stacked="true"
x-label="Population"
x-min="0"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, Decennial Census and American Community Survey"
chart-title="Population Over Time - Area of Influence"></rpc-chart>

## Age Distribution

Due to the corridor’s proximity to the University of Illinois campus, college-aged residents make up a disproportionate share of the area’s population, at 78.1% of residents. Working-age adults make up the next largest group, at 16%, with youth and senior citizens following at 3.1% and 2.8%, respectively. As seen in the graph below, the high college age representation results in substantially lower youth, working-age, and senior populations than in the City of Urbana or Champaign County overall. Looking at population change over time, this college-age population has also increased in representation over recent decades, from 71.8% in 2000 to 78% in 2021. This shift toward greater student representation may account for some changes discussed later on this page, including increased rates of vehicle non-ownership and non-vehicle commuting in the area of influence.


<rpc-chart
url="age.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B01001"
chart-title="Age Distribution"></rpc-chart>

<rpc-chart
url="age_over_time.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B01001"
chart-title="Age Distribution Over Time, Area of Influence"></rpc-chart>

## Race and Ethnicity

When looking at race and ethnicity, the largest group in the Area of Influence is non-Hispanic white individuals, who make up 59.5% of residents. They are followed by non-Hispanic Asian residents (19.4%), Hispanic or Latino residents of any race (11.3%), and non-Hispanic black residents (5.7%). Compared to the City of Urbana and Champaign County, Asian and Latino residents are disproportionately represented in the Area of Influence, while Black residents are underrepresented in this area.


<rpc-chart
url="race.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B03002"
chart-title="Race and Ethnicity"></rpc-chart>

## Language and English Fluency

Nearly half of the households in the Area of Influence speak a language other than English at home, with 22.8% speaking an Asian and Pacific Islander language, 10.5% speaking an Indo-European language, and 10% speaking Spanish (according to the language groupings provided by the Census Bureau). Within these households speaking other languages, the area has a significant portion of limited English speaking households (households in which no individual over 14 years old speaks English “very well”, according to American Community Survey results). In this area, 12.6% of households are limited-English households, double the rate for these households in Urbana and nearly three times the rate for Champaign County.

<rpc-chart
url="language.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table C16002"
chart-title="Households by Language Spoken"></rpc-chart>

<rpc-chart
url="limited_eng.csv"
type="horizontalBar"
x-label="Percent of Households"
x-min="0"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table C16002"
chart-title="Limited English Households"></rpc-chart>

Looking at the language needs of the limited English speakers in the area, it helps to zoom out to the census tract level, a slightly larger geography, but also one where more detailed language data is available. The graph below shows the language spoken for residents in the area who speak English "less than very well". This data still provides several general language groupings, but it shows that among non-fluent English speakers in the area, Chinese is the most common language spoken, followed by Spanish, and the broad "Other Indo-European Languages" category. At the very least, this suggests that providing translation opportunities for Chinese and Spanish-speaking residents could be helpful in outreach and engagement of area residents in the corridor study process. 

<rpc-chart
url="non_fluent_households.csv"
type="doughnut"
x-label="Individuals"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table C16001"
chart-title="Non-Fluent English Speakers, by Language Spoken"></rpc-chart>

## Poverty

The area of influence has a significantly higher poverty rate (at 49.3%) than either the city or the county (27.1% or 18.3%, respectively). However, the significant presence of student populations makes statements about poverty in this area difficult. Many student households may report low incomes but also receive financial support from family. They also may be experiencing a period of low income while in school, but have saved up for this period or are anticipating higher incomes soon after completing their education. This means that many student households fit the technical definition of “households in poverty”, while not necessarily reflecting the information that is often sought after in poverty statistics. 

One alternative method of examining poverty data is to look at the poverty rate for family households. This data—a 12%  family poverty rate, compared with 10.1% in Urbana and 8.2% in Champaign County—indicates that families living in the area experience poverty at similar rates to the region at large. Similarly, the usage of SNAP benefits (also known as food stamps) by households in the Area of Influence is significantly lower (3.9%) than in Urbana and the county (11.1% and 10.6%, respectively). While this is likely partly due to awareness or willingness to use these benefits, it also suggests that significant portions of the low-income households in the area of influence may have family support or other financial sources to support themselves.


<rpc-chart
url="poverty.csv"
type="horizontalBar"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Tables C17002, B17010, and C22010"
chart-title="Poverty and SNAP Usage"></rpc-chart>

## Disability

The Area of Influence has a lower rate of households with disabled individuals, at 13.1%, than either the city or county, whose rates are both near 18%. While this may indicate that accessibility improvements aren’t an outstanding need for area residents in particular, it does not detract from the need for accessibility considerations in citywide infrastructure, of which Lincoln Avenue is a significant piece.


<rpc-chart
url="disability.csv"
type="horizontalBar"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Tables B22010"
chart-title="Disabled Residents"></rpc-chart>

## Tenure

Of the households in the area of influence, 75.1% are renters and 24.9% owned their homes. This is a significantly higher portion of renters than in Urbana (66.2%) and Champaign County (46.6%).


<rpc-chart
url="tenure.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B25003"
chart-title="Household Tenure"></rpc-chart>

## Vehicle Availability

Of the households in the area of influence, 46.3% have access to one motor vehicle, 10.2% have access to two, and 11.2% have access to three or more. This leaves nearly a third (32.3%) of households in the corridor area with no access to motor vehicles. The portion of households without motor vehicle access is significanlty higher in the Area of Influence than in the city (19.8%) or county (11.5%).


<rpc-chart
url="vehicle_availability.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B25044"
chart-title="Household Vehicle Availability"></rpc-chart>

As seen by the graphs below, the percent of households without a vehicle has increased in the Area of Influence over recent decades, from 21.2% in 2000 to the current rate of 32.3%. In the City of Urbana, lack of vehicle access increased significantly from 2000 to 2013 (from 15.7% to 20.9%) before decreasing slightly to its current level of 19.7%

<rpc-chart
url="vehicle_availability_over_time_aoi.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, American Community Survey 5-Year Estimates, Table B25044"
chart-title="Household Vehicle Availability Over Time - Area of Influence"></rpc-chart>

<rpc-chart
url="vehicle_availability_over_time_urbana.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, American Community Survey 5-Year Estimates, Table B25044"
chart-title="Household Vehicle Availability Over Time - City of Urbana"></rpc-chart>

## Commute

When looking at the commute for workers living in the Area of Influence, nearly half of workers (46.3%) walked to work, followed by 23.4% driving alone, 9.6% biking, 8.7% working from home, 8.5% taking public transit, and 3.1% carpooling. Compared to the city and county, where more than half of commuters drove alone, the Area of Influence saw substantially larger proportions of people taking public or active transportation to work. The information in this and the previous section—especially the prevalence of non-car trasnportation modes—is important in creating a corridor that meets the needs of those living in the area, along with playing a role in the region’s transportation system. In addition, the location of the region’s largest employer directly to the west of Lincoln Avenue suggests that many of these commuters are navigating Lincoln Avenue in order to reach their workplace, emphasizing the need to provide a safe transportation experience along and across Lincoln Avenue for all modes.


<rpc-chart
url="commute.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Workers"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B08301"
chart-title="Worker Commute Mode"></rpc-chart>

As seen in the graphs below, the percentage of workers in the area of influence who drive to work has decreased significantly in recent decades, from 31.9% in the year 2000 to 23.4% in the most recent data (and increase from 20% in 2013). In the City of Urbana, the rate of driving to work has stayed largely stable, bouncing in a few point range around the 50% mark over the last two decades. Across both geographies, the percent of workers working from home has increased signficantly since 2000. In contrast, transit ridership increased significantly between 2000 and 2013, before receding close to 2000 levels in 2021, possibly due to the lingering effects of the COVID-19 pandemic.

<rpc-chart
url="commute_over_time_aoi.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Workers"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, American Community Survey 5-Year Estimates, Table B08301"
chart-title="Worker Commute Mode Over Time - Area of Influence"></rpc-chart>

<rpc-chart
url="commute_over_time_urbana.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Workers"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, American Community Survey 5-Year Estimates, Table B08301"
chart-title="Worker Commute Mode Over Time - City of Urbana"></rpc-chart>