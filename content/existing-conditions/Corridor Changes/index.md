---
title: Corridor Changes
draft: false
weight: 19
---

Over the past two decades, the Lincoln Avenue Corridor has experienced land use changes that directly impact transportation usage and future development within this segment of the corridor (Green Street to Florida Avenue) and beyond. This section details these transformations and their significance in the redevelopment of the Lincoln Avenue Corridor.


### Past Improvements
In 2005, CUUATS completed a traffic study along Lincoln Avenue. Improvements to the corridor between Nevada Street and Pennsylvania Avenue were made in 2006, which included modifications in roadway conditions and infrastructure. These changes were made to make the corridor more pedestrian friendly and reduce vehicle delay at intersections. At the time, the 30-mph speed limit remained in place. In 2018, the City of Urbana approved the speed limit reduction from 30 mph to 25 mph along Lincoln Avenue from Nevada Street to Pennsylvania Avenue.  

**Before**
  - Four Lane Roadway
  - 4-Way Stop sign at Pennslyvania Avenue intersection
  - 3-Way Stop at west leg of Nevada Street intersection

**After**
  - Two Lane Roadway (including two-way left turn lane)
  - Traffic signal at Pennslyvania Avenue intersection
  - Traffic signal at west leg of Nevada Street intersection
  - Marked crosswalks and fluorescent green pedestrian crosswalk warning signs at Iowa Street, Ohio Street, and Indiana Avenue intersections
  -	Bus stops consolidated on this section of the corridor

<p float="left">
  <img src="Before and After.PNG" width="100%" />
</p>

### Shift to Multi-Family Residential
The Lincoln Avenue corridor has experienced a shift from single-family residential to multi-family residential properties, especially within the last decade, as new multi-family residential units sit where single-family residential once stood. Two prominent changes are located at 809 S Lincoln Avenue (at the west leg intersection of Nevada and Lincoln) and 901 Western Avenue:
  - The land parcels at 809 S Lincoln Avenue had been used as single-family residential until 2017, when new multi-family residential units were constructed in their place. These three- and four-bedroom units are marketed to UIUC students, given the proximity to the university campus.
  - In 2016, luxury student apartments at 901 Western Avenue were constructed in place of the single-family housing that previously occupied the lot. These one-to-four bedroom units are specifically marketed to students at the unviersity. 

Additional changes have occurred along the corridor, such as 1404 S. Lincoln Avenue. Formerly a sorority house, the property at 1404 S. Lincoln Avenue sat vacant from 2010 to 2017. Redevelopment and construction were completed in 2018 to transform the building’s interior into multi-family residential units. Today, the structure houses 19 units, from studios to three-bedrooms . These units are advertised as being close to campus but are not specifically marketed to the student population.

This increase in residential density directly along the corridor implies that more people are living in the area and thus utilizing this segment of Lincoln Avenue for travel. Accordingly, we can assume that there is an increased need for pedestrian safety given the development in recent years. 

<p float="left">
  <img src="land_use_corridor.png" width="100%" />
</p>


### University Growth
Most properties on the west side of Lincoln Avenue corridor are owned and operated by the University of Illinois Urbana-Champaign. Since the early 2000s, there have been several additions along the corridor that display the university’s growth, such as the construction of the Alumni Center and the Hallene Gateway Plaza along the westside of Lincoln Avenue. The presence and continued growth of the university has had an effect on the Lincoln Avenue corridor and the area of influence (AOI). 

For instance, the UIUC student enrollment has continued to increase over the past decade. [In Fall 2022, the UIUC enrollment totaled 56,916 students, of which only 15% are enrolled in online programs.](https://www.uillinois.edu/cms/One.aspx?portalId=1324&pageId=1565838) The total enrollment is an estimated 27% increase from 2013 enrollment.

<rpc-chart
url="UIUC_Enrollment.csv"
type="line"
switch="false"
y-label="Total Enrollment"
legend-position="top"
legend="false"
grid-lines="true"
tooltip-intersect="true"
source=" University of Illinois System Data: Enrollment Dashboard"
source-url="https://www.uillinois.edu/cms/One.aspx?portalId=1324&pageId=1565838"
chart-title="UIUC Total Enrollment, 2013-2022"></rpc-chart>

This growing university-centric presence within the AOI is made evident by the demographic analysis of the area. [Approximately 78% of the population within the AOI is within college age (18-24), whereas only 38% of Urbana falls within this age category.](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/demographics/#commute:~:text=Due%20to%20the%20corridor%E2%80%99s%20proximity%20to%20the%20University%20of%20Illinois%20campus%2C%20college%2Daged%20residents%20make%20up%20a%20disproportionate%20share%20of%20the%20area%E2%80%99s%20population%2C%20at%2078.1%25%20of%20residents.) The area surrounding the Lincoln Avenue corridor has a disproportionately high population of college age residents as compared to the rest of Urbana due to its proximity to campus and available housing options. 

As shown in the existing conditions analysis, a significant amount of the land use directly along the east side of the Lincoln Avenue corridor is multi-family residential. Many of these structures are occupied by students, young professionals, or university affiliates who use Lincoln Avenue to access the UIUC campus. [In fact, an estimated 46% of the population within the AOI walks as a form of commute (whereas only 15.8% in Urbana utilize walking).](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/demographics/#commute:~:text=When%20looking%20at%20the%20commute%20for%20workers%20living%20in%20the%20Area%20of%20Influence%2C%20nearly%20half%20of%20workers%20(46.3%25)%20walked%20to%20work) This number has also increased in the past decade, from 34.7% in 2013.

Residents in the area of influence, including those within the West Urbana neighborhood and on-campus housing, are walking and encountering Lincoln Avenue as a pedestrian at much higher rates as compared to the rest of the community. As enrollment numbers continue to rise, we will likely see a continued increase in multi-family and high-density housing development and the number of pedestrians utilizing this corridor.
