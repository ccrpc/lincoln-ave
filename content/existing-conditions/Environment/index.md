---
title: Environment
draft: false
weight: 13
---

This section covers some of the environmental features within the corridor that
could be relevant to future transportation improvements. The following table includes the environmental features and regulated substances that were reviewed for their presence in the corridor study’s area of influence. Additional information can be found in the paragraphs below for each of the features present in the area of influence. 

<rpc-table
url="environment.csv"
table-title="Environmental Features in the Lincoln Avenue Corridor Study Area of Influence, 2022">
</rpc-table>

## Topography

Champaign County is known for its flat, agricultural landscape. Consistent with that, the Area of Influence (AOI) remains fairly flat with the elevation ranging from 715 feet (218 meters) to 750 feet (229 meters) above mean sea level. The highest points of elevation are situated south of Florida Ave, the southern edges of the AOI. The lowest points of elevation are located in the north of the AOI, following the Boneyard Creek. With a difference of only 36 feet (11 meters), travelers in the area most likely do not notice a change in the elevation.

## Soil

Within the area of influence, there are multiple soil types, each with unique properties and development potential. The table below describes each soil type.

<rpc-table
url="soil.csv"
table-title="Soil Types in Lincoln Avenue Corridor Study Area of Influence, 2022"
source=" USDA Web Soil Survey. Accessed January 2023."
source-url="https://www2.illinois.gov/epa/Pages/default.aspx">
</rpc-table>

Flanagan silt loam with 0 to 2 percent slopes makes up about 40 percent of the study area. This is a non-hydric yet poorly drained soil with somewhat limited development potential. About 33 percent of the study area has hydric soils (Dummer- 152A). Although hydric soils present the biggest challenges to residential or commercial development due to limited water infiltration and oversaturated pores, these types of soils do not significantly limit infrastructure development because of their workability (quality moisture retention), ease of compaction and controlled surface runoff through sloping and drainage. Native vegetation also can help stabilize hydric soils by providing natural drainage.


<iframe src=" https://maps.ccrpc.org/lincoln-avenue-soils/" width="100%" height="600" allowfullscreen="true"></iframe>


## Natural Areas

Natural areas and open space are defined as areas with natural character, as evidenced by the presence of vegetation. These include, but are not limited to, parks and wooded areas.

<iframe src=" https://maps.ccrpc.org/lincoln-avenue-natural-areas/" width="100%" height="600" allowfullscreen="true"></iframe>

- **University of Illinois Arboretum** is a “living laboratory” which faculty and students at the university use to conduct research on plant collections on the grounds. Though it covers a total of 57 acres of land, our area of influence only includes a portion of this total. It is accessible by pedestrians, cyclists, and automobiles from Lincoln Avenue.  The Idea Garden is located within the area of influence, which includes a variety of vegetables, mixed shrubs, ornamental grasses, rose and berry gardens, rock gardens, walking paths and more.

- **Illini Grove** is part of the University of Illinois Campus Recreation and serves as a place which students, faculty, staff, and other members of the community can hold events, play basketball, volleyball, or tennis, and enjoy nature. The grove includes a variety of tree species (including pine, cypress, sycamore, maple, hickory, juniper, willow, elm, and oak) and number of picnic tables and grills. There is also a covered pavilion for gathering. The Illini Grove is accessible by sidewalk from Lincoln Avenue and Pennsylvania Avenue, and by shared-use paths from campus. 

- **Hallene Gateway Plaza** is located on the University of Illinois campus and serves as the university’s East entrance. It is the gateway to the university from the Lincoln Avenue Corridor, creating a pathway for pedestrians traveling from neighborhood across Lincoln Avenue. The plaza is surrounded by trees and various plant species. 


## Historic Places

Being adjacent to the University of Illinois, there are a number of historic places surrounding the Lincoln Avenue Corridor. However, there are three historic places currently located within the study area, directly along the corridor: Alpha Xi Delta Sorority Chapter House, Kappa Kappa Gamma Sorority House, and the Alpha Gamma Delta Fraternity House. 

 <iframe src="https://maps.ccrpc.org/lincoln-avenue-historic/" width="100%" height="600" allowfullscreen="true"></iframe>

- **Alpha Xi Delta Sorority Chapter House** is located at 715 W. Michigan Avenue, adjacent to Lincoln Avenue. The house was constructed in 1915 for Matthew Busey, longtime former president of Busey State Bank and prominent historical figure in the City of Urbana. In 1928, the Alpha Xi Delta Sorority Chapter at the University of Illinois Urbana-Champaign obtained the house for its use. The house displays Tudor Revival architectural style and was designed by a local architect by the name of Joseph William Royer, whose other notable works include the Urbana Lincoln Hotel. It has been on the National Register of Historic Places since 1989 under the Criterion A: Education and Criterion C: Architecture. 

- **Kappa Kappa Gamma Sorority House** is located at 1102 S. Lincoln Avenue and has housed the Beta Lambda chapter of the Kappa Kappa Gamma Sorority at the University of Illinois Urbana-Champaign since the house was designed and constructed in the English Revival architectural style by the Chicago architecture firm Howard Shaw Associates in 1928. The house has been on the National Register of Historic Places since 2004 under the under the Criterion A: Education and Criterion C: Architecture.

- **Alpha Gamma Delta Fraternity House** is located at 1106 S. Lincoln Avenue and houses the Alpha Gamma Delta Fraternity chapter at the University of Illinois Urbana-Champaign. The house was constructed for the fraternity in 1928 by local architect George Ramsey in the French Eclectic architectural style. It has been on the National Register of Historic Places since 2009 under the Criterion A: Education and Criterion C: Architecture.

The information above was obtained from the [Historic & Architectural Resources Geographic Information System (HARGIS)](https://maps.dnr.illinois.gov/portal/apps/webappviewer/index.html?id=8f6e15ca8973412bbd534e6990da752d) by the Illinois State Historic Preservation Office. For more information on these and other historic places, visit the website linked above.


## Wetlands

Wetlands are defined by IDOT as those areas that are “inundated or saturated by surface or groundwater at a frequency and duration to support, and that under normal circumstances do support, a prevalence of vegetation typically adapted for life in saturated soil conditions.” [^1] The National Wetlands Inventory of the US Fish and Wildlife Service notes a 3.6-acre freshwater emergent wetland habitat northwest of the St. Mary’s Road and Lincoln Avenue intersection. This location’s current use as a university athletic field indicates that there would likely not be any issues in proceeding with new infrastructure development plans in the study area, though an IDOT Environmental Survey Request would be needed to confirm this.


## Waterways

There is one waterway present within the area of influence known as the Boneyard Creek, which is a perennial stream. A stream is broadly defined as “a body of running water moving under gravity flow in a defined channel” [^2]. More specifically, a perennial stream contains water flowing year-round, with water sources such as smaller waterways and groundwater, as well as rainwater and precipitation [^3]. The Boneyard Creek is a 5,311-acre watershed, passing through both Champaign and Urbana, as well as the University of Illinois Urbana-Champaign campus, which feeds into the Saline Branch drainage ditch located northeast of the area of influence. Identified as an E-graded Biologically Diverse Stream segment (streams are graded A-E; A being the highest diversity and E being the lowest) by the Illinois Department of Natural Resources (IDNR), any development surrounding the stream should prioritize protection of the waterway and its inhabitants [^4]. The Boneyard Creek has been a topic of renewal for decades, the most recent plan adopted being the City of Urbana 2008 Boneyard Creek Master Plan. 

 <iframe src="https://maps.ccrpc.org/lincoln-avenue-waterways/" width="100%" height="600" allowfullscreen="true"></iframe>


## Watersheds

Watersheds are the area of land that drains to one stream, lake, or river. These areas affect the water quality of the body of water they drain into, as well as providing a host of ecosystem services, including nutrient cycling, carbon storage, erosion/sedimentation control, water storage and filtration, flood control, timber and food resources, and recreational opportunities. Watersheds follow their area’s topography from the highest ridgeline of the watershed to the lowest point of land. The area of influence includes portions of the Vermillion and Embarras watersheds. The Middle Fork and Salt Fork of the Vermilion River are the major waterways that drain the Vermilion watershed, while the Embarras River is the primary drainage waterway for the Embarras watershed. 


 <iframe src="https://maps.ccrpc.org/lincoln-avenue-watersheds/" width="100%" height="600" allowfullscreen="true"></iframe>



## Drainage Districts

Drainage districts are public corporations with specific governmental functions, formed to help ensure that there is adequate drainage and flood protection for the lands lying within the district. The area of influence includes two drainage districts, the Urbana-Champaign Sanitary District and Upper Embarras River Basin Drainage District, which should be consulted in the coordination of projects in the area.


## Regulated Substances

Federal and state regulations require that all currently known and potential special waste sites be identified as part of the environmental review process, so special preparations can be made to handle contaminants appropriately. [Special waste refers to any potentially infectious medical waste (PIMW), hazardous waste, pollution control waste, or industrial process waste](https://www2.illinois.gov/epa/topics/waste-management/waste-disposal/special-waste/Pages/default.aspx). The presence of hazardous or regulated substances affects both human and ecological health, and work in or around any identified special waste sites can cause a release of contaminants into the air, soil, and/or water.

 <iframe src="https://maps.ccrpc.org/lincoln-avenue-regulated-substances/" width="100%" height="600" allowfullscreen="true"></iframe>


- There are six Leaking Underground Storage Tanks (LUSTs) located within the area of influence. [LUSTs involve the release of a fuel product from an underground storage tank that can contaminate surrounding water sources (both surface and ground), soil, or affect indoor air quality](https://www.epa.gov/ust/leaking-underground-storage-tanks-corrective-action-resources#intro). 

- There are three Resource Conservation and Recovery Act (RCRA) locations within the area of influence, which represent the location of hazardous materials included in the RCRA. [The RCRA is a law that regulates the management of hazardous and non-hazardous materials.](https://www.epa.gov/rcra)

- There are eleven underground storage tanks (USTs) within the area of influence. ["An underground storage tank system (UST) is a tank and any underground piping connected to the tank that has at least 10 percent of its combined volume underground."](https://www.epa.gov/ust/learn-about-underground-storage-tanks-usts#what)


If a project requires a right-of-way acquisition and there is a site within the minimum search distance of that property, then a Preliminary Environmental Site Assessment (PESA) is performed according to Illinois State Geological Survey Standards. 
 

[^1]: [IDOT Bureau of Design and Environment (BDE) Manuel](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf), Chapter 26: Special Environmental Analysis, Accessed 2/17/2021.

[^2]: USGS. (2023). Boneyard Creek at Urbana, Illinois. Retrieved March, 3, 2023 from https://waterdata.usgs.gov/monitoring-location/03337000/#parameterCode=00065&period=P7D

[^3]: U.S. Environmental Protection Agency. (2023, February 17). Streams under CWA Section 404. Retrieved March 3, 2023 from https://www.epa.gov/cwa-404/streams-under-cwa-section-404

[^4]: Illinois Department of Natural Resources.(2007). Biological Stream Ratings. Retrieved March 3, 2023 from https://www2.illinois.gov/dnr/conservation/BiologicalStreamratings/Pages/default.aspx

