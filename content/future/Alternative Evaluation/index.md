---
title: Alternative Evaluation
draft: false
weight: 25
---

Traffic operations were evaluated for the morning and afternoon peak hours on a typical weekday at the five main intersections along the Lincoln Avenue corridor – located at Green Street, Illinois Street, W Nevada Street (3-leg), W Pennsylvania Avenue, and W Florida Avenue, and the four roadway segments between signalized intersections.

The measures of effectiveness that are being reported include the level of service (LOS), delay reported in seconds per vehicle, average and 95th percentile (maximum) queue distance, and volume to capacity ratio (v/c). Volume to capacity ratio is an indication if an approach is oversaturated. Oversaturation will result in a v/c of 1 or more, while an acceptable v/c is typically 0.85 or below.  

The signalized intersections were analyzed using Synchro 11 software suite, segments were analyzed using HCS, and roundabouts were analyzed using SIDRA. Each of these software systems is based on the methodologies outlined in the Highway Capacity Manual (HCM) published by the transportation research board (TRB). 

Intersection performance or traffic operations are quantified by six Levels of Service (LOS), which range from LOS A (“Free Flow”) to LOS F (“Fully Saturated”). LOS C is typically used for design purposes and represents a roadway with volumes ranging from 70% to 80% of its capacity. LOS D is generally considered acceptable for peak period conditions in urban and suburban areas and would be an appropriate benchmark of acceptable traffic for each overall intersection and mainline Lincoln Avenue approaches throughout the study area. Due to the desire to balance multimodal accessibility and comfort along the corridor with the needs of vehicles, LOS E was deemed acceptable for minor and side-street movements in future scenarios.

The LOS criteria descriptions listed below include conditions for both intersections and roadways.
- LOS A: Describes primarily free-flow operations at average travel speeds. Drivers have complete freedom to maneuver within the traffic stream and through an intersection with minimal delay.
- LOS B: At this level, the ability to maneuver within the traffic stream is slightly restricted and represents stable flow with slight delays. Control delays at signalized intersections are not significant.
- LOS C: Represents stable operations but the ability to maneuver and change lanes in mid-block locations is more complicated than at LOS B. Intersections have stable flow with acceptable delays.
- LOS D: This level borders on a range in which small increases in flow may cause substantial increases in delay and decreases in travel speed. This situation may occur due to adverse signal progression, inappropriate signal timing, high volumes, or a combination of these factors. Intersections are approaching unstable flow with tolerable delay (e.g. travelers must occasionally wait through more than one signal cycle before proceeding).
- LOS E: Traffic flow faces significant delays at this level. Adverse progression, high signal density and high volumes are main contributing factors to this situation. Intersections have unstable flow with an approaching intolerable delay.
- LOS F: This level is characterized by extremely low speeds and a forced or jammed flow. Intersection congestion is expected at critical signalized locations, with high delays, high volumes, and extensive queuing.

Levels of service for intersections are determined based on the average delay experienced by motorists. Signalized intersections reflect higher delay tolerances as compared to unsignalized and roundabout locations because motorists are accustomed to and accepting of longer delays at signals. For signalized and all-way stop intersections, the average control delay per vehicle is estimated for each movement and then aggregated for each approach and the intersection as a whole. For intersections with partial (side-street) stop control, the delay is calculated for the minor movements only (side-street approaches and major road left-turns) since thru traffic on the major road is not required to stop. The following tables show the LOS criteria for signalized and unsignalized intersections.


<rpc-table url="table 1.csv"
  table-title="LOS Criteria for Signalized and Unsignalized Intersections"
  text-alignment="l,r"></rpc-table>


{{<image src="intersection los.png"
  alt="Diagram comparing intersection level of service between existing conditions and various 2048 scenarios."
  position="full">}}


## 2048 Baseline Scenario

To understand the future traffic needs of the corridor, a Baseline condition was established by calculating the roadway Level of Service (LOS), delay, queue length, and volume to capacity ratio based on the forecasted future traffic volumes for 2048. 

2048 was chosen as the future year for analysis based on the assumption that the earliest the corridor would receive improvements would be 2028; then, a 20-year design life was projected. This time period is consistent with the current 2045 Long Range Transportation Plan (LRTP). 

This baseline condition analyzes the existing conditions of the intersections using future traffic volumes. This baseline scenario is used as a control to compare the operations of the proposed alternatives. 

The results of the baseline scenario analysis for 2048 are similar to that of existing conditions. Most overall intersections and individual approaches operate with LOS C or LOS D and volume to capacity ratios of 0.85 or less, which falls within an acceptable range. The projected queue lengths of all approaches during both peak periods at Green Street, Illinois Street, and Nevada Street are approximately 400 feet or less, with the exception of the southbound approach queue at Nevada Street during the afternoon peak period, which reaches up to 623 feet. These queues are expected to block some unsignalized side street access but do not extend to their respective upstream signalized intersections.

The intersections of Lincoln Avenue at Pennsylvania Avenue and Florida Avenue are expected to experience higher delay, oversaturated conditions, and extensive queue lengths in the year 2048 if no improvements are made to the corridor. In the morning peak hour, most approaches will operate at a LOS C while some will operate at a LOS D with 40 seconds of delay or less. The queue lengths can become extensive, reaching 933 feet with volume related to capacity nearing saturation with a v/c of 0.98 for northbound traffic at Pennsylvania Avenue. During the afternoon peak hour, operating conditions are expected to deteriorate further from existing conditions. Southbound traffic at Pennsylvania Avenue experiences LOS F with delays at 217 seconds and queue lengths of up to 1220 feet in length. In addition, both southbound approaches at Pennsylvania Avenue and Florida Avenue are projected to have volume to capacity ratios over 1, indicating these approaches are oversaturated.

## Roadway Scenarios Development and Evaluation

Two roadway alternatives were developed using different intersection control techniques to evaluate the operational conditions with future traffic volumes. These alternatives evaluated the five major intersections along Lincoln Avenue within the project limits - Green Street, Illinois Street, W Nevada Street (3-leg), W Pennsylvania Avenue, and W Florida Avenue. Both alternatives include a lane reduction north of Nevada Street to provide a single northbound and southbound lane with a center turn lane, bicycle facilities, pedestrian crossings, and a reduction in side-street access. All volumes associated with intersections with a proposed reduction in access were re-routed through the network to account for changes in travel patterns throughout the corridor.

- 2048 Alternative 1: Optimized traffic signal timing for the corridor 
- 2048 Alternative 2: Keep traffic signals at Green Street and Florida Avenue while changing Lincoln Avenue at Illinois Street, Nevada Street, and Pennsylvania Avenue to single lane roundabouts.

The corridor study steering committee understands the importance of this corridor as a multimodal connection within the City of Urbana, and between the University of Illinois campus and the surrounding region. As such, the target desirable vehicle level of service is D for each overall intersection and mainline Lincoln Avenue approaches throughout the study area, and E was deemed acceptable for minor and side-street movements in future scenarios.

**Alternative 1** was developed to determine the effectiveness of carefully timing the existing traffic signals throughout the corridor to develop an optimal cycle length that can allow for the reduced lanes to operate efficiently without deteriorating corridor vehicular traffic operations to unacceptable levels. For this analysis, the Lincoln Avenue corridor was optimized using a 105 second cycle length. Pedestrian scrambles were also considered as part of the signal timing scheme at each of the signalized intersections in an effort to provide more protection for pedestrians crossing the corridor. However, the required increase in cycle length to accommodate a pedestrian scramble phase at most of the corridor signals was untenable, and severely degraded vehicular levels of service past acceptable conditions. Therefore, pedestrian scrambles were not analyzed as part of the timings in the proposed Alternative 1 results. Pedestrian accommodations including traditional crossing phases were included at each signalized intersection, which still provides a protected corridor crossing.

Although there are some locations that may have slightly higher than ideal delay and queuing, the corridor can function in an acceptable manor while still providing the safety benefits that have been proposed. By keeping the intersections signalized, there will be less impacts to adjacent properties, a reduced cost, and public comfortability in a high pedestrian and bicycle area. 
The traffic signals are expected to include accessible audible pedestrian signals and push-button devices, vehicle and bicycle detection, and provide an opportunity to implement traffic signal priority and preemption to give priority to emergency vehicles and public transit buses when necessary.

During the morning peak hour all intersections are expected to operate at a LOS C or better while all individual approaches operate at LOS D or better. The mainline of Lincoln Avenue is prioritized and can expect an LOS C or better while the side roads may experience the longer delay while still maintaining short queues of no more than 230 feet (approximately 5 vehicles). 

During the afternoon peak hour, all intersections operates at LOS D or better. The approaches for each intersection experience LOS D or better on the mainline of Lincoln Avenue, except for the northbound approach at Illinois Street, and the eastbound approach at Pennsylvania Avenue. The northbound approach at Illinois Street experiences LOS E with 74.3 seconds of delay, volume to capacity ratio of 0.87, and a maximum queue expected to reach 389 feet. These values are close to the recommended goals for corridor operating conditions, and do not depict congestion that will propagate back through any upstream signalized intersections, so therefore are deemed acceptable in this case. The eastbound approach at Pennsylvania Avenue experiences LOS E with 61.5 seconds of delay, volume to capacity ratio of 0.90, and a maximum queue expected to reach 318 feet. While the volume to capacity ratio is approaching saturation of 1.0, the delay and modest queue are not excessive for a peak hour condition along a side street approach, and are therefore deemed acceptable in this case.


**Alternative 2** was developed to determine if the use of single lane and single approach roundabouts could be an effective proposed alternative along the Lincoln Avenue corridor. Roundabouts were not considered at Green Street or Florida Avenue because prior study efforts (Florida Avenue corridor study and MCORE project) considered roundabouts at these locations and, for various reasons, determined traffic signals were recommended. Therefore, in the Alternative 2, roundabouts were only considered at the intersections of Lincoln Avenue with Illinois Street, Nevada Street, and Pennsylvania Avenue while keeping Green Street and Florida Avenue as traffic signal controlled. 

While the roundabout alternative is effective in the morning peak hour, the afternoon peak hour is expected to experience higher delays than what is considered acceptable. Including roundabouts may have implications with right-of-way, higher costs, and less familiarity for all roadway users. Additionally, roundabouts would not provide any protected crossings for pedestrians at the major intersections throughout the corridor.

During the morning peak hour, all the intersections in the study area operate at LOS C or better. The approaches at each intersection are also expected to perform at a LOS D or better.  The expected queue lengths are considered acceptable at 640 feet or less and the v/c ratios are 0.89 or less. 

During the afternoon peak hour, the intersections degrade in level of service from LOS B to an LOS E. Of the intersections analyzed, only Florida Avenue and Nevada Street have approaches expected to operate at LOS D or better. The intersections of Lincoln Avenue at Illinois Street and Pennsylvania Avenue are expected to operate at LOS E with two approaches experiencing LOS F and v/c ratios near or over 1. Specifically, the northbound approach at Lincoln and Green would be expected to extend south past the intersection of Illinois Street to Nevada Street, which would effectively jam the Illinois Street roundabout and prevent vehicular circulation of any kind. 

## Operational Conditions of Roadway Segments

HCS was used to analyze the future operations of four segments of the corridor including the northbound and southbound segments between Green Street and Illinois Street, Illinois Street and Nevada Street, Nevada Street and Pennsylvania Avenue, and between Pennsylvania Avenue and Florida Avenue. The four segments were run for the existing roadway conditions, 2048 baseline, and the two alternative scenarios described above. For Alternative 2, previously deployed modeling software was unable to accurately analyze a segment that begins with a roundabout but ends with a signal, or vice versa. Because the lane configuration between intersections does not change between the two alternatives, it is reasonable to conclude that the segment LOS for these segments is similar to that of the segment results shown for the signalized corridor alternative.
 
The results of the morning peak hour segment analysis shows that, similar to the existing conditions analysis, the segments operate between LOS A and LOS E. The segment with the most optimal traffic flow occurs in the southbound direction between Nevada Street and Pennsylvania Avenue, and operates at LOS A. The most congested segments during the morning peak hour occur in the northbound and southbound directions between Illinois Street and Green Street as well as the northbound direction between Florida Avenue and Pennsylvania Avenue, at a LOS E. While the northbound direction between Florida Avenue and Pennsylvania Avenue remains at a LOS E from the existing conditions analysis, there is a slight decline in operations between Green Street and Illinois Street. As expected, the projected traffic growth and the reduction in traffic lanes have an impact on the segment operations. The other segments in the northbound and southbound directions operate acceptably at a LOS B or C during the morning peak hour. 

{{<image src="segment los.png"
  alt="Diagram comparing segment level of service between existing conditions and various 2048 scenarios."
  position="full">}}

The afternoon peak hour segments between Green Street and Illinois Street as well as Pennsylvania Avenue to Florida Avenue continue to operate at LOS E, as they did in the existing conditions analysis.  The remaining segments are expected to operate between LOS B and LOS D during the afternoon peak hour with the northbound direction experiencing less congestions than the southbound. 

These results indicate that the corridor is expected to vary in its operations during the peak hours, with some segments experiencing more delay and congestion than others. However, acceptable traffic flow along the corridor is expected to be maintained and it is acknowledged that some concessions with respect to vehicle satisfaction may be necessary to increase multimodal access and comfort along the corridor. 