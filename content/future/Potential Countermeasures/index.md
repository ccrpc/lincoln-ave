---
title: Potential Interventions
draft: false
weight: 35
---

Following analysis of [existing conditions](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/) and [public feedback](https://ccrpc.gitlab.io/lincoln-ave/public/all_survey_analysis/), as well as forecasting of [future conditions](https://ccrpc.gitlab.io/lincoln-ave/future/scenario-evaluation/) along the corridor, the project team proposes the following interventions to the corridor. These interventions consist of 1) Identification of Potential Countermeasures, or interventions that would be consistent across future proposed scenarios, and 2) three distinct scenarios for how to improve bike and pedestrian infrastructure along the corridor. These scenarios include bike lanes throughout the entire corridor, and different integrations of bike lanes with off-street shared-use paths. 


## Identification of Potential Countermeasures

Countermeasures are elements of a design that are intended to help reduce the number and severity of crashes within a project area. For Lincoln Avenue, up to 4 countermeasures have been identified that could be applied in some form. Each countermeasure has a crash modification factor (CMF) associated with it. The CMF number reflects the effectiveness of the countermeasure, with any number lower than 1 representing a reduction in crashes, a 1 indicates no change in crashes, and anything over a 1 rating is likely to generate an increase in crashes. These CMF numbers are determined from the [Crash Modification Factors Clearinghouse](https://www.cmfclearinghouse.org/). The CMF Clearinghouse is funded by the Federal Highway Administration and maintained by the North Carolina Highway Safety Research Center. The CMF Clearinghouse provides roadway and traffic professionals with a repository of quantified expectations of crash changes per countermeasure, along with a quality rating to best determine how countermeasures may impact crashes.

The countermeasures identified for Lincoln Avenue include lane reduction, mid-block pedestrian crossing with rectangular rapid flashing beacons (RRFBs), installation of bike lanes, and access control by reducing the number of unsignalized crossroads. 

See the [comment map](https://ccrpc.gitlab.io/lincoln-avenue-scenario-phase/) or scenario diagrams below for more information about the location of individual countermeasures.

### Lane Reduction
*[Lane Reduction / Road Diet](https://www.cmfclearinghouse.org/detail.php?facid=2841) – 0.53 CMF applies to All Crash Types*

*[Install Bike Lanes](https://www.cmfclearinghouse.org/detail.php?facid=7838) – 0.68 CMF applies to All Crash Types*

Lane reduction, or a road diet, helps to calm traffic and provides a safety benefit for pedestrians and bicyclists by providing a shorter crossing distance at intersections and reducing lane changing movements. By reducing the number of traffic lanes, bike lanes or other facilities may be added in the vacated right-of-way space, allowing safer travel for bicyclists. 

Lane reduction on the Lincoln Avenue Corridor would consist of insitituting a consistent, three-lane-wide alignment for the whole corridor, consisting of one lane in each direction with a center left turn lane. Because the current road width differs throughout the corridor, the exact roadway design will also differ. More detailed design information will be provided when it becomes available. In order to transition safely to this three lane section, the two approaches to the corridor (southbound at Green Street and northbound at Florida Avenue) will have their existing straight/right-turn lanes converted into solely right turn lanes. This will ensure that only one lane of traffic enters the corridor.

### Signalized Pedestrian Crossings
*[Mid-Block Crossing with RRFBs](https://www.cmfclearinghouse.org/detail.php?facid=11181) – 0.82 CMF applies to Vehicle/Pedestrian Crash Type*

Mid-block crossings with rectangular rapid flashing beacons (RRFBs) help to improve the safety of pedestrians crossing the street at unsignalized locations. The RRFB’s increase the visibility of mid-block pedestrian crossings with flashing yellow lights to indicate to drivers that a person is entering the road.

These beacons will help address concerns from both drivers and pedestrians regarding current confusion about safe crossing of Lincoln, making it clear when pedestrians intend to cross and vehicles must yield right of way. The current proposal calls for installation of RRFBs at Oregon Street, Iowa Street, and Michigan Avenue, as well as a mid-block beacon between Ohio Street and Indiana Avenue.

### Access Control
*[Access Control](https://www.cmfclearinghouse.org/detail.php?facid=3097) – 0.56 CMF applies to All Crash Types*

Access management is a systematic means of balancing the access and mobility requirements of streets and roads. It involves the coordination between land use and transportation development practices to manage the location, design, and operation of driveways, median openings, and street connections to roadways. Implementing access management guidelines helps provide a safe and efficient transportation system. By closing access to Lincoln Avenue from some of the side roads, traffic can be consolidated at specific access points. This can help to ease congestion, reduce crashes, and provide a safety benefit for pedestrians and bicyclists.

Access management along the Lincoln Avenue corridor was evaluated by four criteria outlined in the [CUUATS Access Management Guidelines (2013)](https://ccrpc.org/transportation/access_management_guidelines.php): intersection functional area, signalized access spacing, unsignalized access spacing, and driveway spacing. *Several locations along the corridor fall short of the recommendations associated with one or more of the four criteria.* These guidelines were previously discussed in the [Existing Conditions](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/transportation/#access-management) section of the plan—notably, the guidelines call for a minimum of 660 feet between unsignalized accesses onto a minor arterial like Lincoln Avenue, while many of the streets accessing Lincoln Avenue from the east are spaced less than 400 feet apart.

In addition to the existing corridor configuration falling short of current access management best practices, pedestrians in the area find it difficult to cross Lincoln Avenue. Protected crossing solutions such as signalized intersections or rectangular rapid flashing beacons (RRFBs) at mid-block crossings were supported through previous rounds of public feedback.

Counts were taken of  of the existing peak hour traffic volumes on Lincoln Avenue's eastern side streets, seen below. These counts confirmed the relatively low quantity of automobiles using these intersections, reducing the likelihood that vehicles redirected by access management interventions would lead to signficant traffic increases on adjacent streets. 

{{<image src="Traffic at unsignalized intersections.png"
  alt="Diagram of vehicle turn counts at four Lincoln Avenue side streets."
  position="full">}}

This project provides an opportunity to both improve access management throughout the corridor and implement protected mid-block crossings via RRFBs. Therefore, the following changes to access are proposed throughout the corridor study area:
- ***Install a RRFB mid-block crossing at (east) Oregon Street.*** This provides a protected crossing approximately 500-600 feet away from two existing signals (Illinois and Nevada). The east leg of Oregon Street would be closed for vehicular traffic to/from Lincoln Avenue to accommodate the mid-block pedestrian crossing.
- ***Convert (east) Nevada Street to a right-in, right-out only access.***
   - Access connections, such as driveways or median openings, are not recommended in the intersection functional area because they could create conflict points, which the driver approaching or exiting an intersection may not be able to negotiate safely. The desirable and limiting/necessary values for the upstream and downstream functional distances at this location are 660ft (desirable) and 200ft -315ft (limiting/necessary). The east intersection of Nevada Street is less than 200 ft away from the W Nevada Street signalized intersection, and therefore does not conform to the necessary functional distance values for the corridor. 
- ***Install a RRFB mid-block crossing at Iowa Street.*** This provides a protected crossing approximately 500 feet south of the existing signal at Nevada Street. Iowa Street would be closed for vehicular traffic to/from Lincoln Avenue to accommodate the mid-block pedestrian crossing.
- ***Install a RRFB mid-block crossing south of Ohio Street and north of Indiana Street.*** This provides a protected crossing approximately 500 feet south of the proposed mid-block crossing at Iowa Street. Indiana Street would be closed for vehicular traffic to/from Lincoln Avenue to accommodate the mid-block pedestrian crossing.
- ***Install a RRFB crossing at Michigan Avenue.*** This provides a protected crossing approximately 500 feet south of the proposed mid-block crossing north of Indiana Street.
- ***Convert Vermont Avenue to a right-in, right-out only access.*** 
   - Access connections, such as driveways or median openings, are not recommended in the intersection functional area because they could create conflict points, which the driver approaching or exiting an intersection may not be able to negotiate safely. The desirable and limiting/necessary values for the upstream and downstream functional distances at this location are 660ft (desirable) and 200ft -315ft (limiting/necessary). The intersection of Vermont Avenue is approximately 250 ft away from the W Pennsylvania Avenue signalized intersection, and therefore does not conform to the desirable functional distance values for the corridor. 

Below are the anticipated changes to automobile routes, resulting from these access management changes - 

- Northbound right at Oregon - Assumed to turn right at Nevada or California
- Southbound left at Oregon and Nevada - Assumed to turn left at Illinois or California
- Northbound right at Iowa - Assumed to turn right at Ohio or Nevada
- Southbound left at Iowa and Indiana - Assumed to turn left at Michigan 
- Northbound right at Indiana - Assumed to turn right at Michigan or Ohio
- Southbound left at Vermont - Assumed to turn left at Pennsylvania or Delaware


## Bike and Pedestrian Scenarios

The following are three potential scenarios for bike and pedestrian infrastructure along the corridor. Below each description, you will find a diagramatic image illustrating the potential changes proposed in this scenario. In addition, you will find a link to a drawing from the project engineering team, demonstrating in more detail the geometry and specific changes proposed in the scenario.


### Scenario 1 - Bike Lanes Along Entirety of Corridor

The first of the three possible scenarios calls for the development of on-street bike lanes for the whole length of the corridor. This scenario would allow for a consistent path of travel for cyclists across the entire length of the corridor, before transitioning to different infrastructure at the edge of the corridor. While the realignment of the roadway to three automobile travel lanes will free up space for these bike lanes througout the corridor, the exact design and alignment will differ as road width and other context changes along the length of the corridor. 

***See design drawing for Scenario 1 [here](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/LincolnAveConcept01-Rotated.pdf).***

{{<image src="Scenario Visualizations.png"
  alt="Diagram of Scenario 1 changes, including bike lanes along length of corridor"
  caption="Right-click and open the image in a new tab to zoom and see detail."
  position="full">}}

### Scenario 2 - Bike Lanes, with Shared-Use Path between Iowa and Pennsylvania

The second of the three possible scenarios calls for the development of on-street bike lanes north of Iowa Street and south of Pennsylvania Avenue, while the area between these streets is served by a shared-use path on the west side of the roadway. This design incorporates the [existing shared-use path](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/transportation/#bike-and-pedestrian-facilities) between Iowa Street and Michigan Avenue, as well as a proposed shared-used nature trail along Illini Grove that is called for in the [Urbana Bicycle Master Plan](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/plans-reports-and-studies/#urbana-bicycle-master-plan-ubmp-2016) and [University of Illinois Campus Master Plan](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/plans-reports-and-studies/#university-of-illinois-at-urbana-champaign-campus-master-plan-2018). 

The bike lanes north of Iowa Street address the gap in bike infrastructure for this corridor, which neither of the above plans specifically address. THe bike lanes south of Pennsylvania Avenue similarly address the gap in cycling infrastructure for this portion of the corridor. These bike lanes to the south will also take advantage of roadway space that is freed up by the three-lane realignment.

***See design drawing for Scenario 2 [here](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/LincolnAveConcept02-Rotated.pdf).***

{{<image src="Scenario Visualizations2.png"
  alt="Diagram of Scenario 2 changes, including bike lanes, with a shared-used path between Iowa Street and Pennsylvania Avenue."
  caption="Right-click and open the image in a new tab to zoom and see detail."
  position="full">}}

  ### Scenario 3 - Bike Lanes North of Iowa Street, Shared-Use Path South of Iowa Street 

The third of the three possible scenarios calls for the development of on-street bike lanes north of Iowa Street, while the area south of Iowa Street is completely served by a shared-use path on the west side of the roadway. This design incorporates the [existing shared-use path](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/transportation/#bike-and-pedestrian-facilities) between Iowa Street and Michigan Avenue, a proposed shared-used nature trail along Illini Grove that is called for in the [Urbana Bicycle Master Plan](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/plans-reports-and-studies/#urbana-bicycle-master-plan-ubmp-2016) and [University of Illinois Campus Master Plan](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/plans-reports-and-studies/#university-of-illinois-at-urbana-champaign-campus-master-plan-2018), and another proposed shared-use path between Pennsylvania Street and Florida Avenue that is called for in these plans. 

The bike lanes north of Iowa Street address the gap in bike infrastructure for this corridor, which neither of the above plans specifically address, while implementing the shared-use paths south of Iowa Street follows the recommendations of these existing plans. Developing shared-use paths for the entire section of the corridor south of Iowa Street will allow for only one transition between cycling infrastructure types on the corridor, while connecting to existing or planned shared-use paths to the east, south, and west of the intersection of Lincoln Avenue and Florida Avenue.

***See design drawing for Scenario 3 [here](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/LincolnAveConcept03-Rotated.pdf).***

{{<image src="Scenario Visualizations3.png"
  alt="An example in the wild"
  caption="Right-click and open the image in a new tab to zoom and see detail."
  position="full">}}
 

## UPDATE - Lincoln Avenue Corridor Anticipated Vehicle Impacts from Access Management 

Following concerns about vehicle impacts from the access closures discussed above, the project engineer conducted modeling, projecting futre traffic volumes on West Urbana side streets both with and without access closures. This analysis can be found below.

### Green Street to Nevada Street (West)

It is recommended Oregon Street (east) be closed at Lincoln Avenue to vehicular access but maintain full access for pedestrians and bicyclists to utilize the proposed mid-block rapid rectangular flashing beacon (RRFB) crossing. Nevada Street (east) is to change from a full access side street stop-controlled intersection to limited right-in right-out (RIRO) access without pedestrian crossing treatments. 

-	The southbound left turning traffic previously utilizing Oregon Street (east) is expected to utilize Illinois Street and California Street instead, while the northbound right turning traffic previously utilizing Oregon Street (east) is expected to use Nevada Street and California Street. This limits the resulting diversion to a one to two block route.
-	The southbound left turning traffic from Lincoln Avenue currently using Nevada Street is likely to use Illinois Street and California Avenue resulting in a diversion of two to three blocks. 
-	Illinois Street can expect approximately one additional vehicle during the morning and six vehicles during the afternoon peak hours from the southbound left turning traffic along Lincoln Avenue. This movement is expected to experience an operational level of service A with one car or less queueing in the morning and afternoon peak hours. These diverted traffic volumes can be reviewed in the vehicle re-routing graphic.  
-	As shown in the same graphic, California Avenue can expect to see an increase in approximately four vehicles (one from Lincoln Avenue southbound left turn and three from the northbound right turn) and nine vehicles (six from the Lincoln Avenue southbound left turn and three from the northbound right turn) in the morning and afternoon peak periods, respectively. The southbound left turning traffic from Lincoln Avenue at California Street is expected to operate at a level of service A with one car or less queueing during the morning and afternoon peak hours.
-	Nevada Street is expected to experience an increase of four vehicles in the morning and six vehicles during the afternoon peak hours turning right from the northbound direction on Lincoln Avenue.  
-	Re-routings on Busey Avenue at Nevada Street and Oregon Street are minimal, with no re-routed volume exceeding 8 vehicles per movement for either peak hour.

### Nevada Street (West) to Pennsylvania Avenue

It is recommended the Iowa Street approach be closed at Lincoln Avenue to vehicular access but maintain full access for pedestrians and bicyclists to utilize a mid-block RRFB crossing. Ohio Street is to remain a full access side street stop-controlled intersection without pedestrian crossing treatments. It is recommended the Indiana Avenue approach be closed at Lincoln Avenue to vehicular access but maintain full access for pedestrians and bicyclists to utilize a mid-block RRFB crossing. Michigan Avenue is to remain a full access side street stop-controlled intersection.

-	The southbound left turning vehicular traffic from Lincoln Avenue using Iowa Street is expected to instead use Michigan Avenue creating a diversion of three blocks. The northbound right turning vehicular traffic from Lincoln Avenue is likely to use Ohio Street or Nevada Street experiencing a diversion of one to two blocks.
-	Southbound left turning and northbound right turning traffic from Lincoln Avenue that currently use Indiana Avenue are expected to use Michigan Avenue and Ohio Street, diverting one block. 
-	Ohio Street will see an additional five vehicles during the morning peak hour and fourteen vehicles during the afternoon peak hour from the southbound left turning movement at Lincoln Avenue. Ohio Street will have an additional two vehicles and four vehicles during the morning and afternoon peak hours turning right from the northbound direction of Lincoln Avenue as shown in the vehicle re-routing graphic. 
-	The southbound left turning movement from Lincoln Avenue to Ohio Street is expected to operate at a level of service B with one or less vehicles queueing during the morning and afternoon peak hours. 
-	As depicted in the vehicle rerouting graphic, Michigan Avenue will experience an additional five vehicles from left turning southbound traffic on Lincoln Avenue during the morning peak hour and fourteen vehicles during the afternoon peak hour. There will also be an additional one vehicle during the afternoon peak hour from the northbound right turning traffic on Lincoln Avenue. 
-	The southbound left turning traffic from Lincoln Avenue to Michigan Ave can expect to experience a level of service B with one or less vehicles queueing during the morning peak hour and a level of service A with one or less vehicles queueing during the afternoon peak hour. 
-	The closing of Iowa Street at Lincoln Avenue is expected to contribute an additional eight vehicles in the morning and twenty-three vehicles in the afternoon peak hours to Busey Avenue. 
-	The closing of Indiana Avenue at Lincoln Avenue is expected to contribute an additional four vehicles in the morning peak hour and four vehicles in the afternoon peak hour to Busey Avenue as redirected traffic.

### Pennsylvania Avenue to Florida Avenue

Vermont Street is to change from a full access side street stop-controlled intersection to limited right-in right-out (RIRO) access without pedestrian crossing treatments. 

-	Southbound left turning traffic from Lincoln Avenue currently utilizing Vermont Avenue is expected to use Pennsylvania Avenue and Delaware Avenue diverting one block. 
-	Due to the removal of the southbound left turn access, seven vehicles in the morning peak hour and ten vehicles in the afternoon peak hour are expected to use Pennsylvania Avenue (3 vehicles in the morning and five in the afternoon) and Delaware Avenue (four vehicles in the morning and five in the afternoon). These traffic re-routing volumes can be viewed on the corresponding graphic. 
-	The Pennsylvania Avenue southbound left turn movement is expected to operate at a level of service A with less than one vehicle queueing in the morning and afternoon peak hours.
-	The southbound left turning movement from Lincoln Avenue at Delaware Avenue is also expected to operate at a level of service A with less than one vehicle queueing during the morning and afternoon peak periods.

### Busey Avenue

Busey Avenue will remain a corridor with two-way stop-controlled intersections and several residential access drives from Green Street to Florida Avenue. The corridor and specific intersections will experience an increase in vehicular traffic due to the re-routed vehicles from the access closures. Busey Avenue carries an average daily traffic count of 250 vehicles (counted in 2021) north of Nevada Street and 275 vehicles (counted in 2021) south of Nevada Street indicating that there is sufficient capacity for Busey Avenue to accommodate the re-routed traffic from Lincoln Avenue. Busey Avenue is comprised of asphalt and brick sections of roadway surface. The physical roadway conditions of Busey Avenue were not a strong factor in determining access closures; rather, the safety and accommodations of vulnerable road users were the priority of the project. The intersection control types (which direction is two-way stop control, potential all-way stop control) along the Busey Avenue corridor from Green Street to Florida Avenue should be re-evaluated as part of the implementation process to ensure appropriate treatments are provided considering operations and sight-distance. 

{{<image src="Diversion Volume graphic_Page_2.png"
  alt="Diagram of projected vehicle turn counts on Lincoln Avenue"
  attr= "Lochmueller Group"
  position="center">}}

{{<image src="Diversion Volume graphic_Page_1.png"
  alt="Diagram of projected vehicle turn counts on Lincoln Avenue, with access closures."
  attr= "Lochmueller Group"
  position="center">}}



