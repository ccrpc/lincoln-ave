---
title: Future Conditions
draft: false
menu: main
weight: 25
---

As we gather data and public sentiment, this section will feature possible scenarios for addressing transportation issues along Lincoln Avenue.

Check back here as the corridor study progresses!