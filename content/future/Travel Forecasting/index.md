---
title: Travel Forecasting
draft: false
weight: 15
---

Traffic growth rates based on an analysis of the following data sources were summarized for the four segments along the Lincoln Avenue study corridor:

1.	**Historic AADT from the Illinois Department of Transportation (IDOT) Traffic Volume Maps**

      The Illinois Department of Transportation (IDOT) provides historic traffic data in an online format. IDOT collects ADT data every five years on the regional transportation network. Staff evaluated online Average Daily Traffic (ADT) data for the study corridor. The historic ADT data along the study corridor was collected in 2011, 2016, and 2021. 

2.	**MPO Travel Demand Model** 

      The Champaign County Regional Planning Commission (CCRPC) is the designated Metropolitan Planning Organization (MPO) for the Champaign-Urbana Metropolitan Planning Area. CCRPC developed and maintains a Travel Demand Model (TDM) which includes the Metropolitan Planning Area (MPA) roadway network. The CCRPC TDM base year is 2015, and the horizon year is 2045. CCRPC provided ADT information along the study corridor for its base and horizon years. 

The four corridor segments include:

1. Between Florida Avenue and Pennsylvania Avenue
2. Between Pennsylvania Avenue and Nevada Street
3. Between Nevada Street and Illinois Street
4. Between Illinois Street and Green Street

Trendlines were extrapolated forward from the historic traffic counts. Trendlines were also developed for the CCRPC TDM by comparing base and horizon year outputs. The selected segments, illustrated in **Figure 1**, were chosen based on the availability of historical count data and location of intersection controls. 

{{<image src="fig 1.jpg"
  alt="Map of Lincoln Avenue Corridor analysis segments"
  caption="Figure 1: Traffic Volume Forecasting Locations"
  position="full">}}


## 1. Lincoln Avenue – Between Florida Avenue and Pennsylvania Avenue

Traffic volume trends and forecasts for the Lincoln Avenue segment between Florida Avenue and Pennsylvania Avenue are summarized in **Figure 2**. The orange trendline, that follows the IDOT historic traffic count data, indicates that traffic volume growth has been negative between 2011 and 2021. However, based on a thorough evaluation of 2021 ADT volumes throughout the University of Illinois campus area, we have identified that 2021 ADT volumes were much lower than expected due to COVID-19 pandemic-related disruptions. Therefore, the traffic growth rate of **-1.8** percent does not reflect this segment's expected historic traffic growth rate. 

The blue points in **Figure 2** represent the forecasted traffic from the CCRPC TDM at this segment. The blue trendline indicates that the forecasted annual traffic growth will be **1.1** percent. 

Based on a thorough knowledge of regional traffic flow patterns, evaluation of COVID-19 pandemic-related impacts, and CCRPC TDM forecasts, the Lochmueller team recommends **0.4** percent annual traffic growth for this segment.

{{<image src="fig 2.png"
  alt="Graph of traffic volumne trendlines for the segment from Florida to Pennsylvania"
  caption="Figure 2: Traffic Volume Trendlines for Lincoln Avenue (Florida - Pennsylvania)"
  position="full">}}

## 2. Lincoln Avenue – Between Pennsylvania Avenue and Nevada Street

**Figure 3** shows historical and forecasted traffic volumes for the segment of Lincoln Avenue between Pennsylvania Avenue and Nevada Street.  The graph also contains linear trend lines for each dataset. The IDOT 2021 counts reflect COVID-19 pandemic-related impacts on the daily traffic flow, and as a result, historical traffic volumes show a traffic growth of **–2.4** percent.

The blue points in **Figure 3** represent the base and horizon years traffic forecasts from the CCRPC TDM at this segment. The blue trendline indicates that this segment's forecasted annual traffic growth rate will be **1** percent. 

Based on a thorough knowledge of regional traffic flow patterns, evaluation of COVID-19 pandemic-related impacts, and CCRPC TDM forecasts, the Lochmueller team recommends an annual traffic growth rate of **0.4** percent for this segment. 

{{<image src="fig 3.png"
  alt="Graph of traffic volumne trendlines for the segment from Pennsylvania to Nevada"
  caption="Figure 3: Traffic Volume Trendlines for Lincoln Avenue (Pennsylvania - Nevada)"
  position="full">}}

## 3.	Lincoln Avenue – Between Nevada Street and Illinois Street

The historical and forecasted traffic volumes for the segment of Lincoln Avenue between Nevada Street and Illinois Street are shown in **Figure 4**. The orange trendline, following the IDOT historic traffic count data, shows a growth rate of **0.71** percent. The 2021 ADT volumes were much lower than expected due to COVID-19 pandemic-related disruptions.

The blue points in **Figure 4** represent the base and horizon years traffic forecasts from the CCRPC TDM at this segment. The blue trendline indicates that this segment's forecasted annual traffic growth rate is **0.7** percent. 

Based on a thorough knowledge of regional traffic flow patterns, evaluation of COVID-19 pandemic-related impacts, and CCRPC TDM forecasts, the Lochmueller team recommends an annual traffic growth rate of **0.5** percent for this segment.

{{<image src="fig 4.png"
  alt="Graph of traffic volumne trendlines for the segment from Nevada to Illinois"
  caption="Figure 4: Traffic Volume Trendlines for Lincoln Avenue (Nevada - Illinois)"
  position="full">}}

## 4.	Lincoln Avenue – Between Illinois Street and Green Street

In **Figure 5**, the historical and forecasted traffic volumes for the segment of Lincoln Avenue between Illinois Street and Green Street are shown. The IDOT historic traffic count data shows low counts for the 2021 ADT count due to COVID-19 pandemic-related disruptions. Therefore, the traffic growth rate will be **0.71** percent.

The blue points in **Figure 5** represent the base and horizon years traffic forecasts from the CCRPC TDM at this segment. The blue trendline indicates that this segment's forecasted annual traffic growth rate will be **0.93** percent. 

Based on a thorough knowledge of regional traffic flow patterns, evaluation of COVID-19 pandemic-related impacts, and CCRPC TDM forecasts, the Lochmueller team recommends an annual traffic growth rate of **0.5** percent for this segment. 

{{<image src="fig 5.png"
  alt="Graph of traffic volumne trendlines for the segment from Illinois to Green"
  caption="Figure 5: Traffic Volume Trendlines for Lincoln Avenue (Illinois - Green)"
  position="full">}}

## Recommendations

The recommended traffic growth rates by corridor are summarized in **Table 1**. This reflects the previous analysis of the input data sources from IDOT’s historic traffic count data and provided travel demand model from the CCRPC. 

<rpc-table url="table 1.csv"
  table-title="Table 1: Recommend Traffic Growth Rates by Location"
  text-alignment="l,r"></rpc-table>


These annual growth rates will be applied to origin-destination flows capturing a combination of growth rates from the table above depending upon the starting and ending points of each route. Therefore, a blended 0.45% rate reflecting the applicable combination of rates above will be applied to turning movements along the four segments to determine the traffic volume for 2048. 