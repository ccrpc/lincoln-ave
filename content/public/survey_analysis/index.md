---
title: Phase One - Survey Comment Analysis
weight: 30
---

The first round of public outreach for the Lincoln Avenue Corridor Study resulted in a total of ***345 survey responses***. Responses were identified as fitting into these broad categories by topic, depending on the contents of each comment:

1.	**Roadway Function/Design**
2.	**Multimodal Facilities**
3.	**Visibility**
4.	**Safety**
5.	**Connectivity**
6.	**Signaling and Signs**
7.	**Pavement Maintenance**
8.	**Other**

## Question Summaries

The following charts display the number of times that each of the topics were mentioned within the comments pertaining to each question. *(Comments mentioned multiple topics, so the percentages presented do not equal 100%)*

<br>

***QUESTION 4:** What do you <u>LIKE</u> about the current transportation conditions on Lincoln Avenue between Green Street and Florida Avenue?*

<rpc-chart
url="Survey_LikesCount.csv"
type="horizontalBar"
colors="#9ACD32"
stacked="false"
x-label="Response Count"
x-min="0"
x-max="160"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Total Comments - Likes"></rpc-chart>

<rpc-table url="Q4_Top_Issues.csv"
  table-title="Question 4 - Top Issues"
  source="Lincoln Avenue Corridor Study Survey"
  text-alignment="l,r"></rpc-table>


<br>

***QUESTION 5:** What do you <u>NOT LIKE</u> about the current transportation conditions on Lincoln Avenue between Green Street and Florida Avenue?*

<rpc-chart
url="Survey_DislikesCount.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Comment Count"
x-min="0"
x-max="250"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Total Comments - Dislikes"></rpc-chart>

<rpc-table url="Q5_Top_Issues.csv"
  table-title="Question 5 - Top Issues"
  source="Lincoln Avenue Corridor Study Survey"
  text-alignment="l,r"></rpc-table>


<br>

***QUESTION 6:** What <u>CHANGES</u> would you like to see implemented on Lincoln Avenue between Green Street and Florida Avenue as a result of this study?*

<rpc-chart
url="Survey_ChangesCount.csv"
type="horizontalBar"
colors="#C0C0C0"
stacked="false"
x-label="Comment Count"
x-min="0"
x-max="300"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Total Comments - Changes"></rpc-chart>

<rpc-table url="Q6_Top_Issues.csv"
  table-title="Question 6 - Top Issues"
  source="Lincoln Avenue Corridor Study Survey"
  text-alignment="l,r"></rpc-table>


## Topic Summaries  
This section provides a summary of the comments as they pertain to each of the topics (including likes, dislikes, and changes). The numbers provided at the end of sentences represent the total count of comments that mention the particular topic of discussion. Because Lincoln Avenue is the focus of this study, all intersections will be referred to solely by their cross street (for example, the intersection of Lincoln Avenue and Pennsylvania Avenue will just be referred to as "at Pennsylvania Avenue"). In any circumstance where the described location is not on Lincoln Avenue, both streets will be specified. 

<rpc-chart
url="Survey_TopIssuesPercentageAll.csv"
type="verticalBar"
colors="#9ACD32,#4169E1,#C0C0C0"
stacked="false"
y-label="Percentage of Comments"
y-min="0"
y-max="80%"
legend-position="top"
legend="true"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Top Issues by Topic"></rpc-chart>

## Roadway Function/Design

<u>***Dislikes***</u> - Many respondents cited confusion with the many changes in the roadway configuration in this section of the corridor (72). For instance, the merge from two lanes of traffic to one lane heading south at Nevada Street was a common criticism amongst responses, as was the merge heading north at Florida Avenue. Another dissatisfaction relates to the roadway width; some cited that the roadway feels too wide to safely cross as a pedestrian, especially at Nevada Street. Others feel that the lanes are too narrow to safely bike along with traffic, given that there are no on-street facilities currently available for cycling. Regardless of the width, many agree that the road is too close to existing pedestrian facilities, and there is a need for better separation between automobile and pedestrian traffic in order to safely utilize the corridor (29). 

Additionally, respondents cited frustration over roadway congestion, especially in the evening (38). Comments relayed difficulty turning from side streets onto Lincoln Avenue (and vice versa), long wait times at intersections, buses stopped in traffic, and anxiety over stopping suddenly for fear of crashing. Respondents also report frustration with automobiles traveling too fast along the corridor (56). Pedestrians expressed feeling unsafe and fearful to cross Lincoln Avenue, especially at unsignalized intersections, due to fast moving traffic. Other notable comments mentioned dissatisfaction with cars parked on Lincoln Avenue at the Jimmy John’s at Nevada Street, saying that it obstructs the view when looking for oncoming traffic and pedestrians (6).

<u>***Changes***</u> - Most of the comments related to roadway function in Question 6 concerned changing the roadway design (77). These comments involved a variety of suggestions: increasing the number of lanes, reducing the number of lanes, omitting the center turn lane, incorporating traffic calming measures, and changing intersections to roundabouts (4), including at Green Street and Florida Avenue. Comments also suggested reducing the speed limit along the corridor to as low as 15 miles per hour, to ensure safer crossing conditions for pedestrians and to slow down traffic (39). Respondents voiced a desire to reduce traffic congestion, especially at intersections (12). Additionally, responses involved suggestions to better separate automobile traffic from pedestrians and cyclists by moving sidewalks and future facilities further back from the roadway (28).

<u>***Likes***</u> - Most Roadway Function-related responses to Question 4 mentioned enjoying the improvements previously made to Lincoln Avenue, citing that the corridor functions better now (67). Comments also mentioned that the corridor is good for driving (15), the speed limit is appropriate (19), and congestion is not an issue (15). Other respondents feel that the corridor is multi-modal friendly (8).

<br>

## Multimodal Facilities

<u>***Dislikes***</u> - Many Multimodal Facilities responses to Question 5 shared a dissatisfaction in the lack of current on-street bicycle facilities along the corridor (66). Those who currently bike on the street note that the roadway feels too narrow and dangerous to share with automobile traffic. Some respondents feel safer using off-street facilities, such as existing sidewalks, but also expressed difficulty with the narrow width of the sidewalk when shared with pedestrians. Regarding pedestrian facilities, most comments expressed interest in additional pedestrian crossings along the corridor, particularly between Illinois Street and Nevada Street and south of Pennsylvania Avenue (30). More specifically, Oregon Street was frequently mentioned by respondents as a acrosswalk location. Additionally, respondents expressed dissatisfaction with the current sidewalk width, citing that it is too narrow for pedestrians (18). Other comments involved changing the current crosswalk design (7), increasing bus routes (7), adding bus turnouts (5), shortening crosswalk distances (3), and adding medians (3).

<u>***Changes***</u> - Of all the comments from Question 6, the majority involved changes to current multimodal facilities (242). Over half of those comments included responses suggesting bicycle facilities along the corridor. On-street bicycle facilities were mentioned frequently (106), as were off-street facilities but at a less frequent rate (33). However, many respondents continued to express interest in increasing the separation between automobiles and pedestrians or cyclists (28), indicating that an off-street option may suffice for both cyclists and pedestrians in terms of safety and accessibility. 

As for pedestrian facilities, respondents suggest adding more pedestrian crossings along the corridor, especially between Illinois Street and Nevada Street (32). Many of these responses also called for signalized pedestrian crossings, similar to the crossing on Springfield Avenue at the UIUC Engineering Quad. Comments also called for changes to current crosswalk design (such as shifting to raised crosswalks), protected crossings, utilizing bump outs, and creating better designated areas of crossing (27). In addition, some respondents suggested adding medians to crossings (5) and shortening the distance of crosswalks (4). Finally, respondents suggested widening existing sidewalks (15).

Some respondents suggested increasing bus route access and frequency (5), incorporating bus turnouts to reduce bus conflicts (5), adding a bus lane (4), and adding bus shelters to stops (3).

<u>***Likes***</u> - Most respondents enjoy that there are plenty of crosswalks at varying places along the corridor, which felt protected and well-marked (51). Other responses cited that they appreciated the consistent access to sidewalks from at least one side of the corridor, while others noted the sidewalk was a good width (38). Respondents were also satisfied by access to bus service and various routes along the corridor (23). Additional bus-related comments included satisfaction with bus stop locations (20) and the existing space for buses to pull off at stops (10). Other comments involved liking existing bus shelters (3), crosswalk design (3), crosswalk distance (1), and medians (1).

<br>

## Visibility
<u>***Dislikes***</u> - Visibility was a topic frequently mentioned in responses, with a majority mentioning difficultly seeing pedestrians attempting to use or currently using pedestrian facilities (86) or bicycle facilities (7). Comments regarding pedestrian visibility cited difficulty seeing pedestrians in crosswalks, pedestrians having to “jump” into traffic to get the attention of oncoming traffic, and cars not stopping for pedestrians using facilities. Responses also cited that physical obstacles and existing conditions make visibility difficult for automobile drivers (11). The noted obstacles include parked cars outside of Jimmy John’s and stopped buses; other points of conflict included turning left onto Lincoln Avenue from eastbound Pennsylvania Avenue and turning left onto Lincoln Avenue from neighborhood side streets. Overall, respondents felt that the corridor is overwhelming to navigate as an automobile driver.

Responses also mentioned that inadequate lighting along the corridor impairs visibility while driving and using pedestrian facilities (26).

<u>***Changes***</u> - Visibility responses to Question 6 mostly included improving pedestrian (22) and bicycle (6) visibility, which includes issues with drivers not being aware of pedestrians or bicyclists attempting to or actively crossing the corridor. Other suggestions involved improving automobile visibility (5) by removing obstacles that interfere with drivers’ sightlines, such as the parking outside of Jimmy John’s, south of Nevada Street. Improving lighting conditions for both pedestrians (10) and automobiles (10) was another frequently cited suggestion regarding visibility. 

<u>***Likes***</u> - Some respondents voiced satisfaction with visibility of pedestrians, citing that they felt cars stopped at crossings (17). Few comments mentioned liking lighting along the corridor (4) and visibility for drivers (1).

<br>

## Safety
<u>***Dislikes***</u> - The vast majority of Safety responses to Question 5 concerned pedestrian safety (59) and bicyclist safety (43), voiced by both pedestrians and automobile drivers. Many of these comments also mentioned issues with visibility, causing respondents anxiety and fear over hitting a pedestrian that they are unable to see. Some respondents mentioned feeling unsafe at crosswalks that are not located at signalized intersections due to failed stops at unsignalized crossings and fast-moving traffic. Many expressed that walking along the corridor feels unsafe due to the proximity of sidewalks to moving traffic; similarly, responses indicated feeling unsafe biking alongside traffic. Additionally, comments mentioned rough patches in the pavement and cracks in the sidewalk make conditions more dangerous. Some respondents cited that automobile safety was a concern, especially having to make sudden stops along the corridor at crosswalks (3). Finally, comments mentioned that some bus stops along the corridor feel unsafe because they are too close to the road and lack bus shelters (4).

<u>***Changes***</u> - Respondents voiced that they would like to see improvements made to enhance the safety of all users: pedestrian safety (42), bicyclist safety (21), and automobile driver safety (5). These suggestions include the following: moving sidewalks away from the road, adding a shared-use path (some suggesting through Illini Grove), adding protected bike lanes with barriers, improving curbs, educating all users on right-of-way, slowing down traffic, and installing pedestrian signals at crossings. Many comments simply stated they wanted safer pedestrian and cycling conditions, as well as safer conditions for all road users. 

<u>***Likes***</u> - There were very few Safety-related responses to Question 4. Those who did respond cited satisfaction with pedestrian safety (3) and automobile safety (2). 

<br>

## Connectivity 
<u>***Dislikes***</u> - Similar to the dissatisfaction over the lack of facilities, Connectivity comments in Question 5 noted a lack of pedestrian (23) and bicycle (37) connectivity across the corridor. Respondents mention this lack in connectivity makes travel along the corridor as a pedestrian or cyclist difficult, as infrastructure fails to meet up with existing facilities entering the university. For instance, some respondents mention having to bike through campus or West Urbana to avoid the corridor, though Lincoln Avenue offers them the most efficient route. Many comments expressed overlap with those mentioning a lack of facilities and feeling unsafe.

<u>***Changes***</u> - Comments for bicycle (19) and pedestrian (34) connectivity involved requesting more facilities, such as dedicated pedestrian crossings and bicycle infrastructure, that will connect with existing facilities and make for safer and efficient trips. Many of these comments overlapped with those requesting bicycle and pedestrian facilities along the corridor. Most comments simply expressed interest in making the corridor more accessible and pedestrian friendly. 

<u>***Likes***</u> - Similar to the satisfaction over pedestrian facilities, respondents voiced approval over access to plenty of crossings, which increase pedestrian connectivity (20) and bicycle connectivity (12) along the corridor. Comments mentioned liking how the corridor serves as a connection between the nearby neighborhood and campus infrastructure. Additionally, responses regarding automobile travel cited that the corridor serves as good north to south access in Urbana (11).

<br>

## Signals/Signs
<u>***Dislikes***</u> - Many of signal and sign comments in Question 5 mentioned dissatisfaction over the lack of signalized pedestrian crosswalks and push-button pedestrian signals to notify drivers that a pedestrian intends to cross the road (26). Others mentioned that the lack of clear signage for automobile drivers and pedestrians that is explicit about right of way results in confusion for all users (10). However, some comments stated that there are too many signs along the corridor, which are overall visually overwhelming. Many noted that the intersection signals need to be retimed and do not seem to function effectively (18).

<u>***Changes***</u> - An overwhelming number of comments regarding signals and signs involved requesting that pedestrian push signals with flashing signs be installed at pedestrian crossings along the corridor, similar to the one on Springfield Avenue at the UIUC Engineering Quad (64). Some comments suggested including stop signs at existing crosswalks to ensure traffic stops for pedestrians (7).

Responses also mentioned adding traffic lights to intersections, specifically at Oregon Street and between Nevada Street and Pennsylvania Avenue (10). Others cited the desire for more efficient traffic signal timing to minimize congestion, increase traffic flow, and increase pedestrian crossing safety (12) and crossing time (3); more specifically, respondents mentioned the  of Florida Avenue, Pennsylvania Avenue, and Green Street intersections as those needing retiming. 

Additionally, some comments expressed the desire for increased signage along the corridor that directs users towards areas of significance (such as Downtown Urbana) and indicates right-of-way at crosswalks (13). 

<u>***Likes***</u> - Most of the signal-and-sign-related responses to Queston 4 involved traffic lights (19) and signals (9). Comments mentioned satisfaction with the traffic light timing and operations, with the signalized intersections at Nevada Street and Florida Avenue stated numerous times. Some respondents also mentioned liking the timing for pedestrian walking signals at signalized intersections. Many responses voiced approval over the flashing pedestrian crossing at Michigan Avenue that was implemented with the previous corridor study, as well as the availability of pedestrian push buttons at signalized intersections (16). Respondents also appreciated having bicycle signals at signalized intersections (5). Finally, other comments related to the increased presence of pedestrian crossing signs (6) and signage (4). 

<br>

## Pavement Maintenance 
<u>***Dislikes***</u> - Comments in this category overwhelmingly involved poor pavement condition, noting that the road has many rough patches and potholes making it difficult for drivers and bicyclists to safely utilize the corridor (49). Others mentioned the sidewalk being in poor condition (13). Those who do not feel safe biking on the road and instead bike on the sidewalk cite having a difficult time due to the uneven pavement. Additional comments mentioned poor road markings (3).

<u>***Changes***</u> - Most of the comments regarding pavement maintenance involved suggestions to repave the roadway and improve pavement (34). Many respondents feel that the roadway is dangerous due to its uneven pavement. Other comments involved calls to repaint crosswalks (3), repair sidewalks (2), repair curb ramps (2), and eliminate sidewalk obstructions (2).

<u>***Likes***</u> - The comments in this section all involved roadway pavement condition, with the responses mentioning it not being an issue (17). 

<br>

## Other

<u>***Dislikes***</u> - Comments in this category were varied and included dissatisfaction with pedestrians crossing at non-crosswalks, wanting more beautification efforts along the corridor, and more wheelchair accessible infrastructure (18). Other comments were positive, mentioning that the roadway seems effective and well-maintained (13).

<u>***Changes***</u> - Responses in this section suggested converting crosswalks to a pedestrian bridge over Lincoln Avenue (11) and incorporating more wheelchair accessible infrastructure. Respondents also noted wanting to reduce the noise pollution from Lincoln Avenue, citing that it is loud when walking alongside traffic on the sidewalk (2). Other suggestions included the following: installing a streetcar, installing cameras for speed enforcement, and deemphasizing a vehicular focus.

<u>***Likes***</u> - Many of the comments in this section did not offer any positive feedback on the corridor (41). Others mentioned appreciating the corridor’s greenery and trees, access to commercial businesses, and campus architecture (18). Very few respondents mentioned being satisfied with the existing pedestrian infrastructure (2) and automobile conditions (1). 

