---
title: Phase Two - Summary
draft: false
weight: 50
---

The second round of public outreach for the Lincoln Avenue Corridor Study was conducted via a Google Forms survey and comment maps, which were linked to on the project website and available at an open house, hosted at the Illinois Street Residence Hall on April 23rd, 2024. This survey featured questions on the public’s opinion towards the proposed improvements, open ended questions regarding likes, dislikes, and other thoughts on the corridor, as well as optional questions about demographics and location of residency. The survey was available online from April 11th, 2024 to May 15th, 2024.

The following methods were used to make community members aware of the second round of public outreach: 
- Hosting an open house at the Illinois Street Residence Hall on the corridor
- Posting on the CUUATS Facebook, Instagram, and Twitter
- Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include:
    - CCRPC Communications office
    -   Champaign-Urbana Mass Transit District
    -   City of Urbana
    - PACE Center for Independent Living
    - The West Urbana Neighborhood Association
    -   Champaign County Bikes
    -   UIUC Facilities Department
    -   UIUC Panhellenic and Interfraternity Councils
-   Installing yard signs in the public right of way along the Lincoln Avenue corridor
-   Distributing flyers to businesses or gathering places near the corridor
- Distributing a press release about the outreach to local media

## Public Input Materials Summary
The survey and map responses and comments were obtained from the following:

-   **The Lincoln Avenue Corridor Study Survey:** The survey was available online (as well as through physical paper copies at the open house) to gain the public’s feedback on proposed improvements along the corridor. The survey resulted in ***192 responses***. Questions 1-4 pertained to preferences on proposed improvements; Q6 and Q7 asked about modes of transportation; Q5 and Q8 were open ended for additional comments on improvements or the overall project.

- **Interactive online map:** The comment map—[available online](https://ccrpc.gitlab.io/lincoln-avenue-scenario-phase/)—allowed to pinpoint specific areas of concern regarding proposed improvements, as well as sorting by specific scenarios.

- **Physical map:** Three physical maps displaying the proposed improvements pertaining to Scenario 1, Scenario 2, and Scenario 3 were available in-person at the public meeting held on April 23rd, 2024. The public was able to leave comments directly on the map using post-it notes. Following the open house, physical map comments were digitized onto the online map, resulting in ***95 cumulative map comments***.

# Response Summary

Of the proposed scenarios, 54% of survey respondents preferred Scenario 1: Continuous on-street bike lanes in both directions from Green to Florida. Overall, respondents favored scenarios with on-street bicycle facilities, specifically calling for protected bicycle lanes and separation in these facilities.

Respondents supported pedestrian safety infrastructure—such as RRFBs at crosswalks—and bettering traffic flow. On the other hand, of the proposed interventions, respondents were opposed to reducing side street access due to concern over neighborhood impact and traffic flow.

On the provided feedback maps, most respondent comments pertained to the proposed interventions that are consistent amongst all scenarios. Many comments called for modifying the proposal to relocate or consolidate crosswalks, in order to ensure the maximum crossing opportunities for pedestrians. Additionally, comments called for modifying the proposal for on-street bike lanes to ensure adequate separation and protection from vehicular traffic.

A detailed breakdown of the gathered feedback can be found in the following pages. [Phase Two - Survey Comment Analysis](https://ccrpc.gitlab.io/lincoln-ave/public/phase-two-survey/) analyzes the survey responses to the proposed interventions on Lincoln Avenue, while [Phase Two - Map Comment Analysis](https://ccrpc.gitlab.io/lincoln-ave/public/phase_two_map_comment/) provides analysis on the map comments. [Phase Two - Respondent Demographics](https://ccrpc.gitlab.io/lincoln-ave/public/phase_two_demographics/) provides self-reported information on the respondents to the survey, including demographic information, transportation mode access, and frequency and mode of travel along the Lincoln Avenue Corridor.