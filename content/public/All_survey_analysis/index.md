---
title: Phase One - Summary
weight: 25
draft: false
---

The first round of public outreach for the Lincoln Avenue Corridor Study was conducted via a Google Forms survey and online map, linked to in the project website at [ccrpc.org/lincoln](https://ccrpc.gitlab.io/lincoln-ave/), as well as an open house, hosted at the Alice Campbell Alumni Center on October 10th, 2023. This survey featured questions on respondent’s use and access to different transportation modes, open ended questions about their likes, dislikes, and other thoughts on the corridor, as well as optional questions about demographics and location of residency. The online survey was available online from September 18, 2023 to November 4, 2023.

The following methods were used to make community members aware of the first round of public outreach:

- Hosting an open house at the Alice Campbell Alumni Center on the corridor
- Posting on the CUUATS Facebook, Instagram, and Twitter
- Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include:
   - CCRPC Communications office
   - Champaign-Urbana Mass Transit District
   - City of Urbana
   - PACE Center for Independent Living
   - The West Urbana Neighborhood Association
   - Champaign County Bikes
   - UIUC Facilities Department
   - UIUC Panhellenic Council
- Installing yard signs in the public right of way along the Lincoln Avenue corridor
- Distributing flyers to businesses or gathering places near the corridor
- Discussing the study with reporters for news pieces in the News-Gazette, WCIA, and WAND
- Tabling at the Hallene Gateway during the annual Light the Night bike light giveway during C-U Bike Month.

## Response Summary

This page provides a broad summary of the comments received from the first round of public feedback. This includes comments from the following: 
- **The Lincoln Avenue Corridor Study Survey:** The survey was available online (as well as through physical paper copies) to gain the public's feedback on the study area. The survey resulted in ***345 responses***. Questions 4-6 were open-ended and used for the purposes of this comment analysis.

- **Interactive online map:** The map was available online using a web link, in which users could use to pinpoint specific areas of concern (i.e. automobile, pedestrian, bicycle, and bus).

- **A physical map:** A physical map of the study area was available in-person at the public meeting held on October 10th, 2023. The public was able to leave comments directly on the map using post-it notes. All map comments, both online and physical, were analyzed together, resulting in ***317 map comments***.

The charts and tables below provide a cumulative count of the open-ended comments provided from these three sources. A more specific breakdown of the comments as they pertain to the survey and map are located in the following tabs: [Phase One - Survey Comment Analysis](https://ccrpc.gitlab.io/lincoln-ave/public/survey_analysis/) and [Phase One - Map Comment Analysis](https://ccrpc.gitlab.io/lincoln-ave/public/map_analysis/). Analysis of the non-open-ended questions, including respondent demographics, travel patterns, and transportation mode use, can be found on [Phase One - Respondent Demographics](https://ccrpc.gitlab.io/lincoln-ave/public/phase_one_demo/). 

<rpc-table url="All_Mode_Count.csv"
  table-title="Comment Type by Mode"
  source="Lincoln Avenue Corridor Study Survey and Map Comments"
  text-alignment="l,r"></rpc-table>

<br>

<rpc-table url="Top_10_Issues_All.csv"
  table-title="Top 10 Issues from All Comments"
  source="Lincoln Avenue Corridor Study Survey and Map Comments"
  text-alignment="l,r"></rpc-table>