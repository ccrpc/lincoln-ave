---
title: Phase One - Respondent Demographics
draft: false
weight: 40
---

## Introduction

The first round of feedback was conducted via a Google Forms survey linked to in the project website at [ccrpc.org/lincoln](https://ccrpc.org/lincoln/). This survey featured questions on respondent’s use and access to different transportation modes, open ended questions about their likes, dislikes, and other thoughts on the corridor, as well as optional questions about demographics and location of residency. The online survey was available online from September 18, 2023 to November 4, 2023.

This page provides a summary of the non-open-ended survey questions, including questions about respondents' travel habits and mode use, residency, and demographics.

## Respondent Transportation

Based on the question “How often do you use each of the following transportation modes on Lincoln Avenue betweeen Green Street and Florida Avenue?”, private motor vehicles were the most common transportation mode on Lincoln Avenue, with 64% of respondents traveling the corridor by motor vehicle weekly, and 93% travelling this way at least annually. This was followed by walking (51% weekly, 84% annually), biking (38% weekly, 63% annually), riding the bus (16% weekly, 58% annually), and ride share (2% weekly, 27% annually). Three percent of respondents also reported using a wheelchair or motorized chair on the corridor at least once a year. 


<rpc-chart
url="survey_mode_use.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Respondent Transportation Mode Use on Lincoln Avenue Corridor"></rpc-chart>

These trends were also reflected in response to the question "Do you own or have reliable access to any of the following transportation modes?," where 80% of respondents had access to a motor vehicle, 75% had access to a bicycle, and 69% had access to a bus pass.


<rpc-chart
url="survey_mode_access.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Respondent Access to Transportation Modes"></rpc-chart>

This information is useful in considering improvements in the corridor, both encouraging travellers to take advantage of active transportation modes that they have access to and already utilize, and in acknowledging that automobiles will continue to play a role in transportation along this corridor.


## Area of Residence
Out of 345 respondents to the survey itself, 319 provided the nearest intersection to their home, as seen in the table and maps below.

<rpc-table url="survey_home.csv"
  table-title="Survey Respondent Residence Location"
  table-subtitle="Reported by Nearest Intersection"
  source="Lincoln Avenue Corridor Study Survey"
  text-alignment="l,r"></rpc-table>


{{<image src="Survey Map - All.png"
  alt="Map of all survey respondent residences, reported by the closest intersection"
  position="right">}}

{{<image src="Survey Map - AOI.png"
  alt="Map of survey respondent residences, reported by the closest intersection, zoomed in to focus on the area of influence"
  position="right">}}


This shows that responses came from across the Champaign-Urbana area; however, respondents were concentrated around the Study Area, with more than a third located within the Study Area, nearly half in the Area of Influence, and more than three quarters in the City of Urbana. This distribution provides a focus on those most likely to be affected by changes along the corridor; future outreach should focus on continuing to hear from these groups, in addition to corridor users who live outside of the area.

According to the 2020 Census, 6,055 people lived in the blocks within the Study Area, and 8,724 lived in the blocks in the Area of Influence. As mentioned in the [Transportation](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/transportation/) page of the Existing Conditions section of this plan, traffic count data from 2016 saw an average daily traffic of 16,100 on the north end of the corridor and 13,400 on the south end of the corridor, while more recent data from 2021, taken during the COVID-19 pandemic, showed counts of 13,100 at the north end of the corridor and 10,700 at the south end of the corridor. This data shows substantial populations both residing near the corridor and travelling along it daily; improvements to the corridor should consider the varied needs of these  different but overlapping population groups.

## Age

The distribution of survey responses is split approximateley halfway between respondents younger than 40 (53%) and 40 or older (47%). This indicates that respondents 40 and above are significantly overrepresented not only relative to the Area of Influence, but also relative to their proportion in the City of Urbana and Champaign County populations. Responses in the 20-39 age cohort are relatively consistent with their representation in the Area of Influence and wider community; the signficant underrepresentation is taking place in the under-20 cohort. While it is not suprising that youths and younger undergraduate students had a lower response rate to a local government survey than older and more established residents, this nonetheless encourages further outreach to younger community members in the future, including through reaching out to students on campus.

<rpc-chart
url="survey_age.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Survey; U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B01001"
chart-title="Age Distribution"></rpc-chart>

## Race and Ethnicity

Non-Hispanic white residents comprised the vast majority of respondents, with 77% of responses from this group. The next most prevalent were non-Hispanic Asian respondents, at 8%, and Hispanic or Latino respondents of any race, at 6%. Compared to local ACS data, we can see that non-Hispanic white residents are overrepresented, as they make up 60% of Area of Influence residents, 55% of Urbana residents, and 66% of county residents. The undercounting of student populations, as well as the demographics of the West Urbana neighborhood which is adjacent to the corridor and where many responses originated from, are likely contributing factors to this level of representation, and should be considered in future public outreach.

<rpc-chart
url="survey_race.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Survey; U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B03002"
chart-title="Race and Ethnicity"></rpc-chart>

## Gender

Slightly fewer than half of the respondents identified as women (49%), while 43% identified as men, and the remaining 8% included respondents who selected "Prefer Not to Answer", respondents who were Non-binary or Non-Conforming, respondents who utilized the "Other" fill-in-the-blank option, or respondents who selected multiple choices.


<rpc-chart
url="survey_gender.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Gender of Survey Respondents"></rpc-chart>

## Employment

When looking at overlapping employment categories and allowing respondents to select multiple choices, 61% indicated that they work outside the home, 32% are students, 14% are retired, 11% work in their home, and less than 5% each are looking for work, homemakers, or Other.

<rpc-chart
url="survey_work_2.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Employment Categories of Survey Respondents (Non-Exclusive)"></rpc-chart>


