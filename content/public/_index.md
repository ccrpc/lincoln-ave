---
title: Public Involvement
draft: false
menu: main
weight: 40
---

Your input is needed to identify the challenges and opportunities people experience while traveling on Lincoln Avenue. Your input will inform the future vision for Lincoln Avenue and help increase mobility in the Champaign-Urbana area.

Throughout the planning process, this section will be the one-stop-shop for project information and public involvement opportunities.
