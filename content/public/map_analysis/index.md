---
title: Phase One - Map Comment Analysis
draft: false
weight: 35
---

The first round of public outreach for the Lincoln Avenue Corridor Study resulted in a total of ***317 map comments***. This includes all map comments made by the public using the online interactive map, as well as those provided on the physical map at the public meeting in October. 
Responses were identified as fitting into these broad categories by topic, depending on the contents of each comment:

1.	**Roadway Function/Design**
2.	**Multimodal Facilities**
3.	**Visibility**
4.	**Signals and Signs**
5.	**Pavement Maintenance**
6.	**Other**

# Map Comments 

<iframe
  src="https://ccrpc.gitlab.io/lincoln-avenue-voices/"
  style="width:100%; height:600px;"
></iframe>


The following charts display the number of times that each of the topics were mentioned within the comments pertaining to each question. *(Comments mentioned multiple topics, so the percentages presented do not equal 100%)*

<rpc-table url="Map_All_CountandPercent.csv"
  table-title="Top Issues by Topic"
  source="Lincoln Avenue Corridor Study Map Comments"
  text-alignment="l,r"></rpc-table>


Because Lincoln Avenue is the focus of this study, all intersections will be referred to solely by their cross street  for the remainder of this analysis (for example, the intersection of Lincoln Avenue and Pennsylvania Avenue will just be referred to as "at Pennsylvania Avenue"). In any circumstance where the described location is not on Lincoln Avenue, both streets will be specified. Similarly, all segments of Lincoln Avenue will be referred to solely by their start and end point (eg., the section of Lincoln Avenue between Green Street and Illinois Street is referred to as Green - Illinois).

## Multimodal Facilities

The most prevalent multimodal facilities-related comments pertain to adding pedestrian crossings along the corridor (48) and including on-street bicycle facilities (23). The intersections of at Oregon Street (East Leg- 6; West Leg- 3), California Avenue (4), and Vermont Avenue (4) were some of the most mentioned locations for additional pedestrian crossings. Some comments called for raised crosswalks at various locations, including Oregon Street, Iowa Street, Ohio Street, and Michigan Avenue. Others called for an all-way crosswalk at various intersections, including Illinois Street, Nevada Street, and Florida Avenue.

Other subjects included increasing crosswalk distance (8), widening sidewalks from Oregon Street to Nevada Street and from Michigan Avenue to Florida Avenue (8), and adding sidewalks to the west side of the corridor (specifically south of Florida Avenue) (6). Remaining comments involve adding a bus shelter at Pennsylvania Avenue (1) and increasing the frequency of bus service along the corridor (1).


## Other

Because the interactive map allowed the public to input their feedback directly on their own, users were able to classify comments using their own discretion. Due to this selection process, many users chose to input their comments as “Other,” though some would have fit better in another category. However, comments remained in the “Other” category for the purpose of this analysis.  The list provided below describes the topics mentioned in comments within these categories:

<image src="Other_Comments.PNG"
  link="/destination"
  alt="A chart showing the categories that comments in the other category fell into."
  attr="CUUATS" 
  position="right">


<br>


## Signals and Signs

Many of the comments in this section involved adding pedestrian flashing signals at crossings (21). The intersections of Lincoln Avenue with Iowa Street, Ohio Street, and Indiana Avenue were some of the most mentioned locations for pedestrian flashing signals at crossings. 

Other comments pertained to bicycle signals at the following Lincoln Avenue intersections: Green Street, Nevada Street, Iowa Street, and Pennsylvania Avenue (12). Some comments mentioned improving the signal timing at existing traffic signals to improve traffic flow (7). Other comments suggested adding traffic signals at Iowa Street, Ohio Street, Indiana Avenue, and Michigan Avenue to improve pedestrian crossings (6). Respondents also suggested improving pedestrian crossing time at Illinois Street, Nevada Street (West Leg), Pennsylvania Avenue, and Florida Avenue by allowing more time to cross and reducing conflict with turn arrows (6).

Remaining comments along the corridor involved requests to remove pedestrian beg buttons at the intersections at Green Street, Illinois Street, and Florida Avenue (5), improve wayfinding signage along the corridor for both automobiles and pedestrians/cyclists (5), and add stop signs at Ohio Street, McKinley Health Center, and Indiana Avenue (3).

## Roadway Function/Design
Comments in this category pertained to a variety of roadway function and design elements (26). Specifically, comments mentioned that merging at Florida Avenue is difficult, confusing, and unsafe. Other comments mentioned the desire to reduce the corridor to one-lane in each direction or keep it as two lanes, while some suggested continuing two lanes in each direction from Nevada to Florida. Comments mentioned adding a roundabout at Green Street, Vermont Avenue, Pennsylvania Avenue, or Florida Avenue (7). 

Many respondents expressed a desire to reduce the speed limit along the corridor (8). Other comments involved increasing separation between automobiles and pedestrians or cyclists, specifically from California Avenue to Oregon Street, Nevada Street to Ohio Street, and Pennsylvania Avenue to Florida Avenue (6). Additional comments include reducing congestion (3) and parking (2) along the corridor.

## Visibility
Comments relating to visibility include both automobile (6) and pedestrian (6) issues. Comments mentioning automobile visibility specified issues turning onto Lincoln Avenue from side streets, such as Oregon Street or California Avenue, as well as from eastbound Pennsylvania Avenue. Pedestrian visibility comments focused on the lack of lighting along the corridor allowing pedestrians to safely navigate facilities. Other comments involved improving lighting at the bus amenities from Iowa Street to Ohio Street (1).

## Pavement Maintenance
Many comments mentioned the need to repave the roadway, specifically from Green Street to Illinois Street and Indiana Avenue to Michigan Avenue, as well as the intersections at Oregon Street (West Leg) and Iowa Street (5). Other comments involved improving pavement markings by adding stop lines at the crosswalk at Ohio Street, as well as improving the markings for the lane merges at Nevada Street and Delaware Avenue (3). Additional comments pertained to improving the bicycle crossing at Iowa Street (1) and the sidewalk pavement at McKinley Health Center (1).

## Comments by Intersection
This section details the comments pertaining to each intersection of the corridor study area. To view the comments, click on each intersection name to open the drop down list.


{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="Green Street (27 comments)"%}}

* **Automobile (9)**
    * Reduce the speed limit (2)
    * Increased traffic enforcement (1)
    * Add roundabout (1)
    * Push traffic to surrounding streets (1)
    * Signal retiming (1)
    * Increase wayfinding signage for automobiles into Downtown Urbana (1)
    * No turn on red (1)
    * Add stop bars along corridor (1) 
* **Bicycle (12)**
    * Add bike signal (5)
    * Add protected bike lanes (3)
    * Add protected intersection/Dutch junction (3)
    * Improve safety (1)
* **Pedestrian (6)**
    * Add all-cross here (3)
    * Reduce pedestrian wait time (1)
    * Improve safety (1)
    * Remove beg buttons (1)
  {{%/accordion-content%}}
  {{%accordion-content title="Illinois Street (18 comments)"%}}

* **Automobile (7)**
    *	No turn on red (2)
    *	Traffic blocks intersection (1)
    *	Signal retiming (1)
    *	Increase wayfinding signage for automobiles into Downtown Urbana (1)
    *	Add roundabout (1) 
    *	Add left arrow turn signal (1)
* **Bicycle (2)**
    *	Space narrows for bicycle traffic (1)
    *	Increase bicycle signage (1)
* **Bus (1)**
    *	Prioritize buses (1)
* **Pedestrian (8)**
    *	Increase crossing time (2)
    *	Improve lighting (1)
    *	Increase connectivity (1)
    *	Remove beg buttons (1)
    *	Add all-way crosswalk (1)
    *	Change to ladder crosswalk design (1)
    *	Curb is dangerous for pedestrians (1)
 {{%/accordion-content%}}
 {{%accordion-content title="California Avenue (7 comments)"%}}

*  **Automobile (2)**
    * Improve visibility (1)
    * Roadway reconfiguration (1)
* **Pedestrian (5)**
    * Add crosswalk (5)
        * *Raised crosswalk (1); Flashing signal (1)*
 {{%/accordion-content%}}
 {{%accordion-content title="Oregon Street (West Leg) (11 comments)"%}}

*   **Automobile (6)**
    *   Roadway reconfiguration (3)
    * Repave roadway (1)
    * Add traffic light (1)
    * Improve traffic flow (1)
* **Pedestrian (5)**
    * Add crosswalk (3)
        * *Raised crosswalk (1); Flashing signal (1)*
    * Increase auto separation (1)
    * Add pedestrian signal (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Oregon Street (East Leg) (9 comments)"%}}

* **Automobile (1)**
    * Improve visibility (1)
* **Bicycle (1)**
    * Improve connectivity (1)
* **Pedestrian (7)**
    * Add crosswalk (7)
        * *Safe crossing with median (5); Pedestrian signal (1)*
 {{%/accordion-content%}}
 {{%accordion-content title="Nevada Street (East Leg) (7 comments)"%}}

* **Automobile (3)**
    *  Roadway reconfiguration (2)
    * Improve visibility (1)
* **Pedestrian (4)**
    * Widen sidewalk (2)
    * Add crosswalk (1)
    * Increase auto separation (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Nevada Street (West Leg) (20 comments)"%}}

* **Automobile (4)**
    * Signal retiming (2)
    * Reduce speed limit (1)
    * Improve visibility (1)
        * *Apartment lights are blinding* 
* **Bicycle (6)**
    * Add protected bike lanes (3)
    * Add signal (2)
    * Prioritize bicycle and pedestrian movement (1)
* **Bus (2)**
    * Add bus lane (1)
    * Parking along corridor is an obstacle (1)
* **Pedestrian (8)**
    * Improve crossing time and signaling (4)
        * *Cars turn when walk signal is on (2)*
    * Add crosswalk (1)
        * *All-way crosswalk*
    * Reduce speed limit (1) 
    * Remove beg buttons (1)
    * Push buttons work (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Iowa Street (21 comments)"%}}

* **Automobile (1)**
    * Repave roadway
* **Bicycle (4)**
    * Add on-street bicycle facilities (1)
    * Improve connectivity (1)
        * *Align existing bike path with access to east of Lincoln*
    * Add signal (2)
* **Pedestrian (16)**
    * Add crosswalk (6)
        * *Raised crosswalk (3)*
    * Reduce crosswalk distance (1)
    * Improve lighting (1)
    * Widen sidewalk (1)
    * Reduce wait time (1)
    * Add pedestrian signal (4)
    * Add flashing light signal 
    * Add traffic signal (2)
 {{%/accordion-content%}}
 {{%accordion-content title="Ohio Street (20 comments)"%}}

* **Automobile (4)**
    * Roadway reconfiguration (2) 
    * Improve pavement markings (1)
        * *Add stop bars*
    * Improve wayfinding signage (1)
* **Bicycle (1)**
    * Add on-street bike facilities (1)
        * *Run bike lanes behind bus stop (like Illini Union)*
* **Pedestrian (15)**
    * Add crosswalk (4)
        * *Raised crosswalk (3)*
    * Reduce crosswalk distance (1) 
    * Improve lighting (1)
    *	Increase auto separation (1) 
    *	Add pedestrian signal (5)
        * *Add flashing light signal*
    *	Add stop sign (1)
    *	Add traffic signal (1)
    *	Add pedestrian refuge (1)
 {{%/accordion-content%}}
 {{%accordion-content title="McKinley Health Center (5 comments)*"%}}

*	**Pedestrian (5)**
    *	Fix crosswalk pavement (1)
    *	Add crosswalk (3)
        *  *Connect to existing sidewalk at McKinley*
    *	Add stop sign (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Indiana Avenue (13 comments)"%}}

*	**Automobile (1)**
    *	Roadway reconfiguration
*	**Bicycle (1)**
    *	Add bicycle facilities (on/off street) that connect to the university 
*	**Pedestrian (11)**
    *	Add lighting (2)
        * *Difficult to see at night*
    *	Add crosswalk (1)
        * *Raised crosswalk*
    *	Add signal (6)
        * *Add flashing light signal (5)*
    *	Add stop sign (1)
    *	Add traffic signal (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Michigan Avenue (10 comments)"%}}

*	**Pedestrian (10)**
    *	Add crosswalk (4)
        * *Raised crosswalk (3)*
    *	Add signal (4)
        * *Add flashing light signal*
    *	Add traffic signal (2)
 {{%/accordion-content%}}
 {{%accordion-content title="Pennsylvania Avenue (22 comments)"%}}

*	**Automobile (6)**
    *	Add roundabout (2)
    *	Improve visibility (2)
    *	Signal retiming (1) 
    *	Add signage (1)
        * *“No left turn arrow”*
*	**Bicycle (6)**
    *	Add on-street bicycle facilities (1)
    *	Add signal (2)
    *	Move existing beg button (1)
    *	Increase auto separation (1)
    *	Add roundabout (1)
*	**Bus (2)**
    *	Add bus shelter (1)
    *	Add left turn arrow eastbound to Lincoln Avenue (1)
*	**Pedestrian (8)**
    *	Add crosswalk (1)
    *	Increase auto separation (2)
    *	Reduce crosswalk distance (1)
    *	Improve safety (1)
    *	Remove beg button (1)
    *	Increase crossing time (1)
    *	Check signal timing (1)
        *   *Audible signals on North Pennsylvania do not change with light*
 {{%/accordion-content%}}
 {{%accordion-content title="Vermont Avenue (6 comments)"%}}

*	**Automobile (1)**
    *	Add roundabout (1)
*	**Pedestrian (5)**
    *	Add crosswalk (4)
        * *Add crosswalk connecting to PAR (1)*
        * *Raised crosswalk (2)*
    *	Add flashing signal (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Delaware Avenue (8 comments)"%}}

*	**Automobile (2)**
    *	Roadway reconfiguration (2)
*	**Bicycle (1)**
    *	Improve connectivity
        * *Add crossing to College Court*
*	**Pedestrian (5)**
    *	Add crosswalk (2)
    *	Reduce crosswalk distance (2)
    *	Add signal (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Florida Avenue (30 comments)"%}}

*	**Automobile (10)**
    *	Roadway reconfiguration (6)
        * *Merging is confusing and difficult*
    *	Add roundabout (3)
    *	Improve signaling (1)
        * *Turning right is dangerous when pedestrians cross*
*	**Bicycle (7)**
    *	Add bicycle facilities (3)
        * *Add on-street (2)*
        * *Add off-street (1)*
    *	Improve safety (1)
    *	Improve connectivity (2)
        * *Safely connect to facilities south of Florida*
    *	Add wayfinding (1)
*   **Bus (1)**
*	**Pedestrian (12)**
    *	Add all-way crossing (1)
    *	Reduce crosswalk distance (2)
    *	Improve safety (2)
    *	Increase crossing time (2)
    *	Add sidewalk (2)
    *	Widen sidewalk (1)
    *	Remove beg buttons (1)
    *	Roadway reconfiguration (6)
  {{%/accordion-content%}}
{{</accordion>}}

<br>

*The intersection of Lincoln Avenue and the McKinley Health Center was incorporated into this analysis due to a number of comments citing that location specifically.

<br>

<rpc-chart
url="Intersection_Ped.csv"
type="verticalBar"
colors="#9ACD32"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="18"
legend-position="top"
legend="false"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Pedestrian Map Comments by Intersection"></rpc-chart>

<rpc-chart
url="Intersection_Auto.csv"
type="verticalBar"
colors="orange"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="12"
legend-position="top"
legend="false"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Automobile Map Comments by Intersection"></rpc-chart>

<rpc-chart
url="Intersection_Bike.csv"
type="verticalBar"
colors="#FFD700"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="14"
legend-position="top"
legend="false"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Bicycle Map Comments by Intersection"></rpc-chart>

<rpc-chart
url="Intersection_Bus.csv"
type="verticalBar"
colors="#4169E1"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="3"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Bus Map Comments by Intersection"></rpc-chart>


<br>

## Comments by Segment
This section details the comments pertaining to each segment of Lincoln Avenue in the corridor study area. To view the comments, click on each segment name to open the drop down list.

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="Springfield – Green (6 comments)"%}}
 
*	**Automobile (4)**
    *	Roadway reconfiguration (2)
        * *“No left hand turns, all right turns” (1)*
        * *Divert traffic to campus (1)*
    *	Reduce speed limit (2)
*	**Pedestrian (2)**
    *	Add crosswalk (1)
        * *Raised crosswalk*
    *	Add sidewalk (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Green – Illinois (7 comments)"%}}

*	**Automobile (4)**
    *	Roadway reconfiguration (2) 
        * *Continue two lane roadway in each direction to Florida (1)*
        * *Extend sidewalk to slow drivers (1)*
    *	Repave roadway (1)
    *	Reduce speed limit (1)
*	**Bicycle (3)**
    *	Add bicycle facilities (2)
        * *Add on-street*
    *	Increase auto separation (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Illinois – California (1 comment)"%}}

*	**Automobile (1)**
    *	Roadway reconfiguration
        * *Continue two lane roadway*
 {{%/accordion-content%}}
 {{%accordion-content title="California – Oregon (4 comments)"%}}

*	**Automobile (1)**
    *	Roadway reconfiguration
        * *Decrease number of lanes*
*	**Pedestrian (3)**
    *	Add crosswalk (2)
    *	Increase auto separation (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Oregon – Oregon (2 comments)"%}}

*	**Bicycle (1)**
    *	Add bicycle facilities (2)
        * *Add on-street*	
* **Pedestrian (1)**
    *	Add crosswalk (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Oregon – Nevada (3 comments)"%}}

*	**Automobile (2)**
    *	Reduce speed limit (1)
    *	Improve wayfinding signage (1)
*	**Pedestrian (1)**
    *	Widen sidewalk (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Nevada – Nevada (3 comments)"%}}

*	**Bicycle (2)**
    *	Add bicycle facilities (1)
        * *Add on-street*
    *	Safety (1)
        * *Cycling here does not feel safe*
*	**Bus (1)**
    *	Visibility (1)
        * *Difficult to see bus stop around stopped cars*
 {{%/accordion-content%}}
 {{%accordion-content title="Nevada – Iowa (11 comments)"%}}

*	**Automobile (7)**
    *	Roadway reconfiguration (3)
        * *There is too much going on at this intersection. Confusing and hazardous (1)*
        * *Merge here causes traffic issues (1)*
        * *Reconfigure lanes to eliminate delivery parking space (1)*
    *	Improve visibility (1)
        * *Difficult to turn onto Lincoln and see drivers crossing Nevada/Lincoln*
    *	Reduce parking (3)
        * *Remove parking in front of Jimmy Johns; impairs visibility*
*	**Bicycle (1)**
    *	Bicycle facilities (1)
        * *Lack of facilities and confusing to navigate*
*	**Pedestrian (3)**
    *	Add median (2)
    *	Widen sidewalk (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Iowa – Ohio (13 comments)"%}}

*	**Automobile (2)**
    *	Roadway reconfiguration (2)
        * *Reconfigure lanes*
*	**Bicycle (2)**
    *	Add bicycle facilities (1)
        * *Add on-street*
*	**Bus (3)**
    *	Add lighting (1)
    *	Increase bus route frequency (2)
*	**Pedestrian (6)**
    *	Add crosswalk (4)
        * *Add midblock crossing at LAR*
    *	Add lighting (1)
        * *Feels unsafe walking at night*
    *	Add flashing signal (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Ohio – McKinley* (5 comments)"%}}

*	**Automobile (1)**
    *	Roadway reconfiguration (1)
        * *Raise road level with sidewalk*
*	**Bicycle (1)**
    *	Add bicycle facilities (1)
        * *Add off-street path to separate from auto traffic*
*	**Bus (1)**
    *	Reduce conflict with buses (1)
        * *Run bike lanes behind bus stops like on Green Street*
*	**Pedestrian (2)**
    *	Add crosswalk (1)
    *	Reduce speed limit (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Indiana – Michigan (9 comments)"%}}

*	**Automobile (5)**
    *	Reduce congestion (1)
        * *“Southbound rush hour traffic backs up to here”*
    *	Roadway reconfiguration (1)
        * *Narrow the roadway*
    *	Repave roadway (2)
    *	Reduce speed limit (1)
*	**Bicycle (2)**
    *	Add bicycle facilities (2)
        * *Add protected on-street, multi-mode lanes (1)*
*	**Pedestrian (2)**
    *	Add crosswalk (1)
        * *Raised crosswalk*
    *	Add flashing signal (1)
 {{%/accordion-content%}}
 {{%accordion-content title="Michigan – Pennsylvania (4 comments)"%}}

*	**Automobile (1)**
    *	Roadway reconfiguration (1)
        * *Slow traffic with roadway design*
*	**Bicycle (1)**
    *	Add bicycle facilities (2)
        * *Add protected on-street*
*	**Pedestrian (2)**
    *	Increase connectivity (1)
        * *Better connection between West Urbana and Illini Grove*
    *	Widen sidewalk (1)
        * *Sidewalk is narrow and close to the street*
 {{%/accordion-content%}}
 {{%accordion-content title="Pennsylvania – Vermont (1 comment)"%}}

*	**Automobile (1)**
    *	Roadway configuration (1)
        * *Reduce to two lane roadway*
 {{%/accordion-content%}}
 {{%accordion-content title="Vermont – Delaware (1 comment)"%}}

*  **Automobile (1)**
    *	Roadway configuration (1)
        * *Convert northbound left lane into a turn only lane (onto Pennsylvania)*

 {{%/accordion-content%}}
 {{%accordion-content title="Delaware – Florida (5 comments)"%}}

*	**Automobile (2)**
    *	Roadway configuration (2)
        * *Reduce to two lane roadway (1)*
        * *Lane merge occurs too suddenly (1)*
*	**Bicycle (1)**
    *	Increase auto separation (1)
*	**Pedestrian (2)**
    *	Increase auto separation (1)
    *	Widen sidewalk (1)
        * *Make wide enough for shared use path*
 {{%/accordion-content%}}
 {{%accordion-content title="Florida - Windsor (3 comments)"%}}

*	**Pedestrian (3)**
    *	Add crosswalk (1)
    *	Add sidewalk (2)
        * *Add sidewalk south of Florida to Arboretum/Idea Garden*
  {{%/accordion-content%}}
{{</accordion>}}

<rpc-chart
url="Segment_Ped.csv"
type="verticalBar"
colors="#9ACD32"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="7"
legend-position="top"
legend="false"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Pedestrian Map Comments by Segment"></rpc-chart>

<rpc-chart
url="Segment_Auto.csv"
type="verticalBar"
colors="orange"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="8"
legend-position="top"
legend="false"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Automobile Map Comments by Segment"></rpc-chart>

<rpc-chart
url="Segment_Bike.csv"
type="verticalBar"
colors="#FFD700"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="4"
legend-position="top"
legend="false"
tooltip-intersect="true"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Bicycle Map Comments by Segment"></rpc-chart>

<rpc-chart
url="Segment_Bus.csv"
type="verticalBar"
colors="#4169E1"
stacked="false"
y-label="Response Count"
y-min="0"
y-max="4"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Map Comments"
chart-title="Bus Map Comments by Segment"></rpc-chart>

<br>

*The Ohio to Indiana segment of Lincoln Avenue was separated at the McKinley Health Center for this analysis due to a number of comments citing that location specifically.

## LRTP - Comments Relevant to the Corridor

The Long Range Transportation Plan, or LRTP, is a federally required document in which CUUATS plans for transportation in our region for the next 25 years. 

The [2050 LRTP update](https://ccrpc.gitlab.io/lrtp-2050-demo/) conducted its first phase of public outreach from the spring through the fall of 2023. One component of this outreach was a similar public webmap to the one used in the Lincoln Avenue outreach described above. In contrast to the Lincoln Avenue map, this LRTP map provided the opportunity to leave comments across the entire metropolitan planning area. Of these region-wide comments, 13 were located in the Lincoln Avenue Corridor Study area. The map below shows this feedback. Map points that included text comments show that text in the map, while map points where the user did not input  text just show the category of suggestion that the user selected, for example, "bicycle.facility.offstreet." 

This data is not included in the counts of feedback from the Lincoln Avenue Corridor Study discussed above, but it nevertheless reinforces many of the themes brought up in the study's public outreach, including concerns about vehicle speed, bike and pedestrian infrastructure, and the safe interaction between various modes.    

<image src="LRTP Comments.png"
  alt="A map showing public comments related to transportation concerns on the Lincoln Avenue Corridor"
  attr="CUUATS" 
  position="full">