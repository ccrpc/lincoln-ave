---
title: Phase Two - Map Comment Analysis
draft: false
weight: 70
---

The second round of public engagement for the Lincoln Avenue Corridor Study resulted in ***95 pieces of map feedback*** (including both comments provided on the [online map](https://ccrpc.gitlab.io/lincoln-avenue-scenario-phase/) and physical comments from the open house, which were digitized by staff). As seen in the analysis below, map feedback was largely in line with the themes from the survey responses; however, the maps do provide more insight on specific locations of feedback or concern.


<iframe
  src="https://ccrpc.gitlab.io/lincoln-avenue-scenario-phase/"
  style="width:100%; height:600px;"
></iframe>

## Scenario Analysis

<rpc-chart
url="Scenario_Breakdown.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label=""
x-min="0"
x-max="70"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Relevant Scenario of Proposal"></rpc-chart>

Of the 95 comments, the vast majority (60) applied to proposed changes that were consistent across scenarios (i.e. proposals from Question 1 of [the survey](https://ccrpc.gitlab.io/lincoln-ave/public/phase-two-survey/)), while 35 combined comments applied to specific, scenario-related changes (as seen in [Question 4](https://ccrpc.gitlab.io/lincoln-ave/public/phase-two-survey/#question-4---which-of-the-proposed-bicycle-and-pedestrian-improvements-do-you-prefer)). 

### Consistent Proposals

<rpc-chart
url="Consistent_Changes.csv"
type="horizontalBar"
colors="#4169E1,#008000,#A9A9A9"
stacked="false"
x-label=""
x-min="0"
x-max="18"
legend-position="top"
legend="true"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Consistent Change Comments"></rpc-chart>

Looking at the consistently-proposed changes, the highest number of comments regarded the proposal to relocate or consolidate crosswalks. Feedback to to this proposal included 10 suggestions to implement raised crosswalks, six requests to shorten crossing distance (either through pedestrian islands or bump outs), as well as various other concerns, including worries about jaywalking if crossings are consolidated.

Comments affirming the proposal to implement rapid rectangular flashing beacons (RRFBs) were the next most common. A variety of perspectives were offered on the proposal to close side streets, with most of the modifications being requests to allow bike and ped access on closed streets, several respondents supporting the measure, as well as several raising concerns about how vehicle traffic will be redirected. The seven modification comments regarding signal timings consisted primarily of requests for leading pedestrian intervals and no right turn on red, along with comments about cycling at signalized intersections (asking how signals will accommodate off-street cyclists, and encouraging signal infrastructure for cyclists).

In comparison to the Question 1 results from the survey, it is useful to note that opposition was not the strongest sentiment for any proposed change on the map, where respondents were given the opportunity to classify their comment as "Implement," "Implement Modified," or "Do Not Implement." This suggests that much of the opposition seen in the survey may be tied to specific concerns, which can be addressed in the final version of the corridor study.

### Scenario One

<rpc-chart
url="Scenario_1.csv"
type="horizontalBar"
colors="#4169E1,#008000,#A9A9A9"
stacked="false"
x-label="On-Street Bike Lanes"
x-min="0"
x-max="9"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Scenario One"></rpc-chart>



The 19 comments regarding Scenario 1 (Continuous on-street bike lanes in both directions from Green to Florida) were all focused with the on-street bike lanes. Across the three response types, comments primarily were concerned with physical separation—ensuring that cyclists had adequate separation from vehicle traffic, whether through more space or through physical barriers.


### Scenario Two

<rpc-chart
url="Scenario_2.csv"
type="horizontalBar"
colors="#4169E1,#008000,#A9A9A9"
stacked="false"
x-label="Shared-Use Path"
x-min="0"
x-max="9"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Scenario Two- Total"></rpc-chart>

The five comments regarding Scenario 2 (On-street bike lanes in both directions between Green and Iowa; Shared-use path on west side of street from Iowa to Pennsylvania; On-street bike lane in both directions between Pennsylvania and Florida) were primarily concerned with the transition points, where cyclists would move between bike lanes and shared use paths. These comments raised questions about safety moving between infrastructure types or across intersections.

### Scenario Three

<rpc-chart
url="Scenario_3.csv"
type="horizontalBar"
colors="#4169E1,#008000,#A9A9A9"
stacked="false"
x-label=""
x-min="0"
x-max="9"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Scenario Three- Total"></rpc-chart>




Like the Scenario 2 comments, the 11 comments regarding Scenario 3 (On-street bike lanes in both directions between Green and Iowa; Shared-use path on west side of street from Iowa to Florida) were primarily concerned with the safety and clarity of transitions between different types of cycling infrastructure. Most of the modification comments were again requesting vehicle protection on the proposed bike lanes. Support for the shared use path came from respondents who viewed it as an improvement over the existing sidewalk.


## Location Analysis
The following analysis covers the same 95 data points discussed above, but focuses on the geographic location of the data points. All comments can be seen on the online map tool [here](https://ccrpc.gitlab.io/lincoln-avenue-scenario-phase/).


### Intersections

<rpc-chart
url="Intersections.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label=""
x-min="0"
x-max="16"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Total Intersection Comments"></rpc-chart>



As seen in the graph above, the 68 intersection-related comments were most commonly located at Iowa Street (15 comments) and Pennsylvania Avenue (12 comments), with a handful of comments each at the other intersections.  Iowa Street comments were made up of five pro-RRFB comments, four comments regarding on-street bike lanes (two in support, one opposed, and one with a modification question about the share-used use path transition here), three comments on street closures (two modification comments about maintaining bike and pedestrian access, one rejection concerned about vehicle rerouting), and a handful of comments on other subjects. 

Pennsylvania Avenue feedback consisted of five comments on adjusting signal timing (suggestions for no right turns on red, leading pedestrian intervals, and updating the signal infrastructure to accommodate bikes, along with a question about shared-use path signal phasing and a comment about how vehicles currently disregard pedestrians here). This intersection also featured three shared-use path comments (opposing the path or asking questions due to concerns about the transition between path and bike lanes at Pennsylvania) and a handful of comments on other subjects.


### Segments

<rpc-chart
url="Segments.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label=""
x-min="0"
x-max="9"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="LACS Round 2 Map Comments, 2024"
chart-title="Total Segment Comments"></rpc-chart>


The 27 segment-related map comments were most commonly located on the  Michigan Avenue to Pennsylvania Avenue segment (8 comments) and the Ohio Street to Indiana Avenue segment (7 comments). Michigan-Pennsylvania comments were primarily focused on on-street bike infrastructure, with two pro-bike lane comments (with elaboration that the current narrow sidewalk in this section provides an obstacle to cyclists biking off-street), two rejections (due to desire to further separate bikes from traffic) and a modification (a proposal for a two-way bike path, either on or off street). Ohio-Indiana comments centered on the proposal to relocate or consolidate crosswalks, with three rejections expressing a desire not to consolidate and relocate crosswalks here, under the belief that this would encourage jaywalking, while two other comments supported raised or narrowed pedestrian crossings here.