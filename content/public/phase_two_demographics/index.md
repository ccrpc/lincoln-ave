---
title: Phase Two - Respondent Demographics
draft: false
weight: 80
---

## Introduction
As discussed in the [Phase Two - Summary](https://ccrpc.gitlab.io/lincoln-ave/public/phase_two/) and [Phase Two - Survey Comment Analysis](https://ccrpc.gitlab.io/lincoln-ave/public/phase-two-survey/) pages, one component of the second phase of outreach for the Lincoln Avenue Corridor Study was an online survey. In addition to questions asking for feedback on proposed changes to the corridor, this survey included questions about the respondents themeselves, including their travel habits and demographic information. This data allows for the study team to have a fuller picture of the degree to which feedback is representative of the larger community and users of the corridor. The results of this analysis are found below.

## Respondent Transportation

***Question 6 - How often do you use each of the following transportation modes on Lincoln Avenue betweeen Green Street and Florida Avenue?***

The responses to Question 6, about mode use on the Lincoln Avenue Corridor, indicate that private motor vehicles were the most common transportation mode on Lincoln Avenue for survey respondents, with 64% of respondents traveling the corridor by motor vehicle weekly, and 90% travelling Lincoln Avenue by private vehicle at least annually. This mode was very closely followed by walking (60% weekly, 89% annually), while biking (29% weekly, 57% annually), riding the bus (21% weekly, 61% annually), ride share (8% weekly, 31% annually), and wheelchair or motorized chair (2% weekly, 4% annually) encompassed the remaining travel on the corridor.

<rpc-chart
url="Q6-Chart.csv"
type="horizontalBar"
stacked="true"
x-label="Percentage of total respondents"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source="LACS Round 2 Survey, 2024"
chart-title=" How often do you use each of the following transportation modes on Lincoln Avenue?"></rpc-chart>

<br>

***Question 7 - Do you own or have reliable access to any of the following transportation modes?***

<rpc-chart
url="Q7-Mode_Access.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Percentage of total respondents"
x-min="0"
x-max="100"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Do you own or have reliable access to any of the following transportation modes?"></rpc-chart>

In a similar trend to Question 6, the 190 responses to Question 7 responses indicated that 80% of respondents report have access to a motor vehicle, 64% to a bicycle, and 66% to a bus pass. Respondents that utilize Lincoln Avenue cite having the most access to personal motor vehicles, followed by active and public transportation options. 

Looking at combinations of mode access (below), the highest proportion of respondents (35%) had access to all three modes, followed by bike and motor vehicle (21%), and and tight grouping of  bus pass only, bus pass and motor vehicle, and motor vehicle only (all ~12%). 

<rpc-chart
url="Q7-Chart.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Percentage of total respondents"
x-min="0"
x-max="100"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Do you own or have reliable access to any of the following transportation modes?"></rpc-chart>


## Area of Residence
Of the 193 survey responses, 160 provided the nearest intersection to their home, as seen in the table and maps below.

<rpc-table url="Location_chart.csv"
  table-title="Survey Respondent Residence Location"
  table-subtitle="Reported by Nearest Intersection"
  source="Lincoln Avenue Corridor Study Survey - Round 2"
  text-alignment="l,r"></rpc-table>

{{<image src="Response_Intersections_Map_ALL.png"
  alt="Map of all survey respondent residences, reported by the closest intersection"
  position="right">}}




This shows that responses came from across the Champaign-Urbana area, with respondents concentrated within and around the Study Area. 

Nearly half of the respondents reported living within the Study Area, and nearly 70% within the Area of Influence. 

Overall, about 83% of respondents reside in the City of Urbana. These results show that most people who responded to the survey were those residing within close proximity to the corridor and most affected by the project.

{{<image src="aoi_intersections.png"
  alt="Map of survey respondent residences, reported by the closest intersection, zoomed in to focus on the area of influence"
  position="">}}

*The numbers within each symbol in the map represent how many respondents listed that particular intersection as  nearest to their home. If there is no number on the symbol, then there is just one response at that location.* 

## Age

***Question 10 - What is your age range?***

Of the 188 respondents to Question 10, approximately 61% were younger than 40, and 39% were 40 or older. As discussed in the [demographic analysis](https://ccrpc.gitlab.io/lincoln-ave/public/phase_one_demo/#age) for the first round of public engagement, this indicates that respondents 40 or over are still significantly overrepresented relative to the Area of Influence and City of Urbana, although approaching the proportion for Champaign County overall. The starkest difference in representation in seen in the under-20 cohort. However, representation in this cohort was nearly doubled, compared to the first round of public feedback. This indicates that, although the previously-discussed difficulties in reaching younger audiences remain, student-focused outreach was successful in increasing the proportion of respondents under 20.

<rpc-chart
url="Age_Chart_REVISED.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
legend-position="top"
legend="true"
tooltip-intersect="false"
source="LACS Survey Round 2, 2024; U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B01001"
chart-title="Age of Survey Respondents"></rpc-chart>


## Gender

### Question 11 - Which best describes your gender identity?
Of the 187 respondents to Question 11, slightly fewer than half of the respondents (48%) identified solely as women , while 41% identified as solely as men. Five percent of respondents selected solely Non-binary/Non-conforming to describe themselves, while 1% selected multiple options or provided their own response, leaving 5% who selected "I prefer not to answer." 

<rpc-chart
url="Gender_Chart.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Percentage of total respondents"
x-min="0"
x-max="50"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Gender of Survey Respondents"></rpc-chart>


## Race and Ethnicity

### Question 12 - Which race/ethnic group(s) do you most identify with?
Of the 176 respondents to Question 12, non-Hispanic white residents comprised the vast majority, with 74% of responses from this group. The next most prevalent were non-Hispanic Asian respondents, at 8%, and Hispanic or Latino respondents of any race, at 7%, followed by non-Hispanic African American or Black and Two or More Races, both around 5%. Compared to local ACS data, we can see that non-Hispanic white residents are overrepresented, as they make up 60% of Area of Influence residents, 55% of Urbana residents, and 66% of county residents. 

As discussed in the [first round of public feedback](https://ccrpc.gitlab.io/lincoln-ave/public/phase_one_demo/#race-and-ethnicity), the undercounting of student populations, as well as the demographics of the West Urbana neighborhood which is adjacent to the corridor and where many responses originated from, are likely contributing factors to this level of representation. However, similarly to the Age Demographics, the second round of feedback was able to reduce some of the disproportionate representation seen in the first (even if not to the same degree as the age question), with non-Hispanic white respondents shifting from 77% to 74% of respondents.

<rpc-chart
url="Race_Chart_REVISED.csv"
type="horizontalBar"
stacked="true"
x-label="Percentage of Population"
x-min="0"
x-max="100"
legend-position="top"
legend="true"
tooltip-intersect="false"
source="LACS Survey Round 2, 2024; U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B03002"
chart-title="Race and Ethnicity of Respondents"></rpc-chart>

## Employment 

### Question 13 - Which category(ies) best describes [your employment status]?
When looking at overlapping employment categories and allowing respondents to select multiple choices, 55% of the 186 respondents to Question 13 indicated that they work outside the home, 39% are students, 9% are retired, 9% work in their home, and less than 4% each are looking for work, homemakers, or other.

<rpc-chart
url="Employment.csv"
type="horizontalBar"
stacked="true"
x-label="Percentage of total respondents"
x-min="0"
x-max="100"
legend-position="top"
legend="true"
tooltip-intersect="false"
source="LACS Survey Round 2, 2024"
chart-title="Employment Categories of Survey Respondents (Non-Exclusive)"></rpc-chart>