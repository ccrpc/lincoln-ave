---
title: Phase Two - Survey Comment Analysis
weight: 60
---

The second round of public outreach for the Lincoln Avenue Corridor Study resulted in a total of total of ***192 survey responses***.
This page will analyze the survey responses relating to proposed changes on Lincoln Avenue, as expressed through the following questions - 
- **Question 1** – Please indicate your support for the following proposed improvements.
- **Question 2** – What about these improvements do you support?
- **Question 3** – What about these improvements do you NOT support?
- **Question 4** – Which of the proposed bicycle and       pedestrian improvements do you prefer?

- **Question 5** – Do you have additional comments about these proposed bicycle and pedestrian improvements?
- **Question 8** – Please provide any other comments about this section of Lincoln Avenue or the content on the project website.

<br>

## Level of Support

***Question 1 - Please indicate your support for the following proposed improvements.***

Question 1 asked respondents to provide their support or opposition to proposed improvements, as described on the [Potential Interventions](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/) page.

As seen in the graph and table below, the majority of the proposed improvements received strong public support and minimal opposition. Adding rectangular rapid flashing beacons (RRFBs) to existing crosswalks along the corridor scored the highest (83% approval), followed by adjusting traffic signal timing (77%), adding a new, RRFB-equipped crosswalk at Oregon (73%), and reconstructing the Lincoln Avenue roadway to three traffic lanes (61% approval). 

<rpc-chart
url="Q1-Graph.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of total respondents"
x-min="0"
x-max="100"
wrap=20
legend-position="top"
legend="true"
tooltip-intersect="true"
source="LACS Round 2 Survey, 2024"
chart-title="Q1 Responses - Level of Support"></rpc-chart>




In comparison, consolidating the Indiana and Ohio crosswalks into a single, mid-block RRFB (52% approval), converting Nevada and Vermont to right-turn in/right-turn out (51%), and closing vehicle access to Lincoln from Oregon, Iowa, and Indiana (37.5%) all had a narrrow majority or less than majority affirmatively supporting these proposals. While none of these had a majority in opposition to the proposal, the vehicle access closure did receive a plurality in opposition (40.6%).

Analysis of the comments provided in the following questions and on the online map provides insight into the specific concerns that respondents had with these proposals. In response to this, the study team is working to address these concerns, as well as developing clearer explanations of what the changes would and would not entail, what the benefits are, and how the different changes are connected to one another (for example, the road closures enabling the installation of rectangular rapid flashing beacons).


----------------------

## Supported Improvements

***Question 2 - What about these improvements do you support?***

The responses to Questions 2 and 3, where respondents provided written descriptions of their  support or opposition to the proposed improvements,  were identified as fitting into three broad categories- **Bicycle and Pedestrian Improvements**, **Roadway Improvements**, and **Other**. Within these categories, comments were further sorted into specific topics. The graph below displays the number of Question 2 responses addressing these specific topics, out of 143 respondents to this question.


<rpc-chart
url="Q2-Chart.csv"
type="horizontalBar"
colors="#4169E1, #4169E1, #4169E1, #4169E1, #4169E1, #4169E1, #4169E1,#008000,#008000,#008000,#008000,#008000,#008000,#FFA500,#FFA500"
stacked="false"
x-label="Count"
x-min="0"
x-max="45"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Q2 Responses - Supported Improvements"></rpc-chart>


 <u>Bicycle and Pedestrian Improvements</u> - Of the responses to this question, 27% mentioned support for improvements that increase pedestrian safety; these comments involved reducing crossing distance, creating safer crosswalks, decreasing traffic speed, heightening pedestrian visibility, and adding RRFBs. In fact, 19% of respondents supported adding RRFBs at crosswalks along the corridor to make crossing a more visible and safer experience. 
 
 Additionally, 17% of comments approved of crosswalk improvements, calling for fewer but safer crosswalks that would make pedestrian travel easier along the corridor. Eleven percent showed support for general pedestrian improvements, such as creating a more pedestrian safe corridor and increased pedestrian lighting. About 8% of comments supported advances in bicycle safety with proposed improvements. 

<u>Roadway Improvements</u> - About 17% of comments supported changes in the roadway configuration, appreciating attempts to slow traffic, increase flow, and remove conflict points. Eight percent supported limiting side street access; eight percent also showed support for roadway improvements, such as improving traffic, decreasing speed, and repairing pavement.

<u>Other</u> - Other comments included supportive remarks about the project and options presented (8%), as well as concerns regarding pedestrian safety at crossings and the improvements’ impact on traffic flow (3%).



---------------------

## Opposed Improvements

***Question 3 - What about these improvements do you NOT support?***

As mentioned above, the responses to Questions 2 and 3, where respondents provided written descriptions of their support or opposition to the proposed improvements,  were identified as fitting into three broad categories- **Bicycle and Pedestrian Improvements**, **Roadway Improvements**, and **Other**. Within these categories, comments were further sorted into specific topics. The following graph displays the number of Question 3 responses addressing these specific topics, out of the 122 respondents to this question.

<rpc-chart
url="Q3-Chart.csv"
type="horizontalBar"
colors="#4169E1, #4169E1, #4169E1, #4169E1, #9ACD32, #9ACD32, #9ACD32, #9ACD32, #9ACD32, #DB7093, #DB7093, #DB7093"
stacked="false"
x-label="Count"
x-min="0"
x-max="60"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Q3 Responses - Opposed Improvements"></rpc-chart>


<u>Bicycle and Pedestrian Improvements</u> - Of responses to this question, 11% were unsupportive of crosswalk improvements, more specifically consolidating crosswalks. Respondents were opposed to crosswalk removal, which may have been due to unclear wording regarding the consolidation of the Indiana and Ohio crosswalks into a single, mid-block RRFB crossing, or a belief that this consolidation would present excessive distance between crosswalks and lead to jaywalking.

<u>Roadway Improvements</u> - Of the responses to this question, 47% were unsupportive of reducing side street access. Most of these comments concerned the impact on the neighborhood (e.g., increased traffic on unclosed streets) and those living on closed roads. Others cited dissatisfaction with the lack of options on which roads to close, the choice to leave access to brick roads (i.e. Nevada Street) that are difficult to travel on, and concern regarding bicycle connectivity across Lincoln Avenue when limiting access. 

Comments relating to Roadway Configuration (12%) concerned the lane reduction’s impact on traffic flow; others showed concern for how drivers will navigate delivery trucks and buses stopping along the corridor and whether that would involve using the turn lane to pass stopped vehicles.

<u>Other</u> - Other comments (8%) included concern that improvements would be confusing to out-of-town drivers, and some found the proposed changes confusing, themselves. Other comments were unsupportive of the potential time and cost of construction. 

---------

## Bike and Pedestrian Scenarios

***Question 4 - Which of the proposed bicycle and pedestrian improvements do you prefer?***

The following graph displays respondents' preferences on the three provided bike and pedestrian infrastructure scenarios, as described in the [Potential Interventions](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/#bike-and-pedestrian-scenarios) page.


<rpc-chart
url="Q4-Chart.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Count"
x-min="0"
x-max="115"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Q4 Responses - Bike and Pedestrian Scenarios"></rpc-chart>

Between the three scenarios, Scenario 1, with continuous bike lanes for the whole length of the corridor, was the strong favorite, with preference from 103 (54%) of respondents. The continuous bike lane option was more popular than the other two options combined (35% preference together), with bike lane north of Iowa and shared use path south of Iowa (26%) significantly more popular than a shared use path between two sets of bike lanes (9%). Six percent of survey respondents did not provide an answer to Question 4, and another 6% preferred “None of the above.”

---------

## Bike and Pedestrian Comments

***Question 5 - Do you have additional comments about these proposed bicycle and pedestrian improvements?***

Question 5 allowed respondents to provide additional input on proposed bicycle and pedestrian improvements. The 86 responses to this question were sorted into the categories seen in the graph and summaries below.

<rpc-chart
url="Q5-Chart.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Count"
x-min="0"
x-max="70"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Q5 Responses - Bike and Pedestrian Comments"></rpc-chart>


<u>Bicycle Facilities</u> - Of the responses to this question, 64 comments (74%) addressed Bicycle Facilities; about one-third of these bicycle facility comments called specifically for protected bike lanes. Other comments in this category mentioned the shared-use path; the majority of these comments supported a shared use path for bicycle use because of the separation from traffic. However, a small portion of these comments expressed uneasiness over a shared-use path used for both biking and walking, citing increased risk of conflict, and wished for better separation of modes. Other comments mentioned bike crossings and concern about crossing the corridor to connect to existing facilities (i.e. Florida Ave north and south of Lincoln).

<u>Roadway Configuration</u> - Nineteen of the twenty-three comments concerning Roadway Configuration (27%) encouraged separation between all modes of transportation along the corridor. Many responses that showed support for bicycle and pedestrian facilities mentioned feeling unsafe if there are no protective measures in place to separate them from automobile traffic. The remaining comments called for road reconfiguration that prioritizes pedestrian safety, narrowing the road, and improving infrastructure (e.g., curb cuts and extensions).

<u>Safety</u> - Of the responses to this question, 23% reiterated prioritizing Safety, specifically for cyclists and pedestrians. Many of these remarks were in conjunction with the comments for protected bicycle lanes and better mode separation. 

<u>Other Topics</u> - Other comments restated concerns over time, confusion, and cost (previously discussed in Question 3). Several comments concerned the speed along the corridor, changes to traffic flow, current pavement conditions, and confusion over loading zones in proposed plans. Some respondents mentioned pedestrian facilities (favoring safer, signalized crosswalks) and better education for drivers and pedestrians about crosswalks.

---------

## Additional Comments

***Question 8 - Please provide any other comments about this section of Lincoln Avenue or the content on the project website.***

Question 8 provided a space for additional feedback on the Lincoln Avenue Corridor Study. The 49 responses to this question were sorted into the categories seen in the graph and summaries below. Individual comments frequently mentioned more than one topic, so the percentages presented exceed 100%.

<rpc-chart
url="Q8-Chart.csv"
type="horizontalBar"
colors="#4169E1"
stacked="false"
x-label="Count"
x-min="0"
x-max="20"
legend-position="top"
legend="false"
tooltip-intersect="false"
source="Lincoln Avenue Corridor Study Survey"
chart-title="Q8 Responses - Additional Comments"></rpc-chart>


The responses to Question 8 were very similar to those of Question 5, although Safety improvements (37% of responses) received a stronger emphasis here. About 44% of these comments involved pedestrian safety, while 33% involved bicycle safety. Specifically, responses mentioned slowing traffic, installing better lighting, and increasing visibility of pedestrians through signage. As for bicycle safety, comments mentioned needing protected bicycle facilities to foster a safe environment to bike. Twenty percent of the total comments pertained to pedestrian facilities, which included comments on improving crosswalks, RRFBs, and adding infrastructure such as rumble strips or raised crossings. Overall, these comments reiterated common themes of prioritizing safety, supporting pedestrian and bicycle facilities, and a expressing some concerns about the impacts of proposed changes. 