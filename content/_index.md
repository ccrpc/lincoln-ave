---
title: Home
draft: false
bannerHeading: "Lincoln Avenue Corridor Study"
bannerText: "The overarching goal of this study is to identify ways to increase transportation safety, mobility, and multimodal connectivity in this high-priority north-south corridor bridging the City of Urbana and the University of Illinois campus."
---

# We welcome your feedback!

The [final recommendations](https://ccrpc.gitlab.io/lincoln-ave/implementation/recs/) of the Lincoln Avenue Corridor Study have been published under the Implementation section of the plan, in addition to [Frequently Asked Questions](https://ccrpc.gitlab.io/lincoln-ave/implementation/faq/) about the recommendations. The project team presented these recommendations to community stakeholders in fall of 2024 and accepted public feedback on these final recommendations from Wednesday, November 20th, 2024 to Friday, January 3rd, 2025.

Following analysis of this feedback and any final edits to the study, the study Working Group will seek the endorsement of the study by the Urbana City Council, so that it can be used in future grant applications for implementation funds. 

In addition to reviewing the Implementation section, interested community members can visit the prevously-published sections of the study, including the [Future Conditions](https://ccrpc.gitlab.io/lincoln-ave/future/) section of the site for [forecasts for future conditions](https://ccrpc.gitlab.io/lincoln-ave/future/travel-forecasting) on the corridor, [evaluation of different alternatives](https://ccrpc.gitlab.io/lincoln-ave/future/alternative-evaluation/) for transportation management, and the [proposed interventions scenarios](https://ccrpc.gitlab.io/lincoln-ave/future/potential-countermeasures/) for the corridor. Visitors can also check out the [Existing Conditions](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/demographics/) and [Public Involvement](https://ccrpc.gitlab.io/lincoln-ave/public/all_survey_analysis/) sections of the website for more information on the work that has already been conducted.


Thank you for your help in enabling the Lincoln Avenue Corridor to best serve our community!

<br>

___

<br>

# Lincoln Avenue Corridor Study

**Project Lead:** Champaign-Urbana Urbanized Area Transportation Study (CUUATS)

**Project Partners:** City of Urbana, University of Illinois at Urbana-Champaign, Champaign-Urbana Mass Transit District (MTD)

**Project Timeline:** Spring 2023 - Winter 2024

**Project Funding:** Statewide Planning and Research funds from the Federal Highway Administration (FHWA) and the Illinois Department of Transportation (IDOT)

The Lincoln Avenue Corridor Study is evaluating approximately 1.2 miles of Lincoln Avenue in the City of Urbana, focused on the area between Green Street and Florida Avenue. Situated in the City of Urbana, adjacent to the University of Illinois flagship campus in Urbana-Champaign, this corridor is a high-priority, high-traffic location in the region. The goal of this study is to identify and coordinate multimodal infrastructure improvements to increase safety and mobility along this corridor, including addressing known high-frequency crash destinations and developing proposals for safe and efficient usage of the corridor by pedestrians, cyclists, transit riders, and drivers. 

For a summary of the corridor study process, including the project timeline, see the [Planning Process](https://ccrpc.gitlab.io/lincoln-ave/process/) tab. Information on the past and current conditions along the corridor, including transportation infrastructure, demographics, land use, and other variables, can be found under [Existing Conditions](https://ccrpc.gitlab.io/lincoln-ave/existing-conditions/). As the study progresses, the [Future Conditions](https://ccrpc.gitlab.io/lincoln-ave/future/), [Implementation](https://ccrpc.gitlab.io/lincoln-ave/implementation/), and [Public Involvement](https://ccrpc.gitlab.io/lincoln-ave/public/) pages will contain additional information and opportunities for the public to get involved in the study.

For questions or additional information, see the [Contact](https://ccrpc.gitlab.io/lincoln-ave/contact/) page.

CUUATS, the City of Urbana, and their partner agencies look forward to working with our residents to plan for and create a safe and well-functioning Lincoln Avenue for years to come!
