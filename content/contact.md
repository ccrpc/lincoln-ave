---
title: Contact
draft: false
menu: eyebrow
weight: 10
---

For information about the Lincoln Avenue Corridor Study, please contact - 

J.D. McClanahan <br>
Planner II<br>
Champaign County Regional Planning Commission<br>
jmcclanahan@ccrpc.org<br>
217-328-3313, ext. 196 <br>