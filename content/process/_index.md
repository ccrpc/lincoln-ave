---
title: "Planning Process"
draft: false
menu: main
weight: 10
---

The Lincoln Avenue Corridor Study is evaluating approximately 1.2 miles of Lincoln Avenue in the City of Urbana, focused on the area between Green Street and Florida Avenue. Situated in the City of Urbana, adjacent to the University of Illinois flagship campus in Urbana-Champaign, this corridor is a high-priority, high-traffic location in the region with several regional destinations located within or adjacent to the corridor, including the University’s Division I athletic facilities, several university residence halls, a historic residential neighborhood, the Spurlock Museum of World Cultures, and a variety of university academic facilities. The goal of this study is to identify and coordinate multimodal infrastructure improvements to increase safety and mobility along this corridor. 

The primary motivation for this corridor study is based on the corridor’s central intersection of Lincoln Avenue and Ohio Street, which was identified by the Illinois Department of Transportation (IDOT) in the top five percent of priority safety locations in IDOT District 5 in 2017. Top five percent locations exhibit the most severe safety needs based on crashes, injuries, deaths, traffic volume levels, and other relevant data as determined by the state. During the five-year safety analysis period, 26 crashes—including four type-A (severe injury) crashes—occurred at this intersection. The intersection currently operates as a three-leg intersection, with stop sign control on the eastern leg. The state’s safety analysis suggested a pedestrian refugee median and curb bump outs as a countermeasure to address safety concerns here.

Because the City of Urbana is committed to supporting active transportation as a healthy and environmentally responsible way to get around, the corridor study recommendations will incorporate all modes of transportation. In 2014, Urbana was named the first Gold-Level Bicycle Friendly Community in Illinois by the League of American Bicyclists. Urbana’s rates of walking, biking, and using transit are the highest in the region and significantly higher than state and national averages. Thanks to a 2016 updated Bicycle Master Plan and a 2020 Pedestrian Plan, the City has already identified desirable upgrades to bicycle and pedestrian infrastructure in the study area. This corridor study will identify how and when these improvements can move forward in coordination with the safety improvements.

CCRPC and the City of Urbana aim to produce a corridor study report that will provide innovative and coordinated recommendations to improve the safety of the corridor while maintaining the high-level of multimodal accessibility that the Urbana community is known for. This corridor study is being undertaken by CCRPC staff and will be collaborative in nature, meeting regularly with project partners from the City of Urbana, the University and the Mass Transit District. During the study, CCRPC staff will also involve other stakeholders, including local residents, business owners, and property owners, to actively seek input on the future of the corridor and to promote awareness of transportation issues and potential countermeasures.

The final product will be a corridor study report that addresses all modes of transportation in the corridor including pedestrians, bicyclists, transit, and automobiles. The final product will include recommendations for future improvements as well as documentation of how those improvements are projected to impact safety and mobility in the corridor and public input regarding the recommendations. The City of Urbana plans to pursue state and local funding to construct recommendations from this study, and the corridor study will serve as a guide for the implementation and coordination of future improvements in the corridor, including documentation to facilitate applications for funding assistance.


# Anticipated Project Schedule – Spring 2023 - Fall 2024

### Spring - Fall 2023

- **Approve Scope and Timeline**
  - Finalize scope/goals and timetable with project partners
  - Develop public involvement plan

- **Existing Conditions** 
  - Collect and analyze transportation data along the corridor
  - Analyze existing data on corridor demographics, land use, environmental conditions, emergency services, and utilities and infrastructure
  - Engage with public to understand residents' opinions and concerns with corridor's current state

- **Future Conditions** 
  - Project future population, employment, and traffic
  - Develop possible future land use scenarios
  - Develop baseline transportation modeling and simulation of corridor based on projections

### Fall 2023 - Spring 2024

- **Identify Potential Countermeasures**
  - Identify potential actions, changes, and transportation investments along the corridor
  - Engage with public to obtain feedback about potential countermeasures, analyze feedback

- **Scenario Development and Evaluation**
  - Develop future corridor scenarios based on public input
  - Model/evaluate future scenarios, refine scenarios, and compare between different scenarios

### Spring - Fall 2024

- **Preferred Alternative Selection**
  - Hold public meeting to present and get feedback on future scenarios; analyze public input
  - Determine preferred future scenario

- **Project Prioritization and Implementation Recommendations**
  - Develop recommendations and implementation plan in order to work toward preferred scenario
  - Engage with public to address concerns or questions about preferred scenario

- **Finalize Corridor Study Report**
  - Make final changes to preferred scenario plan, based on public and stakeholder feedback
  - Develop materials to seek future funding for infrastructure investments and countermeasures 
